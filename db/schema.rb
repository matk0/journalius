# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_21_090747) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "hstore"
  enable_extension "plpgsql"

  create_table "abstract_and_indexing_databases", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "abstract_and_indexing_databases_journals", id: false, force: :cascade do |t|
    t.bigint "abstract_and_indexing_database_id"
    t.bigint "journal_id"
    t.index ["abstract_and_indexing_database_id"], name: "index_ai_dbs_journals_on_abstract_and_indexing_databases_id"
    t.index ["journal_id"], name: "index_abstract_and_indexing_databases_journals_on_journal_id"
  end

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "addresses", force: :cascade do |t|
    t.string "line_1"
    t.string "line_2"
    t.string "zipcode"
    t.string "city"
    t.string "country"
    t.bigint "institution_id"
    t.bigint "institutions_billing_address_id"
    t.index ["institution_id"], name: "index_addresses_on_institution_id"
    t.index ["institutions_billing_address_id"], name: "index_addresses_on_institutions_billing_address_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "assignments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "role_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "assignable_id"
    t.string "assignable_type"
    t.index ["role_id"], name: "index_assignments_on_role_id"
    t.index ["user_id"], name: "index_assignments_on_user_id"
  end

  create_table "channel_users", force: :cascade do |t|
    t.bigint "channel_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "last_read_at"
    t.index ["channel_id"], name: "index_channel_users_on_channel_id"
    t.index ["user_id"], name: "index_channel_users_on_user_id"
  end

  create_table "channels", force: :cascade do |t|
    t.string "name"
    t.boolean "private"
    t.boolean "direct"
    t.bigint "manuscript_id"
    t.bigint "institution_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["institution_id"], name: "index_channels_on_institution_id"
    t.index ["manuscript_id"], name: "index_channels_on_manuscript_id"
  end

  create_table "co_authors", force: :cascade do |t|
    t.string "full_name"
    t.bigint "manuscript_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["manuscript_id"], name: "index_co_authors_on_manuscript_id"
  end

  create_table "copyleaks_logins", force: :cascade do |t|
    t.text "access_token"
    t.datetime "issued_at"
    t.datetime "expires_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "editor_in_chief_roles", force: :cascade do |t|
  end

  create_table "events", force: :cascade do |t|
    t.integer "user_id"
    t.string "action"
    t.integer "eventable_id"
    t.string "eventable_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "institutions", force: :cascade do |t|
    t.string "name"
    t.integer "kind"
    t.string "iban"
    t.string "bic"
    t.string "account_name"
    t.boolean "eu_vat_registered", default: false
    t.string "vat_no"
    t.string "billing_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "doi"
  end

  create_table "internal_invitations", force: :cascade do |t|
    t.bigint "invitee_id"
    t.bigint "inviter_id"
    t.bigint "invited_as_id"
    t.bigint "invitable_id"
    t.string "invitable_type"
    t.string "state"
    t.string "raw_invitation_token"
    t.index ["invited_as_id"], name: "index_internal_invitations_on_invited_as_id"
    t.index ["invitee_id"], name: "index_internal_invitations_on_invitee_id"
    t.index ["inviter_id"], name: "index_internal_invitations_on_inviter_id"
  end

  create_table "issues", force: :cascade do |t|
    t.integer "number"
    t.bigint "volume_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["volume_id"], name: "index_issues_on_volume_id"
  end

  create_table "journals", force: :cascade do |t|
    t.string "title"
    t.string "print_issn"
    t.string "electronic_issn"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "institution_id"
    t.boolean "published"
    t.string "subtitle"
    t.string "languages"
    t.integer "volumes_per_year"
    t.integer "issues_per_volume"
    t.integer "publishing_frequency", default: 1
    t.string "expected_months_of_issue"
    t.integer "publishing_model"
    t.integer "peer_review_type"
    t.boolean "open_access_creative_commons"
    t.bigint "publisher_id"
    t.string "webpage"
    t.bigint "payer_id"
    t.bigint "owner_id"
  end

  create_table "journals_subjects", id: false, force: :cascade do |t|
    t.bigint "journal_id", null: false
    t.bigint "subject_id", null: false
    t.index ["journal_id", "subject_id"], name: "index_journals_subjects_on_journal_id_and_subject_id"
    t.index ["subject_id", "journal_id"], name: "index_journals_subjects_on_subject_id_and_journal_id"
  end

  create_table "language_edits", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "manuscript_id", null: false
    t.string "state"
    t.string "invitee_email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["manuscript_id"], name: "index_language_edits_on_manuscript_id"
    t.index ["user_id"], name: "index_language_edits_on_user_id"
  end

  create_table "manuscripts", force: :cascade do |t|
    t.string "title"
    t.string "doi"
    t.string "area_of_science"
    t.datetime "published_at"
    t.bigint "corresponding_author_id"
    t.bigint "issue_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "state"
    t.string "languages"
    t.bigint "journal_id"
    t.text "html"
    t.index ["corresponding_author_id"], name: "index_manuscripts_on_corresponding_author_id"
    t.index ["issue_id"], name: "index_manuscripts_on_issue_id"
    t.index ["journal_id"], name: "index_manuscripts_on_journal_id"
  end

  create_table "manuscripts_subjects", id: false, force: :cascade do |t|
    t.bigint "manuscript_id", null: false
    t.bigint "subject_id", null: false
    t.index ["manuscript_id", "subject_id"], name: "index_manuscripts_subjects_on_manuscript_id_and_subject_id"
    t.index ["subject_id", "manuscript_id"], name: "index_manuscripts_subjects_on_subject_id_and_manuscript_id"
  end

  create_table "manuscripts_users", id: false, force: :cascade do |t|
    t.bigint "manuscript_id"
    t.bigint "user_id"
    t.index ["manuscript_id"], name: "index_manuscripts_users_on_manuscript_id"
    t.index ["user_id"], name: "index_manuscripts_users_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text "body"
    t.bigint "channel_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["channel_id"], name: "index_messages_on_channel_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "peer_reviews", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "manuscript_id", null: false
    t.string "state"
    t.integer "recommendation"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "questionnaire_id"
    t.hstore "questionnaire_answers"
    t.index ["manuscript_id"], name: "index_peer_reviews_on_manuscript_id"
    t.index ["questionnaire_answers"], name: "index_peer_reviews_on_questionnaire_answers", using: :gin
    t.index ["user_id"], name: "index_peer_reviews_on_user_id"
  end

  create_table "pins", force: :cascade do |t|
    t.string "pinnable_type", null: false
    t.bigint "pinnable_id", null: false
    t.bigint "user_id", null: false
    t.index ["pinnable_type", "pinnable_id"], name: "index_pins_on_pinnable_type_and_pinnable_id"
    t.index ["user_id"], name: "index_pins_on_user_id"
  end

  create_table "plagiarism_scans", force: :cascade do |t|
    t.text "encoded_file"
    t.text "state"
    t.json "completed_payload"
    t.json "error_payload"
    t.json "credits_check_payload"
    t.json "indexed_payload"
    t.bigint "manuscript_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["manuscript_id"], name: "index_plagiarism_scans_on_manuscript_id"
  end

  create_table "proof_reads", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "manuscript_id", null: false
    t.string "state"
    t.string "invitee_email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["manuscript_id"], name: "index_proof_reads_on_manuscript_id"
    t.index ["user_id"], name: "index_proof_reads_on_user_id"
  end

  create_table "questionnaire_templates", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "institution_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string "text"
    t.string "field_type"
    t.string "field_attr"
    t.boolean "required"
    t.bigint "questionnaire_template_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["questionnaire_template_id"], name: "index_questions_on_questionnaire_template_id"
  end

  create_table "rejections", force: :cascade do |t|
    t.bigint "manuscript_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["manuscript_id"], name: "index_rejections_on_manuscript_id"
  end

  create_table "resubmissions", force: :cascade do |t|
    t.bigint "manuscript_id", null: false
    t.index ["manuscript_id"], name: "index_resubmissions_on_manuscript_id"
  end

  create_table "revision_requests", force: :cascade do |t|
    t.bigint "manuscript_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["manuscript_id"], name: "index_revision_requests_on_manuscript_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "ancestry"
    t.index ["ancestry"], name: "index_subjects_on_ancestry"
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "taggings_taggable_context_idx"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "technical_edits", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "manuscript_id", null: false
    t.string "state"
    t.string "invitee_email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["manuscript_id"], name: "index_technical_edits_on_manuscript_id"
    t.index ["user_id"], name: "index_technical_edits_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "uid"
    t.string "avatar_url"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.bigint "institution_id"
    t.bigint "last_visited_channel_id"
    t.integer "title"
    t.string "first_name"
    t.string "last_name"
    t.string "provider"
    t.hstore "oauth_response"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "volumes", force: :cascade do |t|
    t.integer "number"
    t.bigint "journal_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["journal_id"], name: "index_volumes_on_journal_id"
  end

  add_foreign_key "abstract_and_indexing_databases_journals", "abstract_and_indexing_databases"
  add_foreign_key "abstract_and_indexing_databases_journals", "journals"
  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "assignments", "roles"
  add_foreign_key "assignments", "users"
  add_foreign_key "channel_users", "channels"
  add_foreign_key "channel_users", "users"
  add_foreign_key "channels", "institutions"
  add_foreign_key "channels", "manuscripts"
  add_foreign_key "internal_invitations", "roles", column: "invited_as_id"
  add_foreign_key "internal_invitations", "users", column: "invitee_id"
  add_foreign_key "internal_invitations", "users", column: "inviter_id"
  add_foreign_key "language_edits", "manuscripts"
  add_foreign_key "language_edits", "users"
  add_foreign_key "messages", "channels"
  add_foreign_key "messages", "users"
  add_foreign_key "peer_reviews", "manuscripts"
  add_foreign_key "peer_reviews", "users"
  add_foreign_key "pins", "users"
  add_foreign_key "proof_reads", "manuscripts"
  add_foreign_key "proof_reads", "users"
  add_foreign_key "questions", "questionnaire_templates"
  add_foreign_key "rejections", "manuscripts"
  add_foreign_key "resubmissions", "manuscripts"
  add_foreign_key "revision_requests", "manuscripts"
  add_foreign_key "taggings", "tags"
  add_foreign_key "technical_edits", "manuscripts"
  add_foreign_key "technical_edits", "users"
end
