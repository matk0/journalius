# frozen_string_literal: true

class AddInstitutionIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :institution_id, :bigint, index: true
  end
end
