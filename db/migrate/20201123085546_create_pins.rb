class CreatePins < ActiveRecord::Migration[6.0]
  def change
    create_table :pins do |t|
      t.references :pinnable, polymorphic: true, null: false
      t.references :user, null: false, foreign_key: true
    end
  end
end
