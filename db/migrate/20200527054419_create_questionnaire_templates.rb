# frozen_string_literal: true

class CreateQuestionnaireTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :questionnaire_templates do |t|
      t.string :name

      t.timestamps
    end
  end
end
