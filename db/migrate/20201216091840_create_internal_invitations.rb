class CreateInternalInvitations < ActiveRecord::Migration[6.0]
  def change
    create_table :internal_invitations do |t|
      t.references :invitee, index: true, foreign_key: { to_table: :users }
      t.references :inviter, index: true, foreign_key: { to_table: :users }
      t.references :invited_as, index: true, foreign_key: { to_table: :roles }
      t.bigint :invitable_id
      t.string :invitable_type
      t.string :state
      t.string :raw_invitation_token
    end
  end
end
