class AddPayerIdToJournals < ActiveRecord::Migration[6.0]
  def change
    add_column :journals, :payer_id, :bigint
  end
end
