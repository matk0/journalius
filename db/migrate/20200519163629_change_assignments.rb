# frozen_string_literal: true

class ChangeAssignments < ActiveRecord::Migration[6.0]
  def change
    change_column :assignments, :journal_id, :bigint, null: true
  end
end
