# frozen_string_literal: true

class CreateRevisionRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :revision_requests do |t|
      t.references :manuscript, null: false, foreign_key: true

      t.timestamps
    end
  end
end
