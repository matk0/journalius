# frozen_string_literal: true

class CreateCoAuthors < ActiveRecord::Migration[6.0]
  def change
    create_table :co_authors do |t|
      t.string :full_name
      t.references :manuscript

      t.timestamps
    end
  end
end
