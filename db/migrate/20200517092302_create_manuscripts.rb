# frozen_string_literal: true

class CreateManuscripts < ActiveRecord::Migration[6.0]
  def change
    create_table :manuscripts do |t|
      t.string :title
      t.string :doi
      t.string :area_of_science
      t.datetime :published_at
      t.bigint :corresponding_author_id, index: true
      t.bigint :issue_id, index: true
      t.bigint :submission_id, index: true

      t.timestamps
    end
  end
end
