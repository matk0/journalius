# frozen_string_literal: true

class AddAttributesToManuscripts < ActiveRecord::Migration[6.0]
  def change
    add_column :manuscripts, :languages, :string
  end
end
