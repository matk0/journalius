class CreatePlagiarismScans < ActiveRecord::Migration[6.0]
  def change
    create_table :plagiarism_scans do |t|
      t.text :encoded_file
      t.text :state
      t.json :completed_payload
      t.json :error_payload
      t.json :credits_check_payload
      t.json :indexed_payload
      t.references :manuscript

      t.timestamps
    end
  end
end
