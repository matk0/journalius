# frozen_string_literal: true

class RefactorSubmissions < ActiveRecord::Migration[6.0]
  def change
    remove_column :manuscripts, :submission_id
    drop_table :submissions
    change_table :manuscripts do |t|
      t.references :journal
    end
  end
end
