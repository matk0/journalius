# frozen_string_literal: true

class CreateAbstractAndIndexingDatabases < ActiveRecord::Migration[6.0]
  def change
    create_table :abstract_and_indexing_databases do |t|
      t.string :name

      t.timestamps
    end
  end
end
