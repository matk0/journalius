class RemovePublisherKindFromJournals < ActiveRecord::Migration[6.0]
  def change
    remove_column :journals, :publisher_kind
  end
end
