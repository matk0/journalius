# frozen_string_literal: true

class CreateJoinTableManuscriptsSubjects < ActiveRecord::Migration[6.0]
  def change
    create_join_table :manuscripts, :subjects do |t|
      t.index [:manuscript_id, :subject_id]
      t.index [:subject_id, :manuscript_id]
    end
  end
end
