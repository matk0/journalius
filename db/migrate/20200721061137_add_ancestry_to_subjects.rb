# frozen_string_literal: true

class AddAncestryToSubjects < ActiveRecord::Migration[6.0]
  def change
    add_column :subjects, :ancestry, :string
    add_index :subjects, :ancestry
  end
end
