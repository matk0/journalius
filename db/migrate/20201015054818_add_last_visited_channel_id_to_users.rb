# frozen_string_literal: true

class AddLastVisitedChannelIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :last_visited_channel_id, :bigint
  end
end
