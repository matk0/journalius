class AddHtmlToManuscripts < ActiveRecord::Migration[6.0]
  def change
    add_column :manuscripts, :html, :text
  end
end
