# frozen_string_literal: true

class AddInstitutionsToJournals < ActiveRecord::Migration[6.0]
  def change
    add_column :journals, :institution_id, :bigint, index: true
  end
end
