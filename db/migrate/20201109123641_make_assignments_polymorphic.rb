class MakeAssignmentsPolymorphic < ActiveRecord::Migration[6.0]
  def change
    add_column :assignments, :assignable_id, :integer
    add_column :assignments, :assignable_type, :string
  end
end
