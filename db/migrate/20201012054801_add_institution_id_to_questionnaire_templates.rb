# frozen_string_literal: true

class AddInstitutionIdToQuestionnaireTemplates < ActiveRecord::Migration[6.0]
  def change
    add_column :questionnaire_templates, :institution_id, :bigint, index: true
  end
end
