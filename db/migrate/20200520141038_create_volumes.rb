# frozen_string_literal: true

class CreateVolumes < ActiveRecord::Migration[6.0]
  def change
    create_table :volumes do |t|
      t.integer :number
      t.belongs_to :journal, index: true

      t.timestamps
    end
  end
end
