# frozen_string_literal: true

class AddAttributesToJournals < ActiveRecord::Migration[6.0]
  def change
    add_column :journals, :published, :boolean
    add_column :journals, :subtitle, :string
    rename_column :journals, :name, :title
    add_column :journals, :languages, :string
    add_column :journals, :volumes_per_year, :integer
    add_column :journals, :issues_per_volume, :integer
    add_column :journals, :publishing_frequency, :integer, default: 1
    add_column :journals, :expected_months_of_issue, :string
    add_column :journals, :publishing_model, :integer
    add_column :journals, :peer_review_type, :integer
    add_column :journals, :open_access_creative_commons, :boolean
    add_column :journals, :publisher_id, :bigint, references: :publishers
    add_column :journals, :webpage, :string
  end
end
