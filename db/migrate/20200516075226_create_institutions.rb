# frozen_string_literal: true

class CreateInstitutions < ActiveRecord::Migration[6.0]
  def change
    create_table :institutions do |t|
      t.string :name
      t.integer :kind
      t.string :iban
      t.string :bic
      t.string :account_name
      t.boolean :eu_vat_registered, default: false
      t.string :vat_no
      t.string :billing_name

      t.timestamps
    end
  end
end
