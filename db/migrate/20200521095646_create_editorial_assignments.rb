# frozen_string_literal: true

class CreateEditorialAssignments < ActiveRecord::Migration[6.0]
  def change
    create_table :editorial_assignments do |t|
      t.belongs_to :manuscript, null: false, foreign_key: true
      t.belongs_to :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
