class DropColumnsFromAssignments < ActiveRecord::Migration[6.0]
  def change
    remove_column :assignments, :journal_id
    remove_column :assignments, :institution_id
  end
end
