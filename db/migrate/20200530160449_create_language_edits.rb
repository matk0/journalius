# frozen_string_literal: true

class CreateLanguageEdits < ActiveRecord::Migration[6.0]
  def change
    create_table :language_edits do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.belongs_to :manuscript, null: false, foreign_key: true
      t.string :state
      t.string :invitee_email

      t.timestamps
    end
  end
end
