class CreateEditorInChiefRole < ActiveRecord::Migration[6.0]
  def change
    create_table :editor_in_chief_roles do |t|
      Role.create(name: "Editor in Chief")
    end
  end
end
