class AddPeerReviewerRole < ActiveRecord::Migration[6.0]
  def change
    Role.create(name: "Reviewer")
  end
end
