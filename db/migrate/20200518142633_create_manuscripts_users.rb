# frozen_string_literal: true

class CreateManuscriptsUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :manuscripts_users, id: false do |t|
      t.belongs_to :manuscript
      t.belongs_to :user
    end
  end
end
