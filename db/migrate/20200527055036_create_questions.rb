# frozen_string_literal: true

class CreateQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :questions do |t|
      t.string :text
      t.string :field_type
      t.string :field_attr
      t.boolean :required
      t.belongs_to :questionnaire_template, null: false, foreign_key: true

      t.timestamps
    end
  end
end
