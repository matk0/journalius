class RemoveAttributesFromPeerReviews < ActiveRecord::Migration[6.0]
  def change
    remove_column :peer_reviews, :invitee_email
  end
end
