# frozen_string_literal: true

class CreatePublishers < ActiveRecord::Migration[6.0]
  def change
    create_table :publishers do |t|
      t.string :name, index: true
      t.string :doi

      t.timestamps
    end
  end
end
