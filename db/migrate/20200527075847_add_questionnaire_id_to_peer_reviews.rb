# frozen_string_literal: true

class AddQuestionnaireIdToPeerReviews < ActiveRecord::Migration[6.0]
  def change
    add_column :peer_reviews, :questionnaire_id, :bigint, index: true
  end
end
