class ChangePeerReviewsRecommendationToInteger < ActiveRecord::Migration[6.0]
  def change
    change_column :peer_reviews, :recommendation, 'integer USING CAST(recommendation AS integer)'
  end
end
