# frozen_string_literal: true

class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.string :line_1
      t.string :line_2
      t.string :zipcode
      t.string :city
      t.string :country
      t.references :institution, index: true
      t.bigint :institutions_billing_address_id, index: true
    end
  end
end
