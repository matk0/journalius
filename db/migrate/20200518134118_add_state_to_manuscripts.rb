# frozen_string_literal: true

class AddStateToManuscripts < ActiveRecord::Migration[6.0]
  def change
    add_column :manuscripts, :state, :string
  end
end
