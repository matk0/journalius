# frozen_string_literal: true

class CreateJournals < ActiveRecord::Migration[6.0]
  def change
    create_table :journals do |t|
      t.string :name
      t.string :print_issn
      t.string :electronic_issn

      t.timestamps
    end
  end
end
