class AddDoiToInstitutions < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :doi, :string
  end
end
