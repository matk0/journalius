# frozen_string_literal: true

class CreatePeerReviews < ActiveRecord::Migration[6.0]
  def change
    create_table :peer_reviews do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :manuscript, null: false, foreign_key: true
      t.string :invitee_email, required: true
      t.string :state
      t.text :message_for_editor
      t.text :message_for_author
      t.string :recommendation

      t.timestamps
    end
  end
end
