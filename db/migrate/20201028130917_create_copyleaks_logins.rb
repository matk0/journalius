class CreateCopyleaksLogins < ActiveRecord::Migration[6.0]
  def change
    create_table :copyleaks_logins do |t|
      t.text :access_token
      t.datetime :issued_at
      t.datetime :expires_at

      t.timestamps
    end
  end
end
