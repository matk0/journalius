# frozen_string_literal: true

class CreateJoinTableAbstractAndIndexingDatabasesJournals < ActiveRecord::Migration[6.0]
  def change
    create_table :abstract_and_indexing_databases_journals, id: false do |t|
      t.references :abstract_and_indexing_database, foreign_key: true, index: { name: 'index_ai_dbs_journals_on_abstract_and_indexing_databases_id' }
      t.references :journal, foreign_key: true
    end
  end
end
