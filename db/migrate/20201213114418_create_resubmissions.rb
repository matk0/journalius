class CreateResubmissions < ActiveRecord::Migration[6.0]
  def change
    create_table :resubmissions do |t|
      t.references :manuscript, null: false, foreign_key: true
    end
  end
end
