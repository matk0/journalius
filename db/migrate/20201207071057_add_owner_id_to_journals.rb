class AddOwnerIdToJournals < ActiveRecord::Migration[6.0]
  def change
    add_column :journals, :owner_id, :bigint
  end
end
