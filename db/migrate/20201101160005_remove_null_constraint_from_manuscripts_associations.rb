class RemoveNullConstraintFromManuscriptsAssociations < ActiveRecord::Migration[6.0]
  def change
    change_column_null :rejections, :manuscript_id, true
    change_column_null :acceptances, :manuscript_id, true
    change_column_null :revision_requests, :manuscript_id, true
  end
end
