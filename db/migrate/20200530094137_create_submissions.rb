# frozen_string_literal: true

class CreateSubmissions < ActiveRecord::Migration[6.0]
  def change
    create_table :submissions do |t|
      t.belongs_to :journal, null: false, foreign_key: true, index: true
      t.belongs_to :manuscript, null: false, foreign_key: true, index: true
      t.string :state

      t.timestamps
    end
  end
end
