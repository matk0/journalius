class AddOauthResponseToUsers < ActiveRecord::Migration[6.0]
  def change
    enable_extension "hstore"
    add_column :users, :oauth_response, :hstore
  end
end
