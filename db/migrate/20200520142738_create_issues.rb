# frozen_string_literal: true

class CreateIssues < ActiveRecord::Migration[6.0]
  def change
    create_table :issues do |t|
      t.integer :number
      t.belongs_to :volume

      t.timestamps
    end
  end
end
