class RemoveEditorialAssignments < ActiveRecord::Migration[6.0]
  def change
    drop_table :editorial_assignments
  end
end
