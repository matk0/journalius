# frozen_string_literal: true

class AddQuestionnaireAnswersToPeerReviews < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'hstore'
    add_column :peer_reviews, :questionnaire_answers, :hstore
    add_index :peer_reviews, :questionnaire_answers, using: :gin
  end
end
