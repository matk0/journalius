# frozen_string_literal: true

class AddPublisherKindToJournals < ActiveRecord::Migration[6.0]
  def change
    add_column :journals, :publisher_kind, :integer
  end
end
