# frozen_string_literal: true

class CreateChannels < ActiveRecord::Migration[6.0]
  def change
    create_table :channels do |t|
      t.string :name
      t.boolean :private
      t.boolean :direct
      t.belongs_to :manuscript, foreign_key: true
      t.belongs_to :institution, foreign_key: true

      t.timestamps
    end
  end
end
