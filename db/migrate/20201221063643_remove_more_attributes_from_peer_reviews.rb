class RemoveMoreAttributesFromPeerReviews < ActiveRecord::Migration[6.0]
  def change
    remove_column :peer_reviews, :message_for_author
    remove_column :peer_reviews, :message_for_editor
  end
end
