# frozen_string_literal: true

require 'faker'

### SUBJECTS
SubjectsImporter.call

### ABSTRACT AND INDEXING DATABASES
AbstractAndIndexingDatabase.create(name: "Scopus")
AbstractAndIndexingDatabase.create(name: "Crossref")

### ROLES
managing_editor_role = Role.create!(name: 'Managing Editor')
editor_role = Role.create!(name: 'Editor')
admin_role = Role.create!(name: 'Admin')

publisher = Institution.create!(name: "Journalius", doi: "12345", kind: "publisher")

corresponding_author = User.create!(email: 'author.journalius@gmail.com',
                                    title: "Dr.",
                                    first_name: "Kush",
                                    last_name: "High",
                                    confirmed_at: DateTime.now,
                                    password: 'password',
                                    password_confirmation: 'password')
co_author = CoAuthor.new(full_name: "Ing. Patrik Vrbovský")

managing_editor = User.create!(email: 'journal.adm.journalius@gmail.com',
                               title: "Dr.",
                               first_name: "Managing",
                               last_name: "Editor",
                               confirmed_at: DateTime.now,
                               password: 'password',
                               password_confirmation: 'password')
editor = User.create!(email: 'editor.journalius@gmail.com',
                      title: "Mr.",
                      first_name: "Just",
                      last_name: "Editor",
                      confirmed_at: DateTime.now,
                      password: 'password',
                      password_confirmation: 'password')

reviewer = User.create!(email: 'reviewer@journalius.com',
                        title: "Dr.",
                        first_name: "Peer",
                        last_name: "Reviewer",
                        confirmed_at: DateTime.now,
                        password: 'password',
                        password_confirmation: 'password')

journal = Journal.create!(title: "International Journal of Advanced Statistics and IT&C for Economics and Life Sciences",
                          publishing_model: :oa_no_apcs,
                          publisher: publisher,
                          subjects: [Subject.first, Subject.second],
                          languages: ["English", "Italian"],
                          published: true,
                          volumes_per_year: 2,
                          issues_per_volume: 10,
                          electronic_issn: '2049-3630',
                          print_issn: '2049-3630')

Assignment.create!(role: admin_role,
                   assignable: journal,
                   user: managing_editor)

volume = Volume.create!(number: Faker::Number.number(digits: 2),
                        journal: journal)

issue = Issue.create!(number: Faker::Number.number(digits: 2),
                      volume: volume)

manuscript = Manuscript.create!(issue: issue,
                                title: "Library and Information Science Vis-À-Vis Web Science in the Light of the OECD Fields of Science and Technology Classification",
                                published_at: Date.today - 2.weeks,
                                doi: "10.1000/xyz123",
                                corresponding_author: corresponding_author,
                                languages: ["English"],
                                journal: journal,
                                co_authors: [co_author],
                                body: "Hello",
                                docx: { io: File.open("spec/fixtures/Journal of KONES Powertrain and Transport.docx"),
                                        filename: "Manuscript.docx",
                                        content_type: "docx" },
                                additional_files: [{ io: File.open("spec/factories/files/Manuscript.pdf"),
                                                     filename: "Manuscript.pdf",
                                                     content_type: "application/pdf" }],
                                abstract: "<p>In a <i>million</i> stars!</p>")

Assignment.create(role: editor_role,
                  assignable: manuscript,
                  user: managing_editor)

Event.create(user: corresponding_author, eventable: manuscript, action: "submitted")
### ADMIN USERS
AdminUser.create!(email: 'admin@journalius.com',
                  password: 'password',
                  password_confirmation: 'password')
AdminUser.create!(email: ' cato@cato.sk',
                  password: 'catocato',
                  password_confirmation: 'catocato')
AdminUser.create!(email: 'alexionut.barbu@gmail.com',
                  password: 'alexalex',
                  password_confirmation: 'alexalex')

### PUBLISHERS
PublishersScraper.call
