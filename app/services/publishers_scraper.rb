# frozen_string_literal: true

require 'nokogiri'
require 'net/http'

class PublishersScraper < ApplicationService
  def initialize; end

  def call
    html = Nokogiri::HTML(Net::HTTP.get(URI.parse('https://www.crossref.org/06members/50go-live.html'))) do |config|
      config.options |= Nokogiri::XML::ParseOptions::HUGE
    end
    html.css("tr[bgcolor='white']").each_slice(100) do |slice|
      slice.each do |publisher|
        name = publisher.css('td')[0].text
        doi = publisher.css('td')[1].text
        if Institution.find_by(doi: doi).blank?
          Institution.create(name: name,
                             kind: 'publisher',
                             doi: doi)
        end
      end
    end
    html.css("tr[bgcolor='#EAEAEA']").each_slice(100) do |slice|
      slice.each do |publisher|
        name = publisher.css('td')[0].text
        doi = publisher.css('td')[1].text
        if Institution.find_by(doi: doi).blank?
          Institution.create(name: name,
                             kind: 'publisher',
                             doi: doi)
        end
      end
    end
  end
end
