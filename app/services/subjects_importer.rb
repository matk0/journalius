# frozen_string_literal: true

class SubjectsImporter < ApplicationService
  def initialize; end

  def call
    xlsx = Roo::Spreadsheet.open('./db/imports/subject_tree.xlsx')
    sheet = xlsx.sheet('Sheet1')
    sheet.each do |row|
      subjects = row[0].split(':').map(&:squish)

      if subjects.size == 1
        Subject.create(name: subjects[0])
      elsif subjects.size == 2
        root_subject = Subject.find_by(name: subjects[0])
        if root_subject.present?
          root_subject.children.create(name: subjects[1])
        else
          root_subject = Subject.create(name: subjects[0])
          root_subject.children.create(name: subjects[1])
        end
      elsif subjects.size == 3
        root_subject = Subject.find_by(name: subjects[0])
        if root_subject.present?
          sub_root_subject = Subject.find_by(name: subjects[1])
          if sub_root_subject.present?
            sub_root_subject.children.create(name: subjects[2])
          else
            sub_root_subject = root_subject.children.create(name: subjects[1])
            sub_root_subject.children.create(name: subjects[2])
          end
        else
          root_subject = Subject.create(name: subjects[0])
          sub_root_subject = root_subject.children.create(name: subjects[1])
          sub_root_subject.children.create(name: subjects[2])
        end
      end
    end
  end
end
