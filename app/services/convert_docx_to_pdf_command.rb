# frozen_string_literal: true

class ConvertDocxToPdfCommand
  def initialize(manuscript_id)
    @manuscript = Manuscript.find manuscript_id
  end

  def execute
    @manuscript.file.open do |f|
      result = ConvertApi.convert('pdf', File: f.path)
      result.file.save('file.pdf')
      @manuscript.pdf.attach(io: File.open('file.pdf'), filename: 'file.pdf')
    end
  end
end
