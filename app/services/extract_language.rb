# frozen_string_literal: true

require 'aws-sdk'

class ExtractLanguage
  def initialize(text)
    @text = text
    @comprehend = Aws::Comprehend::Client.new(
      access_key_id: Rails.application.credentials.dig(:aws, :access_key_id),
      secret_access_key: Rails.application.credentials.dig(:aws, :secret_access_key),
      region: 'eu-west-1'
    )
  end

  def run!
    puts @comprehend.detect_dominant_language(text: @text)
  end
end
