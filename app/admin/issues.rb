# frozen_string_literal: true

ActiveAdmin.register Issue do
  permit_params :number, :volume_id

  show do
    attributes_table do
      row :number
      row 'Volume' do
        issue.volume.number
      end
    end
  end

  form do |f|
    f.semantic_errors
    inputs do
      f.input :number
      f.input :volume_id, as: :select, collection: Volume.all.map { |v| [v.number, v.id] }
    end

    f.actions
  end

  index do
    selectable_column
    column :id do |issue|
      link_to issue.number, admin_issue_path(issue)
    end
    column :volume do |issue|
      issue.volume.number
    end
    actions
  end
end
