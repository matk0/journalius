# frozen_string_literal: true

ActiveAdmin.register Journal do
  permit_params :name, :institution_id

  show do
    attributes_table do
      row :name
      row :institution
      row :electronic_issn
      row :print_issn
      row 'Volumes' do
        journal.volumes.map(&:number).join(', ')
      end
    end
  end
end
