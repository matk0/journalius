# frozen_string_literal: true

ActiveAdmin.register Volume do
  permit_params :number, :journal_id

  show do
    attributes_table do
      row :number
      row :journal
      row 'Issues' do
        volume.issues.map(&:number).join(', ')
      end
    end
  end
end
