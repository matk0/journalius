# frozen_string_literal: true

ActiveAdmin.register Manuscript do
  show do
    attributes_table do
      row :title
      row :corresponding_author
      row :authors
      row :state
      row :updated_at
      row :created_at
    end
  end
end
