# frozen_string_literal: true

ActiveAdmin.register Institution do
  permit_params :name
end
