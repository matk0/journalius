# frozen_string_literal: true

ActiveAdmin.register User do
  permit_params :email,
                :full_name,
                :password,
                :password_confirmation,
                assignments_attributes: %i[id role_id user_id journal_id]

  form do |f|
    f.semantic_errors(*f.object.errors.keys)

    f.inputs 'User' do
      f.input :email
      f.input :full_name
      f.input :password
      f.input :password_confirmation
      f.has_many :assignments, allow_destroy: true do |nf|
        nf.input :role_id, label: 'Role', as: :select, collection: Role.all.map { |r| [r.name, r.id] }
        nf.input :journal_id, label: 'Journal', as: :select, collection: Journal.order(:title).map { |r| [r.title, r.id] }
      end
    end

    f.actions
  end

  index do
    selectable_column
    column :email do |u|
      link_to u.email, admin_user_path(u)
    end
    column 'Journals and Roles' do |u|
      u.roles.map { |r| "#{r.assignments.find_by(user_id: u.id).journal.title} [#{r.name}]" }
    end
  end
end
