class Users::InvitationsController < Devise::InvitationsController
  def create
    request.format = :js
    @result = Invitations::External.call user_params: user_params,
                                         invitable_id: params["invitable_id"],
                                         invitable_type: params["invitable_type"],
                                         current_user: current_user,
                                         invited_as_id: params["invited_as_id"]
  end

  def destroy
    @invitation = @user.internal_invitations.last
    @invitation.decline!
    super
  end

  private

    def user_params
      password = SecureRandom.base64(8)
      params.require(:user).permit(:title, :first_name, :last_name, :email).merge({password: password, password_confirmation: password})
    end
end
