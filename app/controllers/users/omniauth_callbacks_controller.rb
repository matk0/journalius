# frozen_string_literal: true

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def google_oauth2
    @user = User.from_omniauth google_params
    @user.update oauth_response: auth.to_h

    if @user.persisted?
      sign_out_all_scopes
      flash[:success] = t 'devise.omniauth_callbacks.success', kind: 'Google'
      sign_in_and_redirect @user, event: :authentication
    else
      flash[:alert] = t 'devise.omniauth_callbacks.failure', kind: 'Google', reason: "#{auth.info.email} is not authorized."
      redirect_to new_user_session_path
    end
  end

  def linkedin
    @user = User.from_omniauth linkedin_params
    @user.update oauth_response: auth.to_h

    if @user.persisted?
      sign_in_and_redirect @user, event: :authentication
      set_flash_message(:notice, :success, kind: "Linkedin") if is_navigational_format?
    else
      session["devise.linkedin_data"] = request.env["omniauth.auth"].except(:extra) # Removing extra as it can overflow some session stores
      redirect_to new_user_registration_url
    end
  end

  def orcid
    @user = User.from_omniauth orcid_params
    @user.update oauth_response: auth.to_h

    if @user.persisted?
      sign_in_and_redirect @user, event: :authentication
      set_flash_message(:notice, :success, kind: "Orcid") if is_navigational_format?
    else
      redirect_to new_user_registration_url
    end
  end
  
  def failure
    redirect_to root_path
  end

  protected

  def after_omniauth_failure_path_for(_scope)
    new_user_session_path
  end

  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope) || root_path
  end

  private

  def google_params
    @google_params ||= {
      provider: auth.provider,
      uid: auth.uid,
      email: auth.info.email,
      first_name: auth.info.first_name,
      last_name: auth.info.last_name,
      avatar_url: auth.info.image
    }
  end

  def linkedin_params
    @linkedin_params ||= {
      provider: auth.provider,
      uid: auth.uid,
      email: auth.info.email,
      first_name: auth.info.first_name,
      last_name: auth.info.last_name,
      avatar_url: auth.info.picture_url
    }
  end

  def orcid_params
    @orcid_params ||= {
      provider: auth.provider,
      uid: auth.uid,
      email: orcid_email,
      first_name: auth.info.first_name,
      last_name: auth.info.last_name,
      avatar_url: nil
    }
  end

  def orcid_email
    if auth.info.email.present?
      auth.info.email
    else
      random_email = "#{rand(1..1000000000)}@orcid.com"
      user = User.find_by_email random_email
      while user.present?
        random_email = "#{rand(1..1000000000)}@orcid.com"
        user = User.find_by_email random_email
      end
      random_email
    end
  end

  def auth
    @auth ||= request.env['omniauth.auth']
  end
end
