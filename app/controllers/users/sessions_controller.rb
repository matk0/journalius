# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  prepend_before_action :check_captcha, only: [:create] unless Rails.env.development?

  def create
    params[:user].merge!(remember_me: 1)
    super
  end

  def after_sign_in_path resource_or_scope
    stored_location_for(resource_or_scope) || journals_path
  end

  def after_sign_out_path_for resource_or_scope
    new_session_path resource_or_scope
  end

  private

    def check_captcha
      unless verify_recaptcha
        self.resource = resource_class.new sign_in_params
        respond_with_navigational(resource) { render :new }
      end 
    end
  
end
