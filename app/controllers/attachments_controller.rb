# frozen_string_literal: true

class AttachmentsController < ApplicationController
  before_action :authenticate_user!

  def create
    request.format = :js
    @journal = Journal.find(params["journal_id"])
    @attachment = ActionText::Attachable.from_attachable_sgid params["attachable_sgid"]
    @journal.cover.attach @attachment
    # TODO: AUTHORIZE!!!
    # authorize @attachment
  end

  def destroy
    @attachment = ActiveStorage::Attachment.find params[:id]
    # TODO: AUTHORIZE!!!
    # authorize @attachment
    @attachment.purge_later
    respond_to do |format|
      case @attachment.name
      when "docx"
        format.js { render "attachments/destroy_docx" }
      when "cover"
        @journal = @attachment.record
        format.js { render "attachments/destroy_journal_cover" }
      else
        format.js { render "attachments/destroy_additional_file" }
      end
    end
  end
end
