class GlobalSearchesController < ApplicationController
  after_action :verify_authorized, except: [:new]
  after_action :verify_policy_scoped, only: :index

  def new
    request.format = :json
    if params[:q].present?
      @manuscripts = Manuscript.published.where('title ILIKE ?', "%#{params[:q]}%").limit(5)
      @journals = Journal.where('title ILIKE ?', "%#{params[:q]}%").limit(5)
    end
  end
end
