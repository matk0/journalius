# frozen_string_literal: true

class RejectionsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def create
    @rejection = Rejection.new rejection_params
    authorize @rejection
    result = Creators::CreateRejection.call({ rejection: @rejection, executor: current_user })

    respond_to do |format|
      if result.success?
        format.html do
          flash[:notice] = 'Manuscript was rejected and the author notified.'
          redirect_to manuscript_overview_path @rejection.manuscript
        end
      else
        format.html do
          flash[:notice] = result.errors.to_s
          redirect_to manuscript_overview_path @rejection.manuscript
        end
      end
    end
  end

  private

  def rejection_params
    params.require(:rejection).permit(:manuscript_id, :text)
  end
end
