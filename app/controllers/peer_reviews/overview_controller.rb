# frozen_string_literal: true

class PeerReviews::OverviewController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def show
    @peer_review = PeerReview.find(params["peer_review_id"])
    authorize @peer_review
  end
end
