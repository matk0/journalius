# frozen_string_literal: true

class PeerReviewsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_peer_review, only: %i[show edit update destroy]
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def index
    @peer_reviews = policy_scope(PeerReview)
  end

  def show
    authorize @peer_review
  end

  def new
    @peer_review = PeerReview.new
  end

  def edit; end

  def create
    @peer_review = PeerReview.new peer_review_params
    @peer_review.manuscript_id = @manuscript.id
    authorize @peer_review

    respond_to do |format|
      result = Creators::CreatePeerReview.call({ peer_review: @peer_review })
      if result.success?
        format.html { redirect_to journal_manuscript_peer_reviews_path(params[:journal_id], params[:manuscript_id]), notice: 'Peer Review was successfully created.' }
        format.js { render }
      else
        format.html { render :new }
        format.js { render }
      end
    end
  end

  def update
    request.format = :js
    authorize @peer_review

    if @peer_review.update(peer_review_params)
      render
    else
      render
    end
  end

  def destroy
    @peer_review.destroy
    respond_to do |format|
      format.html { redirect_to peer_reviews_url, notice: 'Peer Review was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def submit
    request.format = :js
    @peer_review = PeerReview.find params[:peer_review_id]
    authorize @peer_review
    @peer_review.submit!
    flash.now[:alert] = "Your review has been submitted."
    redirect_to peer_review_overview_path @peer_review
  end

  private

  def set_peer_review
    @peer_review = PeerReview.find params[:id]
  end

  def peer_review_params
    params.require(:peer_review).permit(:manuscript_id, :user_id, :recommendation, :message_for_author, :message_for_editor, :questionnaire_id, :invitee_email)
  end
end
