# frozen_string_literal: true

class InstitutionsController < ApplicationController
  before_action :set_institution, only: [:show]
  after_action :verify_authorized, except: %i[show new create index]

  def new
    @institution = Institution.new
    @institution.build_address
    @institution.build_billing_address
    @institution.users.build
  end

  def create
    @institution = Institution.new institution_params

    respond_to do |format|
      if @institution.save
        format.html { redirect_to journals_path, notice: 'Congratulations! Your institution was successfully registered.' }
        format.js { flash.now[:alert] = "#{@institution.name} was created." }
      else
        format.html { render :new }
        format.js { flash.now[:alert] = "There was a problem!" }
      end
    end
  end

  def show; end

  def index
    @institutions = Institution.paginate(page: params[:page])
  end

  private

  def set_institution
    @institution = Institution.find(params[:id])
  end

  def institution_params
    params.require(:institution).permit(:name, :kind, :iban, :account_name, :bic, :billing_name, :vat_no,
                                        address_attributes: %i[line_1 line_2 city zipcode country],
                                        billing_address_attributes: %i[line_1 line_2 city zipcode country],
                                        users_attributes: %i[full_name email password password_confirmation])
  end
end
