# frozen_string_literal: true

class MessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_channel
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  def create
    @message = @channel.messages.create(message_params)
    authorize @message
    MessageChannel.broadcast_to @channel, message: render_to_string(@message)
    UnreadsChannel.broadcast_to @channel, current_user_id: current_user.id
  end

  private

  def message_params
    params.require(:message).permit(:body).merge(user: current_user)
  end

  def set_channel
    @channel = current_user.channels.find params[:channel_id]
  end
end
