# frozen_string_literal: true

class HomesController < ApplicationController
  before_action :redirect_to_dashboard_if_signed_in
  def show
  end

  def about_us
  end

  def privacy_policy
  end

  private

  def redirect_to_dashboard_if_signed_in
    redirect_to journals_path if signed_in?
  end
end
