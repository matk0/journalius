# frozen_string_literal: true

class PayersController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  def create
    @payer = Institution.new payer_params
    @journal = Journal.find params["journal_id"]

    authorize @payer

    result = Payers::Create.call(payer: @payer,
                                 journal: @journal,
                                 user: current_user)

    respond_to do |format|
      if result.success?
        format.js { flash.now[:alert] = "#{@payer.name} was created and assigned as payer." }
      else
        format.js { flash.now[:alert] = "There was a problem!" }
      end
    end
  end

  private

  def payer_params
    params.require(:institution).permit(:name, :kind)
  end
end
