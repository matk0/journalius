# frozen_string_literal: true

class IssuesController < InheritedResources::Base
  decorates_assigned :issues, :volume

  before_action :authenticate_user!
  before_action :set_volume, only: [:index, :show, :create]
  before_action :set_journal, only: [:index, :show, :create]
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def index
    @issues = policy_scope(@volume.issues)
    add_index_breadcrumbs
  end

  def create
    @issue = Issue.new(number: number,
                       volume: @volume)
    authorize @issue

    respond_to do |format|
      if @issue.save
        format.html { redirect_to journal_volume_issues_path(@journal, @volume), notice: 'Issue was successfully created.' }
        format.json { render :show, status: :created, location: @issue }
      else
        format.html { render :new }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def number
    if @volume.issues.last.present?
      @volume.issues.last.number + 1
    else
      1
    end
  end

  def set_journal
    @journal = Journal.find params[:journal_id]
  end

  def set_volume
    @volume = Volume.find(params[:volume_id]).decorate
  end

  def add_index_breadcrumbs
    add_breadcrumb @journal.title.to_s, journal_volumes_path(@journal)
    add_breadcrumb @volume.title.to_s, journal_volume_issues_path(@journal, @volume)
  end
end
