# frozen_string_literal: true

class InternalInvitationsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :notify_invitee_of_withdrawal, only: :destroy

  def create
    request.format = :js
    @invitation = InternalInvitation.new internal_invitation_params
    authorize @invitation

    @result = Invitations::Internal.call(invitation: @invitation)
  end

  def destroy
    request.format = :js
    @invitation = InternalInvitation.find params["id"]
    authorize @invitation

    @invitation.invitee.destroy if @invitation.raw_invitation_token.present?
    @invitation.destroy
  end
  
  def accept
    @invitation = InternalInvitation.find params["internal_invitation_id"]
    authorize @invitation

    if @invitation.state == "pending"
      @invitation.accept!

      case @invitation.invitable_type
      when "Journal"
        flash[:alert] = "You have accepted your invitation."
        redirect_to journal_overview_path(@invitation.invitable)
      when "Manuscript"
        flash[:alert] = "You have accepted your invitation."
        redirect_to manuscript_overview_path(@invitation.invitable)
      end
    else
      case @invitation.invitable_type
      when "Journal"
        flash[:alert] = "This invitation cannot be accepted anymore."
        redirect_to journals_path
      when "Manuscript"
        flash[:alert] = "This invitation cannot be accepted anymore."
        redirect_to manuscripts_path
      end
    end

  rescue ActiveRecord::RecordNotFound
    redirect_to withdrawn_path
  end

  def decline
    @invitation = InternalInvitation.find params["internal_invitation_id"]
    authorize @invitation

    if @invitation.state == "pending"
      @invitation.decline!

      case @invitation.invitable_type
      when "Journal"
        flash[:alert] = "You have declined your invitation."
        redirect_to journals_path
      when "Manuscript"
        flash[:alert] = "You have declined your invitation."
        redirect_to manuscripts_path
      end
    else
      case @invitation.invitable_type
      when "Journal"
        flash[:alert] = "This invitation cannot be declined anymore."
        redirect_to journals_path
      when "Manuscript"
        flash[:alert] = "This invitation cannot be declined anymore."
        redirect_to manuscripts_path
      end
    end

  rescue ActiveRecord::RecordNotFound
    redirect_to withdrawn_path
  end

  def withdrawn
  end

  private

  def internal_invitation_params
    params.require(:internal_invitation).permit(:id, :invitee_id, :inviter_id, :invited_as_id, :invitable_id, :invitable_type)
  end

  def notify_invitee_of_withdrawal
    if @invitation.state == "pending"
      InvitationsMailer.with(invitation: @invitation).notify_invitee_of_withdrawal.deliver_now
    end
  end
end
