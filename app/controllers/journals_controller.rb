# frozen_string_literal: true

class JournalsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_journal, only: [:show, :edit, :update]
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def index
    @journal = Journal.new
    @journals = policy_scope(Journal).paginate(page: params[:page]).decorate
  end

  def show
    authorize @journal
    @manuscripts = if current_user
                     @journal.manuscripts.includes(:corresponding_author)
                   else
                     @journal.manuscripts.includes(:corresponding_author).published
                   end
  end

  def new
    @journal = Journal.new
    authorize @journal
  end

  def create
    request.format = :js
    @journal = Journal.new pruned_journal_params

    authorize @journal
    @result = Journals::Create.call(journal: @journal, current_user: current_user)

    if @result.success?
      @journals = policy_scope(Journal.includes([:publisher])).paginate(page: params[:page]).decorate
    end
  end

  def edit
    authorize @journal
  end

  def update
    @journal.expected_months_of_issue
    authorize @journal
    
    respond_to do |format|
      if @journal.update update_params
        @journal.check_for_new_institution_admins current_user
        format.html { redirect_to journal_overview_path(@journal), notice: "#{@journal.title} was successfully updated." }
        format.js { flash.now[:alert] = "#{@journal.title} was successfully updated." }
      else
        format.html { render :edit }
        format.js { flash.now[:alert] = "There was a problem!" }
      end
    end
  end

  private

  def journal_params
    params.require(:journal).permit(:title, :subtitle, :institution_id, :publisher_is_owner, :payer_is_owner, :payer_is_publisher,
                                    :webpage, :publisher_id, :payer_id, :owner_id,
                                    :print_issn, :electronic_issn,
                                    :volumes_per_year, :issues_per_volume,
                                    :editorial_board, :advisory_board, :archiving_policy,
                                    :editorial_information, :aims_and_scope,
                                    :publishing_model, :guidelines_for_reviewers, :peer_review_type,
                                    :open_access_creative_commons, :custom_copyrights_policy,
                                    :guidelines_for_authors, :oa_statement, :plagiarism_policy, :ethical_policy,
                                    :publishing_frequency, expected_months_of_issue: [],
                                    assignments_attributes: %i[role_id user_id id _destroy],
                                    abstract_and_indexing_database_ids: [], languages: [], subject_ids: [])
  end

  def pruned_journal_params
    journal_params.except(:publisher_is_owner, :payer_is_owner, :payer_is_publisher)
  end

  def update_params
    p = pruned_journal_params
    p.merge!({expected_months_of_issue: []}) if switching_to_continuos_publishing?
    p.merge!({publisher_id: @journal.owner_id}) if publisher_is_owner?
    p.merge!({payer_id: @journal.owner_id}) if payer_is_owner?
    p.merge!({payer_id: @journal.publisher_id}) if payer_is_publisher?
    p
  end

  def set_journal
    @journal = Journal.find(params["id"]).decorate
  end

  def switching_to_continuos_publishing?
    journal_params[:publishing_frequency] == "continuous_publishing"
  end

  def publisher_is_owner?
    journal_params[:publisher_is_owner] == "true"
  end

  def payer_is_owner?
    journal_params[:payer_is_owner] == "true"
  end

  def payer_is_publisher?
    journal_params[:payer_is_publisher] == "true"
  end
end
