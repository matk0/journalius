require 'docx'

# frozen_string_literal: true

class ManuscriptsController < ApplicationController
  before_action :authenticate_user!, only: %i[edit update show]
  before_action :set_manuscript, only: %i[show update destroy accept_for_publishing publish]
  before_action :set_journal, only: %i[new create]
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def index
    @manuscripts = policy_scope(Manuscript).decorate
  end

  def show
    authorize @manuscript
    @events = EventsDecorator.decorate(Event.includes([:user]).where(eventable_type: "Manuscript", eventable_id: @manuscript.id).order('created_at DESC'))
    # @doc = Docx::document.open url_for(@manuscript.docx)
    @channel = Channel.find_by(manuscript: @manuscript)
  end

  def new
    @manuscript = @journal.manuscripts.new
    authorize @manuscript
    @manuscript.build_corresponding_author
  end

  def create
    @manuscript = @journal.manuscripts.new manuscript_params
    authorize @manuscript

    if (verify_recaptcha(model: @manuscript) or current_user.present?)
      @manuscript.corresponding_author = current_user if current_user
      result = Creators::CreateManuscript.call({ manuscript: @manuscript })

      respond_to do |format|
        if result.success?
          sign_in @manuscript.corresponding_author
          format.html { redirect_to manuscripts_path, notice: "#{@manuscript.title} has been successfully submitted to #{@journal.title}" }
        else
          format.html { render :new }
        end
      end
    else
      render :new
    end
  end

  def edit
    @manuscript = Manuscript.includes(additional_files_attachments: [:blob]).find(params[:id]).decorate
    authorize @manuscript
  end

  def update
    authorize @manuscript
    respond_to do |format|
      if @manuscript.update manuscript_params
        format.html { redirect_to manuscript_overview_path(@manuscript), notice: 'Manuscript was successfully updated.' }
        format.json { render :show, status: :ok, location: @manuscript }
      else
        format.html { render :edit }
        format.json { render json: @manuscript.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @manuscript
    @manuscript.destroy
    respond_to do |format|
      format.html { redirect_to manuscripts_url, notice: 'Manuscript was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def accept_for_publishing
    @manuscript.accept_for_publishing!
    respond_to do |format|
      format.html { redirect_to journal_manuscript_path(params[:journal_id], @manuscript), notice: 'Manuscript was accepted for publishing and the author notified.' }
      format.json { head :no_content }
    end
  end

  def publish
    @manuscript.update(published_at: Date.today)
    respond_to do |format|
      format.html { redirect_to journal_manuscript_path(params[:journal_id], @manuscript), notice: 'Manuscript was published.' }
      format.json { head :no_content }
    end
  end

  private

  def check_captcha
    unless verify_recaptcha
      self.resource = resource_class.new sign_up_params
      resource.validate # Look for any other validation errors besides reCAPTCHA
      set_minimum_password_length
      respond_with_navigational(resource) { render :new }
    end 
  end

  def set_manuscript
    @manuscript = Manuscript.includes(peer_reviews: [:reviewer]).find(params[:id]).decorate
  end

  def manuscript_params
    params.require(:manuscript).permit(:id, :journal_id,
                                       :title, :abstract,
                                       :body, :docx,
                                       :references,
                                       keyword_list: [],
                                       subject_ids: [],
                                       languages: [],
                                       additional_files: [],
                                       co_authors: [],
                                       co_authors_attributes: %i[full_name id _destroy],
                                       corresponding_author_attributes: %i[title first_name last_name email password password_confirmation])
  end

  def add_index_breadcrumbs
    add_breadcrumb @journal.title.to_s, journal_volumes_path(@journal)
    add_breadcrumb @volume.title.to_s, journal_volume_issues_path(@journal, @volume)
    add_breadcrumb @issue.title.to_s, journal_volume_issue_manuscripts_path(@journal, @volume, @issue)
  end

  def resource_name
    :user
  end
  helper_method :resource_name
 
  def resource
    @resource ||= User.new
  end
  helper_method :resource
 
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
  helper_method :devise_mapping
 
  def resource_class
    User
  end
  helper_method :resource_class
end
