# frozen_string_literal: true

class PinsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  def create
    request.format = :js
    @pin = Pin.new pin_params
    authorize @pin
    @pin.save!
  end

  def destroy
    request.format = :js
    @pin = Pin.find params["id"]
    authorize @pin
    @pin.destroy
  end

  private

  def pin_params
    params.require(:pin).permit(:pinnable_id, :pinnable_type).merge(user_id: current_user.id)
  end
end
