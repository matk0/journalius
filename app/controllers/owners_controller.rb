# frozen_string_literal: true

class OwnersController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  def create
    @owner = Institution.new owner_params
    @journal = Journal.find params["journal_id"]

    authorize @owner

    result = Owners::Create.call(owner: @owner,
                                 journal: @journal,
                                 user: current_user)

    respond_to do |format|
      if result.success?
        format.js { flash.now[:alert] = "#{@owner.name} was created and assigned as journal owner." }
      else
        format.js { flash.now[:alert] = "There was a problem!" }
      end
    end
  end

  private

  def owner_params
    params.require(:institution).permit(:name, :kind, address_attributes: [:country])
  end
end
