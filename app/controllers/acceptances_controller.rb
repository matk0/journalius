# frozen_string_literal: true

class AcceptancesController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  def create
    @manuscript = Manuscript.find params["id"]
    authorize @manuscript, :accept?
    result = AcceptManuscript.call({ manuscript: @manuscript, executor: current_user })

    respond_to do |format|
      if result.success?
        format.html do
          flash[:notice] = 'Manuscript was accepted for peer review and the author notified via email.'
          redirect_to manuscript_overview_path @manuscript
        end
      else
        format.html do
          flash[:notice] = result.errors.to_s
          redirect_to manuscript_overview_path @manuscript
        end
      end
    end
  end
end
