# frozen_string_literal: true

class DoisController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  def create
    request.format = :js
    @manuscript = Manuscript.find manuscript_params[:id]
    authorize @manuscript, :generate_doi?
    @manuscript.generate_doi
    render
  end

  private

  def manuscript_params
    params.require(:manuscript).permit(:id)
  end
end
