# frozen_string_literal: true

class ChannelsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  def new
    @channel = Channel.new
    authorize @channel
    @manuscript = Manuscript.find params[:manuscript_id]
  end

  def create
    @channel = Channel.new channel_params
    @channel.name = "Discussing #{@channel.manuscript.title}"
    @channel.users << current_user
    authorize @channel

    respond_to do |format|
      if @channel.save
        current_user.update_attribute('last_visited_channel_id', @channel.id)
        format.html { redirect_to channel_path(@channel) }
      else
        format.html { render :new }
      end
    end
  end

  def show
    set_channel
    authorize @channel
    @channel_user = UserDecorator.decorate current_user.channel_users.find_by(channel: @channel)
    @last_read_at = @channel_user&.last_read_at || @channel.created_at
    @channel_user&.touch(:last_read_at)
    current_user.update_attribute('last_visited_channel_id', @channel.id)
  end

  def index
    @channels = policy_scope(Channel)
  end

  private

  def channel_params
    params.require(:channel).permit(:name, :manuscript_id, user_ids: [])
    # .merge(institution_id: current_user.institution.id)
  end

  def set_channel
    @channel = Channel.includes([messages: [:user]]).find(params[:id])
  end
end
