# frozen_string_literal: true

class TechnicalEditsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_technical_edit, only: %i[show edit update destroy]
  before_action :set_manuscript, only: %i[edit new update index create]
  before_action :set_journal, only: %i[edit new update index]
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def index
    @technical_edits = TechnicalEdit.all
  end

  def show; end

  def new
    @technical_edit = TechnicalEdit.new
  end

  def edit; end

  def create
    @technical_edit = TechnicalEdit.new technical_edit_params
    @technical_edit.manuscript_id = @manuscript.id

    respond_to do |format|
      result = TechnicalEdits::CreateTechnicalEdit.call({ technical_edit: @technical_edit })
      if result.success?
        format.html { redirect_to journal_manuscript_technical_edits_path(params[:journal_id], params[:manuscript_id]), notice: 'Peer Review was successfully created.' }
        format.json { render :show, status: :created, location: @technical_edit }
      else
        format.html { render :new }
        format.json { render json: result.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @technical_edit.update(technical_edit_params)
        format.html { redirect_to journal_manuscript_technical_edits_path(params[:journal_id], params[:manuscript_id]), notice: 'Peer Review was successfully updated.' }
        format.json { render :show, status: :ok, location: @technical_edit }
      else
        format.html { render :edit }
        format.json { render json: @technical_edit.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @technical_edit.destroy
    respond_to do |format|
      format.html { redirect_to technical_edits_url, notice: 'Peer Review was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_technical_edit
    @technical_edit = TechnicalEdit.find(params[:id])
  end

  def set_journal
    @journal = Journal.find params[:journal_id]
  end

  def set_manuscript
    @manuscript = Manuscript.find params[:manuscript_id]
  end

  def technical_edit_params
    params.require(:technical_edit).permit(:manuscript_id, :user_id, :invitee_email)
  end
end
