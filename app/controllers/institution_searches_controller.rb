# frozen_string_literal: true

class InstitutionSearchesController < ApplicationController
  before_action :authenticate_user!
  # after_action :verify_policy_scoped, only: :index
  # after_action :verify_authorized, except: :index

  def new
    request.format = :json
    @institutions = Institution.where('name ILIKE ? OR doi ILIKE ?', "%#{params[:q]}%", "%#{params[:q]}%").order(:name).limit(30)
  end
end
