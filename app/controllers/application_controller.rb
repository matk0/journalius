# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_paper_trail_whodunnit

  private

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform this action.'
    redirect_to(request.referer || root_path)
  end

  def set_journal
    @journal = Journal.find params[:journal_id]
  end

  def set_institution
    @institution = Institution.find params[:institution_id]
  end

  protected

    def configure_permitted_parameters
     devise_parameter_sanitizer.permit(:sign_up, keys: [:title, :first_name, :last_name])
    end
end
