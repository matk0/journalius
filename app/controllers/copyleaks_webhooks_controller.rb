class CopyleaksWebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token

  def completed
    scan = PlagiarismScan.find params["id"]
    scan.update(completed_payload: params)
    Apis::Copyleaks::ExportReportToPdf
  end

  def error
    request.format = :js
    scan = PlagiarismScan.find params["id"]
    scan.update(error_payload: params)
  end

  def creditsChecked
    request.format = :js
    scan = PlagiarismScan.find params["id"]
    scan.update(credits_check_payload: params)
  end

  def indexed
    request.format = :js
    scan = PlagiarismScan.find params["id"]
    scan.update(indexed_payload: params)
  end

  def export_completed
    request.format = :js
  end
end
