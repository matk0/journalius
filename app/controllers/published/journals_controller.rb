# frozen_string_literal: true

class Published::JournalsController < ApplicationController
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def index
    @journals = policy_scope [:published, Journal]
  end

  def show
    @journal = Journal.find params[:id]
    @manuscripts = @journal.manuscripts.published
    authorize [:published, @journal]
  end
end
