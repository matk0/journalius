# frozen_string_literal: true

class Manuscripts::PeerReviewsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def show
    @manuscript = Manuscript.find(params["manuscript_id"]).decorate
    authorize @manuscript
    @peer_review_invitation = InternalInvitation.new(invited_as_id: $reviewer.id,
                                                     inviter: current_user,
                                                     invitable_id: @manuscript.id,
                                                     invitable_type: "Manuscript")
  end
end
