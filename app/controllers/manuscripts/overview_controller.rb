# frozen_string_literal: true

class Manuscripts::OverviewController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def show
    @manuscript = Manuscript.find(params["manuscript_id"]).decorate
    authorize @manuscript
  end
end
