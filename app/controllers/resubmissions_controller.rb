# frozen_string_literal: true

class ResubmissionsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def create
    @resubmission = Resubmission.new resubmission_params
    authorize @resubmission
    result = Resubmissions::Create.call(resubmission: @resubmission, executor: current_user)

    respond_to do |format|
      if result.success?
        format.html do
          flash[:notice] = "Manuscript's editor was notified of your resubmission."
          redirect_to manuscript_overview_path @resubmission.manuscript
        end
      else
        format.html do
          flash[:notice] = result.errors.to_s
          redirect_to manuscript_overview_path @resubmission.manuscript
        end 
      end
    end
  end

  private

  def resubmission_params
    params.require(:resubmission).permit(:manuscript_id, :text)
  end
end
