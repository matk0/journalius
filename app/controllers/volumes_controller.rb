# frozen_string_literal: true

class VolumesController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :set_volume, only: %i[show edit update destroy]
  before_action :set_journal, only: [:index, :show, :create]
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def index
    @volumes = policy_scope(@journal.volumes).decorate
    add_index_breadcrumbs
  end

  def show
    @issues = @volume.issues
    add_show_breadcrumbs
  end

  def new
    @volume = Volume.new
  end

  def edit; end

  def create
    @volume = Volume.new(number: number,
                         journal: @journal)
    authorize @volume

    respond_to do |format|
      if @volume.save
        format.html { redirect_to journal_volumes_path(params[:journal_id]), notice: 'Volume was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def destroy
    @volume.destroy
    respond_to do |format|
      format.html { redirect_to volumes_url, notice: 'Volume was successfully destroyed.' }
    end
  end

  private

  def set_journal
    @journal = Journal.find params[:journal_id]
  end

  def set_volume
    @volume = Volume.find(params[:id])
  end

  def add_index_breadcrumbs
    add_breadcrumb @journal.title.to_s, journal_volumes_path(@journal)
  end

  def add_show_breadcrumbs
    add_breadcrumb @journal.title.to_s, journal_volumes_path(@journal)
    add_breadcrumb @volume.number.to_s, journal_volume_issues_path(@journal, @volume)
  end

  def number
    if @journal.volumes.last.present?
      @journal.volumes.last.number + 1
    else
      1
    end
  end
end
