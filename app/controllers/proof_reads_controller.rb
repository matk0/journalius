# frozen_string_literal: true

class ProofReadsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_proof_read, only: %i[show edit update destroy]
  before_action :set_manuscript, only: %i[edit new update index create]
  before_action :set_journal, only: %i[edit new update index]
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def index
    @proof_reads = ProofRead.all
  end

  def show; end

  def new
    @proof_read = ProofRead.new
  end

  def edit; end

  def create
    @proof_read = ProofRead.new proof_read_params
    @proof_read.manuscript_id = @manuscript.id

    respond_to do |format|
      result = ProofReads::CreateProofRead.call({ proof_read: @proof_read })
      if result.success?
        format.html { redirect_to journal_manuscript_proof_reads_path(params[:journal_id], params[:manuscript_id]), notice: 'Proof Read was successfully created.' }
        format.json { render :show, status: :created, location: @proof_read }
      else
        format.html { render :new }
        format.json { render json: result.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @proof_read.update(proof_read_params)
        format.html { redirect_to journal_manuscript_proof_reads_path(params[:journal_id], params[:manuscript_id]), notice: 'Proof Read was successfully updated.' }
        format.json { render :show, status: :ok, location: @proof_read }
      else
        format.html { render :edit }
        format.json { render json: @proof_read.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @proof_read.destroy
    respond_to do |format|
      format.html { redirect_to proof_reads_url, notice: 'Proof Read was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_proof_read
    @proof_read = ProofRead.find(params[:id])
  end

  def set_journal
    @journal = Journal.find params[:journal_id]
  end

  def set_manuscript
    @manuscript = Manuscript.find params[:manuscript_id]
  end

  def proof_read_params
    params.require(:proof_read).permit(:manuscript_id, :user_id, :invitee_email)
  end
end
