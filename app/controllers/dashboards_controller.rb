# frozen_string_literal: true

class DashboardsController < ApplicationController
  before_action :authenticate_user!
  def show
    manuscript_pins = Pin.where(user: current_user, pinnable_type: "Manuscript")
    @manuscripts = current_user.manuscripts.where(id: manuscript_pins.pluck(:pinnable_id))
    journal_pins = Pin.where(user: current_user, pinnable_type: "Journal")
    if current_user.role? :admin
      @journals = current_user.journals.where(id: journal_pins.pluck(:pinnable_id))
    else
      @journals = Journal.published.where(id: journal_pins.pluck(:pinnable_id))
    end
  end
end
