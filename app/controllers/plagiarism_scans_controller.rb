# frozen_string_literal: true

class PlagiarismScansController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def create
    plagiarism_scan = PlagiarismScan.new manuscript_id: plagiarism_scan_params[:manuscript_id]
    authorize plagiarism_scan
    plagiarism_scan.save
    PlagiarismScanJob.perform_later plagiarism_scan.id
  end

  private

  def plagiarism_scan_params
    params.permit(:manuscript_id)
  end
end
