# frozen_string_literal: true

class KeywordSearchesController < ApplicationController
  after_action :verify_authorized, except: :new
  after_action :verify_policy_scoped, except: :new

  def new
    request.format = :json
    @keywords = ActsAsTaggableOn::Tag.where('name ILIKE ?', "%#{params[:q]}%").order(:name).limit(30)
  end
end
