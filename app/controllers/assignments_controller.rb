# frozen_string_literal: true

class AssignmentsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  def create
    request.format = :js
    @assignment = Assignment.new assignment_params
    authorize @assignment
    @manuscript = Manuscript.find(@assignment.assignable_id) if @assignment.assignable_type == "Manuscript"
    @assignment.save!
    render
  end

  def destroy
    request.format = :js
    @assignment = Assignment.find params["id"]
    authorize @assignment
    @assignment.delete
    render
  end

  private

  def assignment_params
    params.require(:assignment).permit(:assignable_type,
                                       :assignable_id,
                                       :user_id,
                                       :role_id)
  end


end
