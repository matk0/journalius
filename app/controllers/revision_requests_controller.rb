# frozen_string_literal: true

class RevisionRequestsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def create
    @revision_request = RevisionRequest.new revision_request_params
    authorize @revision_request
    result = Creators::CreateRevisionRequest.call({ revision_request: @revision_request, executor: current_user })

    respond_to do |format|
      if result.success?
        format.html do
          flash[:notice] = 'The author was requested to revise the manuscript.'
          redirect_to manuscript_overview_path @revision_request.manuscript
        end
      else
        format.html do
          flash[:notice] = result.errors.to_s
          redirect_to manuscript_overview_path @revision_request.manuscript
        end 
      end
    end
  end

  private

  def revision_request_params
    params.require(:revision_request).permit(:manuscript_id, :text)
  end
end
