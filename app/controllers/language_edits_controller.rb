# frozen_string_literal: true

class LanguageEditsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_language_edit, only: %i[show edit update destroy]
  before_action :set_manuscript, only: %i[edit new update index create]
  before_action :set_journal, only: %i[edit new update index]
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def index
    @language_edits = LanguageEdit.all
  end

  def show; end

  def new
    @language_edit = LanguageEdit.new
  end

  def edit; end

  def create
    @language_edit = LanguageEdit.new language_edit_params
    @language_edit.manuscript_id = @manuscript.id

    respond_to do |format|
      result = LanguageEdits::CreateLanguageEdit.call({ language_edit: @language_edit })
      if result.success?
        format.html { redirect_to journal_manuscript_language_edits_path(params[:journal_id], params[:manuscript_id]), notice: 'Peer Review was successfully created.' }
        format.json { render :show, status: :created, location: @language_edit }
      else
        format.html { render :new }
        format.json { render json: result.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @language_edit.update(language_edit_params)
        format.html { redirect_to journal_manuscript_language_edits_path(params[:journal_id], params[:manuscript_id]), notice: 'Peer Review was successfully updated.' }
        format.json { render :show, status: :ok, location: @language_edit }
      else
        format.html { render :edit }
        format.json { render json: @language_edit.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @language_edit.destroy
    respond_to do |format|
      format.html { redirect_to language_edits_url, notice: 'Peer Review was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_language_edit
    @language_edit = LanguageEdit.find(params[:id])
  end

  def set_journal
    @journal = Journal.find params[:journal_id]
  end

  def set_manuscript
    @manuscript = Manuscript.find params[:manuscript_id]
  end

  def language_edit_params
    params.require(:language_edit).permit(:manuscript_id, :user_id, :invitee_email)
  end
end
