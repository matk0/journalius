# frozen_string_literal: true

class PublishersController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  def create
    @publisher = Institution.new publisher_params
    @journal = Journal.find params["journal_id"]

    authorize @publisher

    result = Publishers::Create.call(publisher: @publisher,
                                     journal: @journal,
                                     user: current_user)

    respond_to do |format|
      if result.success?
        format.js { flash.now[:alert] = "#{@publisher.name} was created and assigned as publisher." }
      else
        format.js { flash.now[:alert] = "There was a problem!" }
      end
    end
  end

  private

  def publisher_params
    params.require(:institution).permit(:name, :doi).merge({kind: "publisher"})
  end
end
