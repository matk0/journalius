# frozen_string_literal: true

class CoAuthorsController < ApplicationController
  def destroy
    request.format = :js
    @co_author = CoAuthor.find(params[:id])
    @co_author.destroy
    render
  end
end
