# frozen_string_literal: true

class BlobsController < ApplicationController
  def destroy
    blob = ActiveStorage::Blob.find(params[:id])
    blob.purge_later
    render json: { id: params[:id], signedId: blob.signed_id }
  end
end
