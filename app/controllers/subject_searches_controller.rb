# frozen_string_literal: true

class SubjectSearchesController < ApplicationController
  after_action :verify_authorized, except: :new
  after_action :verify_policy_scoped, only: :new

  def new
    request.format = :json
    @subjects = policy_scope(Subject).where('name ILIKE ?', "%#{params[:q]}%").order(:id).limit(30)
  end
end
