# frozen_string_literal: true

class Journals::FilesController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def show
    @journal = Journal.find(params["journal_id"]).decorate
    authorize @journal
  end
end
