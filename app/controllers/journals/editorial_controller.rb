# frozen_string_literal: true

class Journals::EditorialController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def show
    @journal = Journal.find(params["journal_id"]).decorate
    @managing_editor_invitation = InternalInvitation.new(invited_as_id: $managing_editor.id,
                                                         inviter: current_user,
                                                         invitable_id: @journal.id,
                                                         invitable_type: "Journal")
    @editor_in_chief_invitation  = InternalInvitation.new(invited_as_id: $editor_in_chief.id,
                                                          inviter: current_user,
                                                          invitable_id: @journal.id,
                                                          invitable_type: "Journal")
    @editor_invitation  = InternalInvitation.new(invited_as_id: $editor.id,
                                                 inviter: current_user,
                                                 invitable_id: @journal.id,
                                                 invitable_type: "Journal")
    authorize @journal
  end
end
