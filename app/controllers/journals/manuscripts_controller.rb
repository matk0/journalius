# frozen_string_literal: true

class Journals::ManuscriptsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index

  def show
    @journal = Journal.find(params["journal_id"]).decorate
    @manuscripts = if current_user
                     @journal.manuscripts.includes(:corresponding_author)
                   else
                     @journal.manuscripts.includes(:corresponding_author).published
                   end
    authorize @journal
  end
end
