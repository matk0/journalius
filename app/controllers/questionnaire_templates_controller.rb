# frozen_string_literal: true

class QuestionnaireTemplatesController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_policy_scoped, only: :index
  after_action :verify_authorized, except: :index



  def new
    @questionnaire_template = QuestionnaireTemplate.new
    authorize @questionnaire_template
  end

  def create
    @questionnaire_template = QuestionnaireTemplate.new questionnaire_template_params
    authorize @questionnaire_template

    respond_to do |format|
      if @questionnaire_template.save
        format.html { redirect_to institution_questionnaire_templates_path(@institution), notice: 'Questionnaire was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def show
    @questionnaire_template = QuestionnaireTemplate.find(params["id"]).decorate
    authorize @questionnaire_template
  end

  def index
    @questionnaire_templates = policy_scope QuestionnaireTemplate.all
  end

  def edit
    @questionnaire_template = QuestionnaireTemplate.find(params["id"])
    authorize @questionnaire_template
  end

  private

  def questionnaire_template_params
    params.require(:questionnaire_template).permit(:name, questions_attributes: %i[id text field_type required questionnaire_template_id _destroy])
  end
end
