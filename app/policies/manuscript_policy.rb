# frozen_string_literal: true

class ManuscriptPolicy < ApplicationPolicy
  attr_reader :user, :manuscript

  def initialize user, manuscript
    @user = user
    @manuscript = manuscript
  end

  def accept?
    user.in? manuscript.all_editors
  end

  def reject?
    user.in? manuscript.all_editors
  end

  def ask_for_revision?
    user.in? manuscript.all_editors
  end

  def new?
    true
  end

  def create?
    true
  end

  def show?
    if manuscript.published?
      true
    else
      user == manuscript.corresponding_author or
      user.in? manuscript.all_editors
    end
  end

  def edit?
    user.in? manuscript.all_editors or
    (user == manuscript.corresponding_author and manuscript.state == 'in_revision')
  end

  def update?
    user.in? manuscript.all_editors or
    (user == manuscript.corresponding_author and manuscript.state == 'in_revision')
  end

  def destroy?
    user == manuscript.corresponding_author or user.in? manuscript.all_editors
  end

  def generate_doi?
    user.in? manuscript.all_editors
  end

  def preview?
    user.in? manuscript.all_editors
  end

  def discuss?
    user.in? manuscript.all_editors
  end

  def resubmit?
    if @manuscript.revision_requests.any? and user == manuscript.corresponding_author
      @manuscript.updated_at > @manuscript.revision_requests.last.created_at
    else
      false
    end
  end

  class Scope < Scope
    def resolve
      editorial_assignments = Assignment.where(user_id: user.id, assignable_type: "Manuscript").pluck(:id)
      admin_assignments = user.journals.includes([:manuscripts]).map{|j| j.manuscripts.map(&:id) }.flatten
      authors = Manuscript.where(corresponding_author_id: user.id)
      scope.includes([:corresponding_author, :journal]).where(id: editorial_assignments + admin_assignments + authors)
    end
  end
end
