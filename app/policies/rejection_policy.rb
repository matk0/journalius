# frozen_string_literal: true

class RejectionPolicy < ApplicationPolicy
  attr_reader :user, :rejection

  def initialize(user, rejection)
    @user = user
    @rejection = rejection
  end

  def create?
    user.in? rejection.manuscript.all_editors
  end
end
