# frozen_string_literal: true

class VolumePolicy < ApplicationPolicy
  attr_reader :user, :volume

  def initialize user, volume
    @user = user
    @volume = volume
  end

  def create?
    user.in? volume.journal.admins
  end

  class Scope < Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
