# frozen_string_literal: true

class Published::JournalPolicy < ApplicationPolicy
  attr_reader :user, :journal

  def initialize(user, journal)
    @user = user
    @journal = journal
  end

  def new?
    user.role? 'managing_editor'
  end

  def show?
    true
  end

  class Scope < Scope
    def resolve
      scope.published
    end
  end
end
