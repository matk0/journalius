# frozen_string_literal: true

class RevisionRequestPolicy < ApplicationPolicy
  attr_reader :user, :revision_request

  def initialize user, revision_request
    @user = user
    @revision_request = revision_request
  end

  def create?
    user.in? revision_request.manuscript.all_editors
  end
end
