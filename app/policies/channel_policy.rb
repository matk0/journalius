# frozen_string_literal: true

class ChannelPolicy < ApplicationPolicy
  attr_reader :user, :channel

  def initialize(user, channel)
    @user = user
    @channel = channel
  end

  ### TODO: FIX POLICIES!!!
  def new?
    true
  end

  def create?
    true
  end

  def show?
    true
  end

  class Scope < Scope
    def resolve
      scope.where(id: ChannelUser.where(user: user).pluck(:channel_id))
    end
  end
end
