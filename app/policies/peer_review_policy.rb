# frozen_string_literal: true

class PeerReviewPolicy < ApplicationPolicy
  attr_reader :user, :peer_review

  def initialize user, peer_review
    @user = user
    @peer_review = peer_review
  end

  def update?
    if peer_review.state == "in_progress"
      user.in? peer_review.manuscript.all_editors or
      user == peer_review.reviewer
    else
      user.in? peer_review.manuscript.all_editors
    end
  end

  def create?
    user.in? peer_review.manuscript.all_editors
  end

  def show?
    user == peer_review.reviewer or
    user.in? peer_review.manuscript.all_editors
  end

  def submit?
    if peer_review.state == "in_progress"
      user == peer_review.reviewer
    end
  end

  class Scope < Scope
    def resolve
      user.peer_reviews
    end
  end
end
