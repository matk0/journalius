# frozen_string_literal: true

### TODO: Finalize authorization rules here

class QuestionnaireTemplatePolicy < ApplicationPolicy
  attr_reader :user, :questionnaire_template

  def initialize(user, questionnaire_template)
    @user = user
    @questionnaire_template = questionnaire_template
  end

  def new?
    true
    # user.institution == questionnaire_template.institution
  end

  def create?
    true
    # user.institution == questionnaire_template.institution
  end

  def show?
    true
  end

  def edit?
    true
  end

  def update?
    true
  end

  class Scope < Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
