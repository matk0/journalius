# frozen_string_literal: true

class PinPolicy < ApplicationPolicy
  attr_reader :user, :pin

  def initialize user, pin
    @user = user
    @pin = pin
  end

  def create?
    true
  end

  def destroy?
    pin.user == user
  end
end
