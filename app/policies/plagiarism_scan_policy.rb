# frozen_string_literal: true

class PlagiarismScanPolicy < ApplicationPolicy
  attr_reader :user, :plagiarism_scan

  def initialize user, plagiarism_scan
    @user = user
    @plagiarism_scan = plagiarism_scan
  end

  def create?
    user.in? plagiarism_scan.manuscript.all_editors
  end
end
