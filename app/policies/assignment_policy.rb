# frozen_string_literal: true

class AssignmentPolicy < ApplicationPolicy
  attr_reader :user, :assignment

  def initialize user, assignment
    @user = user
    @assignment = assignment
  end

  def create?
    case assignment.assignable_type
    when "Journal"
      journal = Journal.find assignment.assignable_id
      case assignment.role_id
      when $editor_in_chief.id
        user.in? journal.admins
      when $managing_editor.id
        user.in? journal.admins or
        user.in? journal.editors_in_chief
      when $editor.id
        user.in? journal.admins or
        user.in? journal.editors_in_chief or
        user.in? journal.managing_editors
      else
        false
      end
    when "Manuscript"
      assignment.assignable.journal.in? user.journals
    end
  end
  
  def destroy?
    case assignment.assignable_type
    when "Journal"
      journal = Journal.find assignment.assignable_id
      case assignment.role_id
      when $editor_in_chief.id
        user.in? journal.admins
      when $managing_editor.id
        user.in? journal.admins or
        user.in? journal.editors_in_chief
      when $editor.id
        user.in? journal.admins or
        user.in? journal.editors_in_chief or
        user.in? journal.managing_editors
      else
        false
      end
    when "Manuscript"
      assignment.assignable.journal.in? user.journals
    end
  end
end
