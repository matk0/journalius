# frozen_string_literal: true

class ResubmissionPolicy < ApplicationPolicy
  attr_reader :user, :resubmission

  def initialize user, resubmission
    @user = user
    @resubmission = resubmission
  end

  def create?
    user == resubmission.manuscript.corresponding_author
  end
end
