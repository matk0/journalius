# frozen_string_literal: true

class InternalInvitationPolicy < ApplicationPolicy
  attr_reader :user, :internal_invitation

  def initialize user, internal_invitation
    @user = user
    @internal_invitation = internal_invitation
  end

  def create?
    case internal_invitation.invitable_type
    when "Journal"
      journal = Journal.find internal_invitation.invitable_id
      case internal_invitation.invited_as_id
      when $editor_in_chief.id
        user.in? journal.admins
      when $managing_editor.id
        user.in? journal.admins or
        user.in? journal.editors_in_chief
      when $editor.id
        user.in? journal.admins or
        user.in? journal.editors_in_chief or
        user.in? journal.managing_editors
      else
        false
      end
    when "Manuscript"
      manuscript = Manuscript.find internal_invitation.invitable_id
      case internal_invitation.invited_as_id
      when $reviewer.id
        user.in? manuscript.all_editors
      end
    end
  end

  def destroy?
    case internal_invitation.invitable_type
    when "Journal"
      journal = Journal.find internal_invitation.invitable_id
      case internal_invitation.invited_as_id
      when $editor_in_chief.id
        user.in? journal.admins
      when $managing_editor.id
        user.in? journal.admins or
        user.in? journal.editors_in_chief
      when $editor.id
        user.in? journal.admins or
        user.in? journal.editors_in_chief or
        user.in? journal.managing_editors
      else
        false
      end
    when "Manuscript"
      manuscript = Manuscript.find internal_invitation.invitable_id
      case internal_invitation.invited_as_id
      when $reviewer.id
        user.in? manuscript.all_editors
      end
    end
  end

  def decline?
    user == internal_invitation.invitee
  end

  def accept?
    user == internal_invitation.invitee
  end
end
