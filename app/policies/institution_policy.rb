# frozen_string_literal: true

class InstitutionPolicy < ApplicationPolicy
  attr_reader :user, :institution

  def initialize user, institution
    @user = user
    @institution = institution
  end

  def create?
    user.role? :admin
  end
end
