# frozen_string_literal: true

class IssuePolicy < ApplicationPolicy
  attr_reader :user, :issue

  def initialize user, issue
    @user = user
    @issue = issue
  end

  def create?
    user.role? :managing_editor and issue.volume.journal.institution.users.include?(user)
  end

  class Scope < Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
