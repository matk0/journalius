# frozen_string_literal: true

class MessagePolicy < ApplicationPolicy
  attr_reader :user, :message

  def initialize(user, message)
    @user = user
    @message = message
  end

  def create?
    message.channel.users.include? user
  end

  class Scope < Scope
    def resolve; end
  end
end
