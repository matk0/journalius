# frozen_string_literal: true

class JournalPolicy < ApplicationPolicy
  attr_reader :user, :journal

  def initialize(user, journal)
    @user = user
    @journal = journal
  end

  ### TODO: set proper authorization rules

  def show?
    journal.published == true or
    user.in? journal.admins or
    user.in? journal.editors_in_chief or
    user.in? journal.managing_editors or
    user.in? journal.editors
  end

  def new?
    true
    # user.role? :managing_editor and user.institution == journal.institution
  end

  def create?
    true
    # user.role? :managing_editor and user.institution == journal.institution
  end

  def edit?
    Assignment.where(assignable: journal, user: user, role_id: $admin.id).present?
  end

  def update?
    user.in? journal.admins
  end

  class Scope < Scope

    def resolve
      journal_ids = Assignment.where(assignable_type: "Journal", user_id: user.id).pluck(:assignable_id)
      Journal.includes([:publisher, :journals_subjects, :subjects]).where(id: journal_ids, published: false).or(Journal.includes([:publisher, :journals_subjects, :subjects]).where(id: journal_ids, published: nil)).or(Journal.includes([:publisher, :journals_subjects, :subjects]).where(published: true))
    end
  end
end
