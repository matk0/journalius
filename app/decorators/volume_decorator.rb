# frozen_string_literal: true

class VolumeDecorator < Draper::Decorator
  delegate_all

  def title
    "Volume no.#{number}"
  end
end
