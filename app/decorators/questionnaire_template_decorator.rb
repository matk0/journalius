class QuestionnaireTemplateDecorator < Draper::Decorator
  delegate_all

  decorates_association :questions
end
