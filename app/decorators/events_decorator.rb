# frozen_string_literal: true

class EventsDecorator < Draper::Decorator
  delegate_all

  def submitted_on
  end
end
