# frozen_string_literal: true

class ManuscriptDecorator < Draper::Decorator
  delegate_all
  decorates_association :corresponding_author

  def submitted_on
    created_at.strftime "Submitted on %d.%m.%Y" + " to #{journal.title}"
  end

  def days_from_submission
    "#{((Time.zone.now - created_at) / 1.day).to_i}"
  end

  def time_from_submission_to_first_decision
    if first_decision.present?
      h.distance_of_time_in_words(created_at, first_event.created_at)
    else
      "N/A"
    end
  end

  def time_from_submission_to_publication
    if published_at
      h.distance_of_time_in_words(created_at, published_at)
    else
      "N/A"
    end
  end

  def time_from_last_action
    if last_event.present?
      h.distance_of_time_in_words(last_event.created_at, Time.now)
    else
      "N/A"
    end
  end
end
