class QuestionDecorator < Draper::Decorator
  delegate_all

  def humanized_field_type
    case field_type
    when "boolean"
      "True or false"
    when "text_field"
      "Text field"
    else
      "Unhumanized field type. Please contact Mat."
    end
  end
end
