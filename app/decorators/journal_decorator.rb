# frozen_string_literal: true

class JournalDecorator < Draper::Decorator
  delegate_all

  def no_of_manuscripts_in_review
    manuscripts.where(state: "in_review").count
  end

  def average_time_to_publication
    "XYZ"
  end

  def humanized_publishing_model
    case publishing_model
    when "oa_no_apcs"
      "OA no APCs"
    when "oa_apcs"
      "OA APCs"
    when "paid_access"
      "Paid Access"
    end
  end

  def humanized_languages
    if languages.present?
      languages.join(", ")
    else
      ""
    end
  end

  def humanized_publishing_frequency
    if publishing_frequency == "continuous_publishing"
      "Continuous publishing with #{volumes_per_year} volumes per year"
    else
      "#{volumes_per_year} #{I18n.t(:volume, count: volumes_per_year)} per year with #{issues_per_volume} #{I18n.t(:issue, count: issues_per_volume)} per volume."
    end
  end
end

