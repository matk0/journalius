class UserDecorator < Draper::Decorator
  delegate_all

  def full_name
    if first_name and last_name
      "#{title} #{first_name} #{last_name}"
    else
      email
    end
  end

  def initials
    if first_name and last_name
      first_name[0] + last_name[0]
    else
      user.email[0]
    end
  end
end
