# frozen_string_literal: true

class IssueDecorator < Draper::Decorator
  delegate_all

  def title
    "Issue no.#{number}"
  end
end
