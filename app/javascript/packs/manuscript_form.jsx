import React from 'react';
import ReactDOM from 'react-dom';
import LanguagesSelect from "../components/LanguagesSelect";
import KeywordsInput from "../components/KeywordsInput";
import SubjectsSelect from "../components/SubjectsSelect";
import Uppy from "@uppy/core";
import Dashboard from "@uppy/dashboard";
import ActiveStorageUpload from "@excid3/uppy-activestorage-upload"

var LanguagesSelectComponent = document.getElementById("languages-select");
if (LanguagesSelectComponent) {
	var languages = JSON.parse(
		LanguagesSelectComponent.getAttribute("languages")
	);
	var options = JSON.parse(LanguagesSelectComponent.getAttribute("options"));
	var forModel = LanguagesSelectComponent.getAttribute("for");

	ReactDOM.render(
		<LanguagesSelect
			languages={languages}
			options={options}
			forModel={forModel}
		/>,
		LanguagesSelectComponent
	);
}

var keywordsInputComponent = document.getElementById("keywords-input");
if (keywordsInputComponent) {
	var keywords = JSON.parse(keywordsInputComponent.getAttribute("keywords"));
	var forModel = keywordsInputComponent.getAttribute("for_model");

	ReactDOM.render(
		<KeywordsInput keywords={keywords} forModel={forModel} />,
		keywordsInputComponent
	);
}

var subjectsSelectComponent = document.getElementById("subjects-select");

if (subjectsSelectComponent) {
	var subjects = JSON.parse(
		subjectsSelectComponent.getAttribute("subjects")
	);
	var forModel = subjectsSelectComponent.getAttribute("for_model");
	ReactDOM.render(
		<SubjectsSelect subjects={subjects} forModel={forModel} />,
		subjectsSelectComponent
	);
}

document
	.querySelectorAll("[data-uppy]")
	.forEach((element) => setupUppy(element));

function setupUppy(target) {
	let direct_upload_url = document
		.querySelector("meta[name='direct-upload-url']")
		.getAttribute("content");
	let fieldName = target.dataset.uppy;
	let maxNumberOfFiles = target.classList.contains("upload-docx") ? 1 : 100
	let minNumberOfFiles = target.classList.contains("upload-docx") ? 1 : 0
	let allowedFileTypes = target.classList.contains("upload-docx") ? [".docx"] : ["image/*", ".pdf", ".doc", ".docx"]

	const uppy = new Uppy({
		restrictions: { allowedFileTypes, maxNumberOfFiles, minNumberOfFiles },
		autoProceed: true,
	})
		.use(Dashboard, {
			inline: true,
			proudlyDisplayPoweredByUppy: false,
			target,
			showRemoveButtonAfterComplete: true
		})
		.use(ActiveStorageUpload, {
			directUploadUrl: direct_upload_url,
		})

	uppy.on("complete", (result) => {
		result.successful.forEach((file) => {
			appendUploadedFile(target, file, fieldName);
		});
	})

	function appendUploadedFile(target, file, fieldName) {
		const hiddenField = document.createElement("input");

		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", fieldName);
		hiddenField.setAttribute("data-pending-upload", true);
		hiddenField.setAttribute("value", file.response.signed_id);

		target.appendChild(hiddenField);
	}

	uppy.on('file-removed', (file, reason) => {
		if (reason === 'removed-by-user') {
			fetch(`/blobs/${file.response.id}.js`, {
				method: "DELETE",
				headers: new Headers({
					"X-CSRF-TOKEN": document.getElementsByName("csrf-token")[0].content,
				}),
			}).then((res) => res.json())
				.then((data) => {
					Array.from(
						document.querySelectorAll(`input[value=${data.signedId}]`)
					).forEach((element) => element.remove());
				});
		}
	})
}
