// RAILS
require("@rails/ujs").start();
require("turbolinks").start();
require("@rails/activestorage").start();
require("channels");
require("@rails/actiontext");
require("trix");
require("jquery");

require("../../assets/dashkit/js/charts.js")

// LIBS
import $ from 'jquery';
global.$ = jQuery;
import "bootstrap/dist/js/bootstrap";
import "@fortawesome/fontawesome-free/js/all";
import "controllers" // Stimulus
import React from "react";
import ReactDOM from 'react-dom';
import GlobalSearchForm from "../components/GlobalSearchForm";
import Chart from 'chart.js';

// DYNAMIC FORMS
$(document).on("turbolinks:load", function () {
  $("body").on("click", "form .remove_fields", function (event) {
    $(this).prev("input[type=hidden]").val("1");
    $(this).closest(".form-row").remove();
    return event.preventDefault();
  });

  $("body").on("click", "form .add_fields", function (event) {
    var regexp, time;
    time = new Date().getTime();
    regexp = new RegExp($(this).data("id"), "g");
    $(this).before($(this).data("fields").replace(regexp, time));
    return event.preventDefault();
  });
});

// GLOBAL SEARCH FORM
$(document).on("turbolinks:load", function () {
	var GlobalSearchComponent = document.getElementById("global-search-form");
	if (GlobalSearchComponent) {

		ReactDOM.render(
			<GlobalSearchForm
			/>,
			GlobalSearchComponent
		);
	}
})

// BOOTSTRAP
$(document).on("turbolinks:load", function () {
  $("body").tooltip({
    selector: '[data-toggle="tooltip"]',
    container: "body",
  });

  $(function () {
    $('[data-toggle="popover"]').popover({
      container: "body",
      trigger: "hover",
    });
  });
	$('.toast').toast({ delay: 3000 })
	$('#toast').toast('show')
});

$(document).on("turbolinks:load", function () {
	var ctx = $('#overviewChart');
	if (ctx.length > 0) {
		var myChart = new Chart(ctx, {
			type: 'line',
			data: {
				labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
				datasets: [{
					label: 'Earned',
					data: [0, 10, 5, 15, 10, 20, 15, 25, 20, 30, 25, 40]
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							callback: function(value) {
								return value + 'k';
							}
						}
					}]
				}
			}
		})
	}
})
