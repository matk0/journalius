import React from 'react';
import ReactDOM from 'react-dom';
import PeerReviewRecommendation from '../components/PeerReviewRecommendation';

var peerReviewRecommendationComponent = document.getElementById("recommendation");
if (peerReviewRecommendationComponent ) {
	var recommendation = peerReviewRecommendationComponent.getAttribute(
		"recommendation"
	);
	var id = peerReviewRecommendationComponent.getAttribute("peer_review_id");

	ReactDOM.render(
		<PeerReviewRecommendation id={id} recommendation={recommendation}/>,
		peerReviewRecommendationComponent
	);
}
