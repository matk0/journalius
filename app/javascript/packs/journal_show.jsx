import React from 'react';
import ReactDOM from 'react-dom';
import JournalSubjectsSelect from "../components/JournalSubjectsSelect";
import PublishingFrequencySelect from "../components/PublishingFrequencySelect";
import PublisherSelect from "../components/PublisherSelect";
import PayerSelect from "../components/PayerSelect";
import OwnerSelect from "../components/OwnerSelect";
import LanguagesSelect from "../components/LanguagesSelect";
import PrintIssnInput from "../components/PrintIssnInput";
import ElectronicIssnInput from "../components/ElectronicIssnInput";
import Uppy from "@uppy/core";
import Dashboard from "@uppy/dashboard";
import ActiveStorageUpload from "@excid3/uppy-activestorage-upload"

document
	.querySelectorAll(".upload-journal-cover")
	.forEach((element) => setupUppy(element));

function setupUppy(target) {
	let trigger = target.querySelector('[data-behavior="uppy-trigger"]')
	let direct_upload_url = document
		.querySelector("meta[name='direct-upload-url']")
		.getAttribute("content");

	const uppy = new Uppy({
		restrictions: {
			allowedFileTypes: ["image/*"],
			maxNumberOfFiles: 1,
			minNumberOfFiles: 1,
		},
		autoProceed: true,
		allowMultipleUploads: false,
	})
		.use(Dashboard, {
			proudlyDisplayPoweredByUppy: false,
			trigger,
			closeAfterFinish: true,
		})
		.use(ActiveStorageUpload, {
			directUploadUrl: direct_upload_url,
		})

	uppy.on("complete", (result) => {
		result.successful.forEach((file) => {
			appendUploadedFile(target, file)
		});
	})

	function appendUploadedFile(target, file) {
		$.ajax({
			url: "/attachments",
			type: 'post',
			headers: {
				"X-CSRF-TOKEN": document.getElementsByName("csrf-token")[0].content,
			},
			data: {
				journal_id: target.id,
				attachable_sgid: file.response.attachable_sgid 
			},
			success: function(data, status) {}
		})
	}
}

var subjectsSelectComponent = document.getElementById("journal-subjects-select");

if (subjectsSelectComponent) {
	var subjects = JSON.parse(
		subjectsSelectComponent.getAttribute("subjects")
	);
	var forModel = subjectsSelectComponent.getAttribute("for_model");
	var isValid = subjectsSelectComponent.getAttribute("is_valid");
	var journalId = subjectsSelectComponent.getAttribute("journal_id")
	ReactDOM.render(
		<JournalSubjectsSelect subjects={subjects} forModel={forModel} journalId={journalId} />,
		subjectsSelectComponent
	);
}

var publishingFrequencyComponent = document.getElementById(
	"publishing-frequency-component"
);
if (publishingFrequencyComponent) {
	var publishingFrequency = publishingFrequencyComponent.getAttribute(
		"publishing_frequency"
	);
	var expectedMonthsOfIssue = publishingFrequencyComponent.getAttribute(
		"expected_months_of_issue"
	);
	ReactDOM.render(
		<PublishingFrequencySelect
			publishingFrequency={publishingFrequency}
			expectedMonthsOfIssue={expectedMonthsOfIssue}
		/>,
		publishingFrequencyComponent
	);
}

var publisherComponent = document.getElementById("publisher-select");

if (publisherComponent) {
	var publisher = JSON.parse(publisherComponent.getAttribute("publisher"));
	var ownerId = publisherComponent.getAttribute("owner_id");
	var ownerName = publisherComponent.getAttribute("owner_name");

	ReactDOM.render(
		<PublisherSelect
			publisher={publisher}
			ownerId={ownerId}
			ownerName={ownerName}
			/>,
		publisherComponent
	);
}

var payerComponent = document.getElementById("payer-select");

if (payerComponent) {
	var payer = JSON.parse(payerComponent.getAttribute("payer"));
	var ownerId = payerComponent.getAttribute("owner_id");
	var ownerName = payerComponent.getAttribute("owner_name");
	var publisherId = payerComponent.getAttribute("publisher_id");
	var publisherName = payerComponent.getAttribute("publisher_name");

	ReactDOM.render(
		<PayerSelect
			payer={payer}
			ownerId={ownerId}
			ownerName={ownerName}
			publisherId={publisherId}
			publisherName={publisherName}
			/>,
		payerComponent
	);
}

var ownerComponent = document.getElementById("owner-select");

if (ownerComponent) {
	var owner = JSON.parse(ownerComponent.getAttribute("owner"));

	ReactDOM.render(
		<OwnerSelect
			owner={owner}
			/>,
		ownerComponent
	);
}

var LanguagesSelectComponent = document.getElementById("languages-select");
if (LanguagesSelectComponent) {
	var languages = JSON.parse(
		LanguagesSelectComponent.getAttribute("languages")
	);
	var options = JSON.parse(LanguagesSelectComponent.getAttribute("options"));
	var forModel = LanguagesSelectComponent.getAttribute("for");

	ReactDOM.render(
		<LanguagesSelect
			languages={languages}
			options={options}
			forModel={forModel}
		/>,
		LanguagesSelectComponent
	);
}

var printIssnComponent = document.getElementById("print-issn");
if (printIssnComponent) {
	var printIssn = printIssnComponent.getAttribute("print_issn");
	var errors = JSON.parse(printIssnComponent.getAttribute("errors"));

	ReactDOM.render(
		<PrintIssnInput printIssn={printIssn} errors={errors} />,
		printIssnComponent
	);
}

var electronicIssnComponent = document.getElementById("electronic-issn");
if (electronicIssnComponent) {
	var electronicIssn = electronicIssnComponent.getAttribute(
		"electronic_issn"
	);
	var errors = JSON.parse(electronicIssnComponent.getAttribute("errors"));

	ReactDOM.render(
		<ElectronicIssnInput electronicIssn={electronicIssn} errors={errors} />,
		electronicIssnComponent
	);
}
