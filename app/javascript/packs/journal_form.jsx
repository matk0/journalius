import React from 'react';
import ReactDOM from 'react-dom';
import SubjectsSelect from "../components/SubjectsSelect";

var subjectsSelectComponent = document.getElementById("subjects-select");

if (subjectsSelectComponent) {
	var subjects = JSON.parse(
		subjectsSelectComponent.getAttribute("subjects")
	);
	var forModel = subjectsSelectComponent.getAttribute("for_model");
	var isValid = subjectsSelectComponent.getAttribute("is_valid");
	ReactDOM.render(
		<SubjectsSelect subjects={subjects} forModel={forModel} />,
		subjectsSelectComponent
	);
}
