import React from "react";
import AsyncSelect from "react-select/async";

class OwnerSelect extends React.Component {
	state = { 
		owner: this.props.owner
	};

	loadOptions = async (query, callback) => {
		const response = await fetch(`/institution_searches/new.json?q=${query}`);
		const json = await response.json();

		callback(
			json.institutions.map((institution) => ({
				label: institution.label,
				value: institution.value,
			}))
		);
	};

	onChange = (selectedOwner) => {
		this.setState({ owner: selectedOwner });
	};

	render() {
		return (
			<div className="row">
				<div className="col-12">
					<div className="form-group">
						<h4>Institutions</h4>
						<AsyncSelect
							defaultOptions
							name={`journal[owner_id]`}
							value={this.state.owner}
							onChange={this.onChange}
							placeholder="Search by name or DOI"
							loadOptions={this.loadOptions}
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default OwnerSelect;
