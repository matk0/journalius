import React, { useState } from "react";

function PeerReviewRecommendation(props) {
	const [recommendation, setRecommendation] = useState(props.recommendation);

	const handleChange = value => {
		$.ajax({
			url: `/peer_reviews/${props.id}`,
			type: "patch",
			headers: {
				"X-CSRF-TOKEN": document.getElementsByName("csrf-token")[0].content,
			},
			data: {
				peer_review: {
					recommendation: value
				}
			},
			success: function(data, success) {setRecommendation(value)}
		})
		
	}

	return (
		<div className="d-flex align-items-start">
			{recommendation == "recommend_to_accept" ?
				<label className="btn btn-success mr-3">
					<i className="fe fe-check-circle pr-2"/>
					Accept
				</label> :
					<label className="btn btn-outline-success mr-3" onClick={() => handleChange("recommend_to_accept")}>
						<i className="fe fe-check-circle pr-2"/>
						Accept
					</label>}
			{recommendation == "request_revision" ?
				<label className="btn btn-warning mr-3">
					<i className="fe fe-check-circle pr-2"/>
					Request revision
				</label> :
					<label className="btn btn-outline-warning mr-3" onClick={() => handleChange("request_revision")}>
						<i className="fe fe-check-circle pr-2"/>
						Request revision
					</label>
			}
			{recommendation == "decline" ?
				<label className="btn btn-danger">
					<i className="fe fe-check-circle pr-2"/>
					Reject
				</label> :
					<label className="btn btn-outline-danger" onClick={() => handleChange("decline")}>
						<i className="fe fe-check-circle pr-2"/>
						Reject
					</label>
			}
		</div>
	)
}

export default PeerReviewRecommendation;
