import React, { useState, useEffect } from "react";

function GlobalSearchForm() {
	const [query, setQuery] = useState("");
	const [results, setResults] = useState([]);

	const handleChange = event => {
		setQuery(event.target.value)
	}


	useEffect(() => {
		const fetchData = async () => {
			const response = await fetch(`/global_searches/new.json?q=${query}`)
			const searchResults = await response.json()
			setResults(searchResults.manuscripts)
		}
		fetchData()
	}, [query])


	return (
		<form className="form-inline mr-4 d-none d-md-flex">
			<div className="input-group input-group-flush input-group-merge">
				<input
					type="text"
					placeholder="Search"
					className="form-control form-control-prepended dropdown-toggle list-search"
					onChange={handleChange}
					value={query}
				>
				</input>
				<div className="input-group-prepend">
					<div className="input-group-text">
						<i className="fas fa-search"></i>
					</div>
				</div>
				{results.length != 0 &&
				<div className="dropdown-menu dropdown-menu-card show">
					<div className="card-body search-result">
						<div className="list-group list-group-flush list-group-focus my-n3">
							{results.map(({id, title, url, author}) => (
								<a className="list-group-item" href={url} key={id}>
									<div className="row align-items-center">
										<div className="col-auto">
											<div className="avatar">
												<img src="https://dashkit.goodthemes.co/assets/img/avatars/teams/team-logo-2.jpg" className="avatar-img rounded"/>
											</div>
										</div>
										<div className="col ml-n2">
											<div className="row align-items-center">
												<div className="col-auto">
												</div>
												<div className="col ml-n2">
													<h4 className="text-body text-focus mb-1 name">{title}</h4>
													<p className="small text-muted mb-0">{author}</p>
												</div>
											</div>
										</div>
									</div>
								</a>
							))}
						</div>
					</div>
				</div>
				}
			</div>
		</form>
	)
}

export default GlobalSearchForm;
