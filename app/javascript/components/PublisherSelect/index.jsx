import React from "react";
import AsyncSelect from "react-select/async";

class PublisherSelect extends React.Component {
	state = { 
		publisher: this.props.publisher,
		publisherIsOwner: (this.props.owner? (this.props.ownerId ? (this.props.publisher.value == this.props.ownerId) : false) : false)
	};

	loadOptions = async (query, callback) => {
		const response = await fetch(`/institution_searches/new.json?q=${query}`);
		const json = await response.json();

		callback(
			json.institutions.map((institution) => ({
				label: institution.label,
				value: institution.value,
			}))
		);
	};

	onChange = (selectedPublisher) => {
		this.setState({ publisher: selectedPublisher });
	};

	toggle = () => {
		this.setState({
			publisherIsOwner: !this.state.publisherIsOwner,
			publisher: { label: this.props.ownerName, value: this.props.ownerId }
		});
		if (this.state.publisherIsOwner) {
			$("#publisher_name").prop("disabled", false)
			$("#publisher_doi").prop("disabled", false)
			$("#new_publisher").show()
		} else {
			$("#publisher_name").prop("disabled", true)
			$("#publisher_doi").prop("disabled", true)
			$("#new_publisher").hide()
		}

	};

	render() {
		return (
			<div className="row">
				<div className="col-12">
					<div className="form-group">
						<h4>Publishers</h4>
						<AsyncSelect
							isDisabled={this.state.publisherIsOwner}
							defaultOptions
							name={`journal[publisher_id]`}
							value={this.state.publisher}
							onChange={this.onChange}
							placeholder="Search by name or DOI"
							loadOptions={this.loadOptions}
						/>
					</div>
					{this.props.ownerId &&
						<div className="custom-control custom-switch">
							<input
								checked={this.state.publisherIsOwner}
								type="checkbox"
								className="custom-control-input"
								id="publisher-is-owner-switch"
								name="journal[publisher_is_owner]"
								value={this.state.publisherIsOwner}
								onChange={() => this.toggle()}/>
							<label
								className="custom-control-label"
								htmlFor="publisher-is-owner-switch">{`Publisher is the same institution as Journal Owner (${this.props.ownerName}).`}
							</label>
						</div>
						}
				</div>
			</div>
		);
	}
}

export default PublisherSelect;
