import React from "react";
import AsyncSelect from "react-select/async";

class PayerSelect extends React.Component {
	state = { 
		payer: this.props.payer,
		payerIsOwner: (this.props.payer ? (this.props.ownerId ? (this.props.payer.value == this.props.ownerId) : false) : false),
		payerIsPublisher: (this.props.payer ? (this.props.publisherId ? (this.props.payer.value == this.props.publisherId) : false) : false) 
	};

	loadOptions = async (query, callback) => {
		const response = await fetch(`/institution_searches/new.json?q=${query}`);
		const json = await response.json();

		callback(
			json.institutions.map((institution) => ({
				label: institution.label,
				value: institution.value,
			}))
		);
	};

	onChange = (selectedPayer) => {
		this.setState({ payer: selectedPayer });
	};

	togglePayerIsOwner = () => {
		this.setState({
			payerIsOwner: !this.state.payerIsOwner,
			payer: { label: this.props.ownerName, value: this.props.ownerId }
		});
		if (this.state.payerIsOwner) {
			$("#payer_name").prop("disabled", false)
			$("#new_payer").show()
			if (this.state.payerIsPublisher) {
				this.setState({ payerIsPublisher: false })
			}
		} else {
			$("#payer_name").prop("disabled", true)
			$("#new_payer").hide()
		}
	}

	togglePayerIsPublisher = () => {
		this.setState({
			payerIsPublisher: !this.state.payerIsPublisher,
			payer: { label: this.props.publisherName, value: this.props.publisherId }
		});
		if (this.state.payerIsPublisher) {
			$("#payer_name").prop("disabled", false)
			$("#new_payer").show()
		} else {
			$("#payer_name").prop("disabled", true)
			$("#new_payer").hide()
		}
	}

	render() {
		return (
			<div className="row">
				<div className="col-12">
					<div className="form-group">
						<h4>Institutions</h4>
						<AsyncSelect
							isDisabled={this.state.payerIsOwner || this.state.payerIsPublisher}
							defaultOptions
							name={`journal[payer_id]`}
							value={this.state.payer}
							onChange={this.onChange}
							placeholder="Search by name or DOI"
							loadOptions={this.loadOptions}
						/>
					</div>
					{this.props.ownerId && (!this.state.payerIsPublisher || (this.state.payerIsPublisher && this.state.payerIsOwner)) &&
						<div className="custom-control custom-switch form-group">
							<input
								checked={this.state.payerIsOwner}
								type="checkbox"
								className="custom-control-input"
								id="payer-is-owner-switch"
								name="journal[payer_is_owner]"
								value={this.state.payerIsOwner}
								onChange={() => this.togglePayerIsOwner()}/>
							<label
								className="custom-control-label"
								htmlFor="payer-is-owner-switch">{`Payer is the same institution as Journal Owner (${this.props.ownerName}).`}
							</label>
						</div>
					}
					{this.props.publisherId && !this.state.payerIsOwner &&
						<div className="custom-control custom-switch form-group">
							<input
								checked={this.state.payerIsPublisher}
								type="checkbox"
								className="custom-control-input"
								id="payer-is-publisher-switch"
								name="journal[payer_is_publisher]"
								value={this.state.payerIsPublisher}
								onChange={() => this.togglePayerIsPublisher()}/>
							<label
								className="custom-control-label"
								htmlFor="payer-is-publisher-switch">{`Payer is the same institution as journal's publisher (${this.props.publisherName}).`}
							</label>
						</div>
					}
				</div>
			</div>
		);
	}
}

export default PayerSelect;
