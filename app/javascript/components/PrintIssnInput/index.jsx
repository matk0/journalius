import React from "react";
import InputMask from "react-input-mask";

export default class PrintIssn extends React.Component {
  state = { printIssn: this.props.printIssn };

  render() {
    return (
      <div className="form-group">
        <label htmlFor="journal_print_issn">p-ISSN</label>
        <InputMask
          onChange={(e) => this.setState({ printIssn: e.value })}
          value={this.state.printIssn}
          id="journal_print_issn"
          name="journal[print_issn]"
          placeholder="NNNN-NNN*"
          mask="9999-999X"
          maskChar=" "
          className={`form-control string ${
            Object.keys(this.props.errors).includes("electronic_issn") &&
            "is-invalid"
          }`}
          formatChars={{ 9: "[0-9]", a: "[A-Za-z]", X: "[X0-9]" }}
        />
        {Object.keys(this.props.errors).includes("print_issn") && (
          <div className="invalid-feedback issn">p-ISSN is not valid!</div>
        )}
      </div>
    );
  }
}
