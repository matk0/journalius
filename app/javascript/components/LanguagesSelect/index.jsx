import React from "react";
import Select from "react-select";
import { customReactSelectStyles } from "helpers";

class LanguagesSelect extends React.Component {
  state = { languages: this.props.languages };

  onChange = (languages) => {
    this.setState({ languages });
  };

  render() {
    return (
      <div className="mt-3">
        <label>Languages</label>
				{(this.props.forModel == "manuscript") && 
					<i
						className="fe fe-info align-top ml-2"
						title="Please select language of your manuscript."
						data-content="This journal accepts manuscripts only in the languages listed below."
						data-toggle="popover"
					/>
				}
        <Select
          id="react-languages-select"
          name={`${this.props.forModel}[languages][]`}
          isMulti
          value={this.state.languages}
          onChange={this.onChange}
          placeholder="Search and select"
          options={this.props.options}
					styles={customReactSelectStyles}
        />
      </div>
    );
  }
}

export default LanguagesSelect;
