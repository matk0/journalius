import React from "react";
import InputMask from "react-input-mask";

export default class ElectronicIssn extends React.Component {
  state = { electronicIssn: this.props.electronicIssn };
  render() {
    return (
      <div className="form-group">
        <label htmlFor="journal_electronic_issn">digital ISSN</label>
        <InputMask
          onChange={(e) => this.setState({ electronicIssn: e.value })}
          value={this.state.electronicIssn}
          id="journal_electronic_issn"
          name="journal[electronic_issn]"
          placeholder="NNNN-NNN*"
          mask="9999-999X"
          maskChar=" "
          className={`form-control string ${
            Object.keys(this.props.errors).includes("electronic_issn") &&
            "is-invalid"
          }`}
          formatChars={{ 9: "[0-9]", a: "[A-Za-z]", X: "[X0-9]" }}
        />
        {Object.keys(this.props.errors).includes("electronic_issn") && (
          <div className="invalid-feedback issn">e-ISSN is not valid!</div>
        )}
      </div>
    );
  }
}
