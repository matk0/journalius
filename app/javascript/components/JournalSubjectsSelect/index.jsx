import React from "react";
import AsyncSelect from "react-select/async";
import { customReactSelectStyles } from "helpers";

class JournalSubjectsSelect extends React.Component {
  state = { subjects: this.props.subjects };

  loadOptions = async (query, callback) => {
    const response = await fetch(`/subject_searches/new.json?q=${query}`);
    const json = await response.json();

    callback(
      json.subjects.map((subject) => ({
        label: subject.label,
        value: subject.value,
      }))
    );
  };

  onChange = (selectedSubjects) => {
		const subject_ids = selectedSubjects === null ? [""] : selectedSubjects.map(s => s.value)
		console.log(subject_ids)
		$.ajax({
			url: `/journals/${this.props.journalId}`,
			type: 'put',
			headers: {
				"X-CSRF-TOKEN": document.getElementsByName("csrf-token")[0].content,
			},
			data: {
				journal: {
					subject_ids
				}
			},
			success: function(data, status) {}
		}).then(() => {
			this.setState({ subjects: selectedSubjects });
		})
  };

  render() {
    return (
      <div className="mt-3">
        <AsyncSelect
          key="subjects-select"
          name={`${this.props.forModel}[subject_ids][]`}
          isMulti
          value={this.state.subjects}
          onChange={this.onChange}
          placeholder="Start typing to search ..."
          loadOptions={this.loadOptions}
					styles={customReactSelectStyles(this.props.isValid)}
        />
      </div>
    );
  }
}

export default JournalSubjectsSelect;
