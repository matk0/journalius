import React from "react";
import AsyncCreatableSelect from "react-select/async-creatable";
import { customReactSelectStyles } from "helpers";

class KeywordsInput extends React.Component {
  state = { keywords: this.props.keywords };

  loadOptions = async (query, callback) => {
    const response = await fetch(`/keyword_searches/new.json?q=${query}`);
    const json = await response.json();

    callback(
      json.keywords.map((keyword) => ({
        label: keyword.label,
        value: keyword.value,
      }))
    );
  };

  onChange = (selectedKeywords) => {
    this.setState({ keywords: selectedKeywords });
  };

  render() {
		return (
			<div className="mt-3">
				<label key="label">Keywords</label>
				<AsyncCreatableSelect
				key="keywords-input"
					name={`${this.props.forModel}[keyword_list][]`}
					isMulti
					value={this.state.keywords}
					onChange={this.onChange}
					placeholder="Start typing to search ..."
					loadOptions={this.loadOptions}
					styles={customReactSelectStyles}
				/>
			</div>
		)
			;
  }
}

export default KeywordsInput;
