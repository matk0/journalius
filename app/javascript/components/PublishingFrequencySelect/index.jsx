import React from "react";

class PublishingFrequencySelect extends React.Component {
  constructor(props) {
    super(props);
    this.onPublishingFrequencySelect = this.onPublishingFrequencySelect.bind(
      this
    );
    this.checkboxValue = this.checkboxValue.bind(this);
    this.onExpectedMonthsOfIssueSelect = this.onExpectedMonthsOfIssueSelect.bind(
      this
    );
    this.state = {
      selectedPublishingFrequency: this.props.publishingFrequency,
      expectedMonthsOfIssue: JSON.parse(this.props.expectedMonthsOfIssue) || [],
    };
  }

  onPublishingFrequencySelect(event) {
    if (event.target.value === "continuous_publishing") {
      this.setState({
        selectedPublishingFrequency: event.target.value,
        expectedMonthsOfIssue: [],
      });
			$("#issues-per-volume").hide()
    } else {
      this.setState({ selectedPublishingFrequency: event.target.value });
			$("#issues-per-volume").show()
    }
  }

  onExpectedMonthsOfIssueSelect(event) {
    let newVal = event.target.value;
    let stateVal = this.state.expectedMonthsOfIssue;

    stateVal.indexOf(newVal) === -1
      ? stateVal.push(newVal)
      : stateVal.length === 1
      ? (stateVal = [])
      : stateVal.splice(stateVal.indexOf(newVal), 1);

    this.setState({ expectedMonthsOfIssue: stateVal });
  }

  checkboxValue(month) {
    return this.state.expectedMonthsOfIssue.indexOf(month) !== -1;
  }

  render() {
    return (
      <div className="row">
        <div className="col">
          <div className="form-group">
            <select
              name="journal[publishing_frequency]"
              id="journal_publishing_frequency"
              className="form-control select"
              onChange={this.onPublishingFrequencySelect}
              value={this.state.selectedPublishingFrequency}
            >
              <option value="periodical_publishing">
                Periodical Publishing
              </option>
              <option value="continuous_publishing">
                Continous Publishing
              </option>
            </select>
          </div>
        </div>
        {this.state.selectedPublishingFrequency == "periodical_publishing" ? (
          <div className="col">
            <div className="form-group string journal_expected_months_of_issue">
              <label>Expected months of issue:</label>
              <div className="col">
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_january"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("january")}
                    value="january"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_january"
                  >
                    January
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_february"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("february")}
                    value="february"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_february"
                  >
                    February
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_march"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("march")}
                    value="march"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_march"
                  >
                    March
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_april"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("april")}
                    value="april"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_april"
                  >
                    April
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_may"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("may")}
                    value="may"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_may"
                  >
                    May
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_june"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("june")}
                    value="june"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_june"
                  >
                    June
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_july"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("july")}
                    value="july"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_july"
                  >
                    July
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_august"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("august")}
                    value="august"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_august"
                  >
                    August
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_september"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("september")}
                    value="september"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_september"
                  >
                    September
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_october"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("october")}
                    value="october"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_october"
                  >
                    October
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_november"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("november")}
                    value="november"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_november"
                  >
                    November
                  </label>
                </div>
                <div className="form-check">
                  <input
                    name="journal[expected_months_of_issue][]"
                    className="form-check-input"
                    type="checkbox"
                    id="journal_expected_months_of_issue_december"
                    onChange={this.onExpectedMonthsOfIssueSelect}
                    checked={this.checkboxValue("december")}
                    value="december"
                  ></input>
                  <label
                    className="form-check-label"
                    htmlFor="journal_expected_months_of_issue_december"
                  >
                    December
                  </label>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="col">
            <p>
              In continuous publishing journal has only Volumes, not Issues and
              publishes articles continuously throughout the year as soon as
              they are ready.
            </p>
          </div>
        )}
      </div>
    );
  }
}

export default PublishingFrequencySelect;
