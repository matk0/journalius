import { Controller } from "stimulus"
import consumer from "channels/consumer"

export default class extends Controller {
  connect() {
		if ($("meta[name='current-user']").length > 0) {
			this.subscription = consumer.subscriptions.create({ channel: "UnreadsChannel", id: this.data.get("id") }, {
				connected: this._connected.bind(this),
				disconnected: this._disconnected.bind(this),
				received: this._received.bind(this)
			})
		}
  }

  disconnect() {
    consumer.subscriptions.remove(this.subscription)
  }

  _connected() {
  }

  _disconnected() {
  }

  _received(data) {
		if (data.current_user_id != $("meta[name='current-user']").data("id").toString()) {
			this.element.classList.add("font-weight-bold")
		}
  }
}
