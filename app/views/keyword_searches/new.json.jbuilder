# frozen_string_literal: true

json.keywords do
  json.array!(@keywords) do |keyword|
    json.label keyword.name
    json.value keyword.name
  end
end
