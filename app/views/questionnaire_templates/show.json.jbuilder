# frozen_string_literal: true

json.partial! 'questionnaire_templates/questionnaire_template', questionnaire_template: @questionnaire_template
