# frozen_string_literal: true

json.array! @questionnaire_templates, partial: 'questionnaire_templates/questionnaire_template', as: :questionnaire_template
