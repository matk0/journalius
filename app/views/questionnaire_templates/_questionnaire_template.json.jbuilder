# frozen_string_literal: true

json.extract! questionnaire_template, :id, :name, :created_at, :updated_at
json.url questionnaire_template_url(questionnaire_template, format: :json)
