# frozen_string_literal: true

json.institutions do
  json.array!(@institutions) do |institution|
    json.label "#{institution.name} - #{institution.doi}"
    json.value institution.id
  end
end
