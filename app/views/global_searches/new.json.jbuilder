json.manuscripts do
  json.array!(@manuscripts) do |manuscript|
    json.title manuscript.title
    json.id manuscript.id
    json.author manuscript.corresponding_author.full_name
    json.url journal_manuscript_path(manuscript.journal, manuscript)
  end
end

# json.journals do
#   json.array!(@journals) do |journal|
#     json.name journal.title
#     json.url published_journal_overview_path(journal)
#   end
# end
