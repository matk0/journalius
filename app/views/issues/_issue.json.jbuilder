# frozen_string_literal: true

json.extract! issue, :id, :belongs_to, :created_at, :updated_at
json.url issue_url(issue, format: :json)
