# frozen_string_literal: true

json.array! @manuscripts, partial: 'manuscripts/manuscript', as: :manuscript
