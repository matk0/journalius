# frozen_string_literal: true

json.extract! manuscript, :id, :title, :doi, :created_at, :updated_at
json.url manuscript_url(manuscript, format: :json)
