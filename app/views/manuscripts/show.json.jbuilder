# frozen_string_literal: true

json.partial! 'manuscripts/manuscript', manuscript: @manuscript
