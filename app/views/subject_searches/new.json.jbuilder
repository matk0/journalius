# frozen_string_literal: true

json.subjects do
  json.array!(@subjects) do |subject|
    json.label subject.name
    json.value subject.id
  end
end
