# frozen_string_literal: true

class ManuscriptMailer < ApplicationMailer
  def submission_notification
    @addressee = params[:editor]
    @manuscript = params[:manuscript]
    @journal = params[:journal]
    mail(to: @addressee.email, subject: "#{@journal.title} received a new submission!")
  end

  def rejection_notification
    @manuscript = params[:manuscript]
    @text = params[:text]
    mail(to: @manuscript.corresponding_author.email, subject: "Your manuscript #{@manuscript.title} was rejected!")
  end

  def acceptance_notification
    @manuscript = params[:manuscript]
    mail(to: @manuscript.corresponding_author.email, subject: "Your manuscript #{@manuscript.title} was accepted for peer review! YAY!")
  end

  def revision_request_notification
    @manuscript = params[:manuscript]
    @text = params[:text]
    mail(to: @manuscript.corresponding_author.email, subject: "Please revise your manuscript titled #{@manuscript.title}.")
  end

  def resubmission_notification
    @addressee = params[:editor]
    @manuscript = params[:manuscript]
    @text = params[:text]
    mail(to: @addressee.email, subject: "#{@manuscript.corresponding_author.decorate.full_name} submitted a revision!")
  end
end
