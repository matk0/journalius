# frozen_string_literal: true

class PeerReviewMailer < ApplicationMailer
  def submission_notification
    @peer_review = params[:peer_review]
  end
end
