class InvitationsMailer < ApplicationMailer

  def internal_invitation
    @invitation = params[:invitation]
    mail to: @invitation.invitee.email, subject: invitation_subject
  end

  def external_invitation
    @invitation = params[:invitation]
    mail to: @invitation.invitee.email, subject: invitation_subject
  end

  def notify_inviter_of_decline
    @invitee = params[:invitee]
    @inviter = params[:inviter]
    @invitation = params[:invitation]
    mail to: @inviter.email, subject: "Your invitation of #{@invitee.decorate.full_name} to become #{@invitation.invited_as.name} at #{@invitation.invitable.title} was declined."
  end

  def notify_invitee_of_withdrawal
    @invitation = params[:invitation]
    mail to: @invitation.invitee.email, subject: "Your invitation to become #{@invitation.invited_as.name} at #{@invitation.invitable.title} was withdrawn."
  end

  def notify_inviter_of_acceptance
    @invitation = params[:invitation]
    mail to: @invitation.inviter.email, subject: acceptance_notification_subject
  end

  private

  def invitation_subject
    case @invitation.invitable_type
    when "Journal"
      "You have been invited by #{@invitation.inviter.decorate.full_name} to become #{@invitation.invited_as.name} at #{@invitation.invitable.title}."
    when "Manuscript"
      "You have been invited by #{@invitation.inviter.decorate.full_name} to review #{@invitation.invitable.title}."
    end
  end

  def acceptance_notification_subject
    case @invitation.invitable_type
    when "Journal"
     "Your invitation of #{@invitation.invitee.decorate.full_name} to become #{@invitation.invited_as.name} at #{@invitation.invitable.title} was accepted."
    when "Manuscript"
     "Your invitation of #{@invitation.invitee.decorate.full_name} to review #{@invitation.invitable.title} was accepted."
    end
  end
end
