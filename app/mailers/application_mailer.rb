# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@journalius.com' if (Rails.env.production? or Rails.env.test?)
  default from: 'mat@journalius.com' if Rails.env.staging?
  layout 'mailer'
end
