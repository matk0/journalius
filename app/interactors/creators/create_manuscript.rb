# frozen_string_literal: true

class Creators::CreateManuscript
  include Interactor

  def call
    manuscript = context.manuscript
    journal = manuscript.journal

    if manuscript.valid?
      ActiveRecord::Base.transaction do
        manuscript.save
        manuscript.corresponding_author.events.create(eventable: manuscript, action: "submitted")
        ManuscriptMailer.with(editor: journal.admin,
                              manuscript: manuscript,
                              journal: journal).submission_notification.deliver_later
      end
    else
      context.fail!(errors: manuscript.errors)
    end
  end
end
