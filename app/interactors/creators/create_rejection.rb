# frozen_string_literal: true

class Creators::CreateRejection
  include Interactor

  def call
    rejection = context.rejection
    if rejection.valid?
      rejection.manuscript.reject!
      rejection.save
      context.executor.events.create(action: "rejected", eventable: rejection.manuscript)
      ManuscriptMailer.with(manuscript: rejection.manuscript, text: rejection.text).rejection_notification.deliver_later
    else
      context.fail!(errors: rejection.errors)
    end
  end
end
