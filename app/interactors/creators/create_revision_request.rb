# frozen_string_literal: true

class Creators::CreateRevisionRequest
  include Interactor

  def call
    revision_request = context.revision_request
    if revision_request.valid?
      revision_request.manuscript.request_revision!
      revision_request.save
      context.executor.events.create(action: "asked_for_revision", eventable: revision_request.manuscript)
      ManuscriptMailer.with(manuscript: revision_request.manuscript, text: revision_request.text).revision_request_notification.deliver_later
    else
      context.fail!(errors: revision_request.errors)
    end
  end
end
