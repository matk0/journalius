# frozen_string_literal: true

class Creators::CreateDefaultPeerReviewQuestionnaire
  include Interactor

  def call
    questionnaire = QuestionnaireTemplate.new(name: "Journalius default peer review questionnaire")
    questions = []
    questions << Question.new(text: "Title of the paper is in accordance with it's focus.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Abstract accurately describes the content of the paper.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Keywords are suitably chosen.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Background knowledge is processed adequately.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Current state of the examined problem is properly developed.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Research methods are used appropriately.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Analysis and interpretation of the acquired results is satisfactory.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Conclusions are beneficial for science",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Findings are useful in practice",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Paper meets the criteria of scientific work.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Paper is authentic.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Topic is current.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Extent of the paper is adequate.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Text is clearly structured.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Text is comprehensible and logically consistent.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Terminology is used correctly.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Charts appropriately depict the text and are tagged properly (if present).",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Illustrations appropriately depict the text and are tagged properly (if present).",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Graphic quality of the illustrations is sufficient (if present).",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Quotations and paraphrases are clearly distinguished from text.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Bibliography and references correspond to the examined topic.",
                              field_type: "boolean",
                              required: false)
    questions << Question.new(text: "Range of bibliography and references is adequate.",
                              field_type: "boolean",
                              required: false)
    questionnaire.questions = questions
    questionnaire.save
  end
end
