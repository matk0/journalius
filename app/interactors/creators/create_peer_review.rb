# frozen_string_literal: true

class Creators::CreatePeerReview
  include Interactor

  def call
    peer_review = context.peer_review

    if peer_review.valid?
      if User.where(email: peer_review.invitee_email).any?
        peer_review.reviewer = User.find_by(email: peer_review.invitee_email)
        PeerReviewMailer.with(peer_review: peer_review).invitation.deliver_later
      else
        peer_review.reviewer = User.invite!(email: peer_review.invitee_email)
      end
      peer_review.save
    else
      context.fail!(errors: peer_review.errors)
    end
  end
end
