# frozen_string_literal: true

class AcceptManuscript
  include Interactor

  def call
    context.manuscript.accept!
    context.executor.events.create(eventable: context.manuscript,
                                   action: "accepted")
    ManuscriptMailer.with(manuscript: context.manuscript).acceptance_notification.deliver_later
  end
end
