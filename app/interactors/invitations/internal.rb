# frozen_string_literal: true

class Invitations::Internal
  include Interactor

  after do
    InvitationsMailer.with(invitation: @invitation).internal_invitation.deliver_now
  end

  def call
    @invitation = context.invitation

    if @invitation.valid?
      ActiveRecord::Base.transaction do
        @invitation.save
      end
    else
      context.fail!(errors: @invitation.errors)
    end
  end
end
