# frozen_string_literal: true

class Invitations::EditorInChief
  include Interactor

  after do
    InvitationsMailer.with(user: @user, invitation_token: @user.raw_invitation_token, journal: @journal).editor_in_chief_invitation.deliver_now
  end

  def call
    @journal = context.journal
    user = User.new context.user_params

    if user.valid?
      role = Role.find_or_create_by name: "Editor in Chief"
      ActiveRecord::Base.transaction do
        @user = User.invite!(context.user_params, context.current_user) do |u|
          u.skip_invitation = true
        end
        InternalInvitation.create(invitee: @user, inviter: @user.invited_by, invited_as_id: $editor_in_chief.id, invitable_type: "Journal", invitable_id: @journal.id)
      end
    else
      context.fail!(errors: user.errors)
    end
  end
end
