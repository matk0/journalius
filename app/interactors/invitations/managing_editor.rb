# frozen_string_literal: true

class Invitations::ManagingEditor
  include Interactor

  after do
    InvitationsMailer.with(user: @user, invitation_token: @user.raw_invitation_token, journal: @journal).managing_editor_invitation.deliver_later
  end

  def call
    @journal = context.journal
    user = User.new context.user_params

    if user.valid?
      role = Role.find_or_create_by name: "Managing Editor"
      ActiveRecord::Base.transaction do
        @user = User.invite!(context.user_params, context.current_user) do |u|
          u.skip_invitation = true
        end
        InternalInvitation.create(invitee: @user, inviter: @user.invited_by, invited_as_id: $managing_editor.id, invitable_type: "Journal", invitable_id: @journal.id)
      end
    else
      context.fail!(errors: user.errors)
    end
  end
end
