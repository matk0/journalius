# frozen_string_literal: true

class Invitations::External
  include Interactor

  after do
    InvitationsMailer.with(invitation: @invitation).external_invitation.deliver_now
  end

  def call
    @invitable = context.invitable_type.camelize.constantize.find(context.invitable_id)

    user = User.new context.user_params

    if user.valid?
      ActiveRecord::Base.transaction do
        @user = User.invite!(context.user_params, context.current_user) do |u|
          u.skip_invitation = true
        end
        @invitation = InternalInvitation.create!(invitee: @user,
                                                 inviter: context.current_user,
                                                 invited_as_id: context.invited_as_id,
                                                 invitable_type: context.invitable_type,
                                                 raw_invitation_token: @user.raw_invitation_token,
                                                 invitable_id: context.invitable_id)
      end
    else
      context.fail!(errors: user.errors)
    end
  end
end
