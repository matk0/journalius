require "uri"
require "net/http"

class Apis::Copyleaks::Download
  include Interactor

  def call
    url = URI("https://api.copyleaks.com/v3/downloads/005/report.pdf")

    https = Net::HTTP.new(url.host, url.port);
    https.use_ssl = true

    request = Net::HTTP::Get.new(url)
    request["Authorization"] = "Bearer #{context.access_token}"
    request["Cookie"] = "__cfduid=d0419cc45eeb6f4d8d8b010961c4df2b21603391743; cf_ob_info=524:5e959cfcaa6d1cee:BUD; cf_use_ob=443"

    response = https.request(request)
    puts response.read_body
  end
end

