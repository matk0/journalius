class Apis::Copyleaks::ScanForPlagiarism
  include Interactor::Organizer

  organize Apis::Copyleaks::Login,
           Base64Encode,
           Apis::Copyleaks::SubmitFile
end
