class Apis::Copyleaks::ExportReportToPdf
  include Interactor::Organizer

  organize Apis::Copyleaks::Login,
           Apis::Copyleaks::ExportReport
end
