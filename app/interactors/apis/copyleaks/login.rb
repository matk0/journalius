require "uri"
require "net/http"

class Apis::Copyleaks::Login
  include Interactor

  def call
    unless logged_in?
      url = URI("https://id.copyleaks.com/v3/account/login/api")

      https = Net::HTTP.new(url.host, url.port);
      https.use_ssl = true

      request = Net::HTTP::Post.new(url)
      request["Authorization"] = "Bearer {{AuthenticationToken}}"
      request["Content-Type"] = "application/json"
      request["Cookie"] = "__cfduid=d0419cc45eeb6f4d8d8b010961c4df2b21603391743"
      request.body = "{\r\n  \"email\": \"developers@journalius.com\",\r\n  \"Key\": \"#{Rails.application.credentials.dig(:copyleaks, :access_key)}\"\r\n}"

      response = JSON.parse(https.request(request).body)
      CopyleaksLogin.create(access_token: response["access_token"],
                            issued_at: response[".issued"],
                            expires_at: response[".expires"])
      context.access_token = response["access_token"]
    else
      context.access_token = CopyleaksLogin.last.access_token
    end
  end

  private

  def logged_in?
    if CopyleaksLogin.any?
      (CopyleaksLogin.last.issued_at + 1.minute) < Time.now
    else
      false
    end
  end
end
