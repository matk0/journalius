require "uri"
require "net/http"

class Apis::Copyleaks::SubmitFile
  include Interactor

  def call
    url = URI("https://api.copyleaks.com/v3/education/submit/file/#{Rails.env}-#{context.scan.id}")

    https = Net::HTTP.new(url.host, url.port);
    https.use_ssl = true

    request = Net::HTTP::Put.new(url)
    request["Authorization"] = "Bearer #{context.access_token}"
    request["Content-Type"] = "application/json"
    request["Cookie"] = "__cfduid=d0419cc45eeb6f4d8d8b010961c4df2b21603391743"
    request.body = "{\r\n  \"base64\": \"#{context.encoded_file}\",\r\n  \"filename\": \"file.pdf\",\r\n  \"properties\": {\r\n    \"pdf\": {\r\n        \"create\": true\r\n    },\r\n    \"webhooks\": {\r\n      \"status\": \"#{webhook_url}\"\r\n    }\r\n  }\r\n}"

    response = https.request(request)
  end

  private

  def webhook_url
    case Rails.env
    when "development"
      "https://#{Rails.application.config.hosts.last}/copyleaks_webhooks/{STATUS}/#{context.scan.id}"
    when "staging"
      "https://staging.journalius.com/copyleaks_webhooks/{STATUS}/#{context.scan.id}"
    when "production"
      "https://www.journalius.com/copyleaks_webhooks/{STATUS}/#{context.scan.id}"
    else
      "https://www.journalius.com/copyleaks_webhooks/{STATUS}/#{context.scan.id}"
    end
  end
end
