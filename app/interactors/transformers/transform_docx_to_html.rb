require 'docx'

class Transformers::TransformDocxToHtml
  include Interactor

  def call
    doc = Docx::Document.open(context.file_url)

    html = ""
    doc.paragraphs.each do |p|
      html += p.to_html
    end

    html
  end
end
