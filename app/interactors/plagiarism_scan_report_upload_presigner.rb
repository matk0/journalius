class PlagiarismScanReportUploadPresigner
  include Interactor

  def call
    s3 = Aws::S3::Client.new(
      region:               'eu-central-1',
      access_key_id:        Rails.application.credentials.dig(:aws, :access_key_id),
      secret_access_key:    Rails.application.credentials.dig(:aws, :secret_access_key)
    )

    signer = Aws::S3::Presigner.new(client: s3)
    context.upload_url = signer.presigned_url(
      :put_object,
      bucket: 'journalius-plagiarism-reports',
      key: "${filename}-#{SecureRandom.uuid}"
    )
  end
end
