# frozen_string_literal: true

class Resubmissions::Create
  include Interactor

  def call
    resubmission = context.resubmission
    if resubmission.valid?
      ActiveRecord::Base.transaction do
        resubmission.manuscript.resubmit!
        resubmission.save
        context.executor.events.create(action: "resubmitted", eventable: resubmission.manuscript)
        resubmission.manuscript.all_editors.each do |editor|
          ManuscriptMailer.with(editor: editor, manuscript: resubmission.manuscript, text: resubmission.text).resubmission_notification.deliver_later
        end
      end
    else
      context.fail!(errors: resubmission.errors)
    end
  end
end
