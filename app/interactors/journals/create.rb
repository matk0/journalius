# frozen_string_literal: true

class Journals::Create
  include Interactor

  def call
    journal = context.journal
    user = context.current_user

    if journal.valid?
      ActiveRecord::Base.transaction do
        journal.save
        user.events.create(eventable: journal, action: "created")
        Assignment.create(assignable: journal,
                          role_id: $admin.id,
                          user: user)
      end
    else
      context.fail!(errors: journal.errors)
    end
  end
end
