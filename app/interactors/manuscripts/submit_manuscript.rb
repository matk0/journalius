# frozen_string_literal: true

class Manuscripts::SubmitManuscript
  include Interactor

  def call
    manuscript = context.manuscript
    journal = context.journal
    ActiveRecord::Base.transaction do
      EditorialAssignment.create(manuscript: manuscript, editor: journal.managing_editor)
      ManuscriptMailer.with(editor: journal.managing_editor, manuscript: manuscript, journal: journal).submission_notification.deliver_later
      manuscript.submit!
    end
  end
end
