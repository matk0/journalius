# frozen_string_literal: true

class TechnicalEdits::CreateTechnicalEdit
  include Interactor

  def call
    technical_edit = context.technical_edit

    if technical_edit.valid?
      if User.where(email: technical_edit.invitee_email).any?
        technical_edit.technical_editor = User.find_by(email: technical_edit.invitee_email)
      else
        technical_edit.technical_editor = User.invite!(email: technical_edit.invitee_email)
      end
      technical_edit.save
    else
      context.fail!(errors: technical_edit.errors)
    end
  end
end
