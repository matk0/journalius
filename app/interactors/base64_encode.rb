require 'uri'

class Base64Encode
  include Interactor

  def call
    data = open(file_url) {|f| f.read}
    context.encoded_file = Base64.strict_encode64 data
  end

  private

  def file_url
    if Rails.env.development?
      "http://localhost:3000#{context.file_url}"
    else
      context.file_url
    end
  end
end
