# frozen_string_literal: true

class Owners::Create
  include Interactor

  def call
    owner = context.owner
    journal = context.journal
    user = context.user

    if owner.valid?
      ActiveRecord::Base.transaction do
        owner.save
        journal.update!(owner_id: owner.id)
        Assignment.create!(role_id: $admin.id,
                           user: user,
                           assignable: owner)
      end
    else
      context.fail!
    end
  end
end
