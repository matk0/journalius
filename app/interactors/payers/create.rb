# frozen_string_literal: true

class Payers::Create
  include Interactor

  def call
    payer = context.payer
    journal = context.journal
    user = context.user

    if payer.valid?
      ActiveRecord::Base.transaction do
        payer.save
        journal.update(payer_id: payer.id)
        Assignment.create(role_id: $admin.id,
                          user: user,
                          assignable: payer)
      end
    else
      context.fail!
    end
  end
end
