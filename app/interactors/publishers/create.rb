# frozen_string_literal: true

class Publishers::Create
  include Interactor

  def call
    publisher = context.publisher
    journal = context.journal
    user = context.user

    if publisher.valid?
      ActiveRecord::Base.transaction do
        publisher.save!
        journal.update!(publisher_id: publisher.id)
        Assignment.create!(role_id: $admin.id,
                           user: user,
                           assignable: publisher)
      end
    else
      context.fail!
    end
  end
end
