# frozen_string_literal: true

class LanguageEdits::CreateLanguageEdit
  include Interactor

  def call
    language_edit = context.language_edit

    if language_edit.valid?
      if User.where(email: language_edit.invitee_email).any?
        language_edit.language_editor = User.find_by(email: language_edit.invitee_email)
      else
        language_edit.language_editor = User.invite!(email: language_edit.invitee_email)
      end
      language_edit.save
    else
      context.fail!(errors: language_edit.errors)
    end
  end
end
