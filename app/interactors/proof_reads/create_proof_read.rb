# frozen_string_literal: true

class ProofReads::CreateProofRead
  include Interactor

  def call
    proof_read = context.proof_read

    if proof_read.valid?
      if User.where(email: proof_read.invitee_email).any?
        proof_read.proof_reader = User.find_by(email: proof_read.invitee_email)
      else
        proof_read.proof_reader = User.invite!(email: proof_read.invitee_email)
      end
      proof_read.save
    else
      context.fail!(errors: proof_read.errors)
    end
  end
end
