# frozen_string_literal: true

class ConvertDocxToPdfJob < ApplicationJob
  queue_as :default

  def perform(manuscript_id)
    ConvertDocxToPdfCommand.new(manuscript_id).execute
  end
end
