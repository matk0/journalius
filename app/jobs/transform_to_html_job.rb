# frozen_string_literal: true

class TransformToHtmlJob < ApplicationJob
  queue_as :default
  include Rails.application.routes.url_helpers

  def perform manuscript_id
    manuscript = Manuscript.find manuscript_id
    # TransformToHtml.call({ file_url: })
    # manuscript.update(html: TransformToHtml.call({ file_url: }))
  end
end
