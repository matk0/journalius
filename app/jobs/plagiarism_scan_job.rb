# frozen_string_literal: true

class PlagiarismScanJob < ApplicationJob
  queue_as :default

  def perform id
    scan = PlagiarismScan.find id
    Apis::Copyleaks::ScanForPlagiarism.call({scan: scan,
                                             file_url: Rails.application.routes.url_helpers.rails_blob_path(scan.manuscript.docx)})
  end
end
