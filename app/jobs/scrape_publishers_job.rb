# frozen_string_literal: true

class ScrapePublishersJob < ApplicationJob
  queue_as :default

  def perform
    PublishersScraper.call
  end
end
