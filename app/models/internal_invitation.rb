# frozen_string_literal: true

class InternalInvitation < ApplicationRecord
  include AASM

  belongs_to :invitee, class_name: "User"
  belongs_to :inviter, class_name: "User"
  belongs_to :invited_as, class_name: "Role"
  belongs_to :invitable, polymorphic: true

  aasm column: 'state' do
    state :pending, initial: true
    state :accepted, :declined

    event :accept do
      after do
        InvitationsMailer.with(invitation: self).notify_inviter_of_acceptance.deliver_now
        self.transform_to_assignment
      end
      transitions from: :pending, to: :accepted
    end

    event :decline do
      after do
        InvitationsMailer.with(invitee: self.invitee,
                               inviter: self.inviter,
                               invitation: self).notify_inviter_of_decline.deliver_now
      end
      transitions from: :pending, to: :declined
    end
  end

  def transform_to_assignment
    Assignment.create! user: self.invitee,
                       role_id: self.invited_as_id,
                       assignable_type: self.invitable_type,
                       assignable_id: self.invitable_id

    if self.invited_as_id == $reviewer.id
      PeerReview.create manuscript_id: self.invitable_id,
                        reviewer: self.invitee
    end
  end
end
