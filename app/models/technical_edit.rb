# frozen_string_literal: true

class TechnicalEdit < ApplicationRecord
  include AASM

  belongs_to :technical_editor, class_name: 'User', foreign_key: 'user_id', optional: true
  belongs_to :manuscript
  has_one_attached :technically_edited_manuscript

  aasm column: 'state' do
    state :invited, initial: true
    state :in_progress, :rejected, :complete

    event :accept do
      transitions from: %i[invited rejected in_progress], to: :in_progress
    end

    event :reject do
      transitions from: %i[invited accepted in_progress], to: :rejected
    end
  end
end
