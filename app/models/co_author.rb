# frozen_string_literal: true

class CoAuthor < ApplicationRecord
  belongs_to :manuscript
  validates :full_name, presence: true
end
