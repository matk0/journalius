# frozen_string_literal: true

class Journal < ApplicationRecord
  include ActiveModel::Dirty
  has_many :volumes
  has_many :manuscripts
  has_and_belongs_to_many :abstract_and_indexing_databases
  has_and_belongs_to_many :subjects
  belongs_to :publisher, optional: true, class_name: "Institution"
  belongs_to :payer, optional: true, class_name: "Institution"
  belongs_to :owner, optional: true, class_name: "Institution"
  has_one_attached :cover

  scope :published, -> { where(published: true) }
  scope :unpublished, -> { where(published: false) }

  enum publishing_frequency: { periodical_publishing: 0, continuous_publishing: 1 }
  enum publishing_model: { oa_no_apcs: 0, oa_apcs: 1, paid_access: 2 }
  enum peer_review_type: { single_blind: 0, double_blind: 1, triple_blind: 2 }

  validates :title, presence: true, uniqueness: true
  validate :print_issn_validity
  validate :electronic_issn_validity
  attribute :volumes_per_year, :integer, default: 1
  validates :volumes_per_year, numericality: { greater_than: 0 }
  attribute :issues_per_volume, :integer, default: 2
  validates :issues_per_volume, numericality: { greater_than: 0 }

  serialize :expected_months_of_issue
  serialize :languages

  has_rich_text :editorial_information
  has_rich_text :aims_and_scope
  has_rich_text :guidelines_for_reviewers
  has_rich_text :guidelines_for_authors
  has_rich_text :custom_copyrights_policy
  has_rich_text :oa_statement
  has_rich_text :plagiarism_policy
  has_rich_text :ethical_policy
  has_rich_text :editorial_board
  has_rich_text :advisory_board
  has_rich_text :archiving_policy

  def admin
    admins[0]
  end

  def admins
    assignments = Assignment.where(assignable: self,
                                   role_id: $admin.id).pluck(:user_id)
    User.find assignments
  end

  def editors_in_chief
    editors_in_chief_ids = Assignment.where(assignable: self,
                                            role_id: $editor_in_chief.id).pluck(:user_id)
    User.where(id: editors_in_chief_ids).decorate
  end

  def managing_editors
    managing_editors_ids = Assignment.where(assignable: self,
                                            role_id: $managing_editor.id).pluck(:user_id)
    User.where(id: managing_editors_ids).decorate
  end

  def editors
    editors_ids = Assignment.where(assignable: self,
                                   role_id: $editor.id).pluck(:user_id)
    User.where(id: editors_ids).decorate
  end

  def outstanding_editor_in_chief_invitations
    InternalInvitation.includes([:invitee]).where(invitable: self,
                                                  invited_as_id: $editor_in_chief.id).where.not(state: "accepted")
  end

  def outstanding_managing_editor_invitations
    InternalInvitation.includes([:invitee]).where(invitable: self,
                                                  invited_as_id: $managing_editor.id).where.not(state: "accepted")
  end

  def outstanding_editor_invitations
    InternalInvitation.includes([:invitee]).where(invitable: self,
                                                  invited_as_id: $editor.id).where.not(state: "accepted")
  end

  def electronic_issn_validity
    if electronic_issn.present?
      errors.add(:electronic_issn, 'is not valid') unless StdNum::ISSN.normalize(electronic_issn)
    end
  end

  def print_issn_validity
    if print_issn.present?
      errors.add(:print_issn, 'is not valid') unless StdNum::ISSN.normalize(print_issn)
    end
  end

  def check_for_new_institution_admins current_user
    if saved_change_to_owner_id?
      unless Assignment.where(role_id: $admin.id, assignable: owner).any?
        Assignment.create(role_id: $admin.id, assignable: self.owner, user: current_user)
      end
    end
    if saved_change_to_publisher_id?
      unless Assignment.where(role_id: $admin.id, assignable: publisher).any?
        Assignment.create(role_id: $admin.id, assignable: self.publisher, user: current_user)
      end
    end
    if saved_change_to_payer_id?
      unless Assignment.where(role_id: $admin.id, assignable: payer).any?
        Assignment.create(role_id: $admin.id, assignable: self.payer, user: current_user)
      end
    end
  end
end
