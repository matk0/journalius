# frozen_string_literal: true

class PeerReview < ApplicationRecord
  include AASM

  belongs_to :reviewer, class_name: 'User', foreign_key: 'user_id', optional: true
  belongs_to :manuscript
  belongs_to :questionnaire, class_name: 'QuestionnaireTemplate', optional: true
  has_one_attached :reviewed_manuscript

  enum recommendation: { recommend_to_accept: 0, request_revision: 1, decline: 2 }

  has_rich_text :message_for_author
  has_rich_text :message_for_editor

  aasm column: 'state' do
    state :in_progress, initial: true
    state :submitted

    event :submit do
      transitions from: :in_progress, to: :submitted
    end
  end
end
