class Pin < ApplicationRecord
  belongs_to :user
  belongs_to :pinnable, polymorphic: true
end
