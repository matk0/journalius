# frozen_string_literal: true

class Resubmission < ApplicationRecord
  belongs_to :manuscript, optional: true
  has_rich_text :text

  validates :text, presence: true
end
