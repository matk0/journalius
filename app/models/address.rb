# frozen_string_literal: true

class Address < ApplicationRecord
  belongs_to :institution, optional: true
end
