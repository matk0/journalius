# frozen_string_literal: true

class Subject < ApplicationRecord
  has_ancestry
  has_and_belongs_to_many :journals
  has_and_belongs_to_many :manuscripts
  validates :name, uniqueness: true
end
