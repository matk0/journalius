# frozen_string_literal: true

class Institution < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :kind, presence: true
  validates :doi, allow_blank: true, uniqueness: true

  has_many :journals
  has_many :channels
  has_many :questionnaire_templates
  has_one :address, dependent: :destroy
  has_one :billing_address, class_name: 'Address', foreign_key: :institutions_billing_address_id, dependent: :destroy

  accepts_nested_attributes_for :address, allow_destroy: true
  accepts_nested_attributes_for :billing_address, allow_destroy: true

  enum kind: { university: 10, academic: 20, society: 30, commercial: 40, privately_owned: 50, publisher: 60, other: 70 }

  def publisher?
    kind == "publisher"
  end
end
