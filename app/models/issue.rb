# frozen_string_literal: true

class Issue < ApplicationRecord
  belongs_to :volume
  has_many :manuscripts
end
