# frozen_string_literal: true

class User < ApplicationRecord
  devise :invitable, :database_authenticatable, :registerable, :confirmable, :trackable,
         :recoverable, :rememberable, :validatable, :omniauthable, omniauth_providers: %i[linkedin google_oauth2 orcid]

  has_many :assignments, dependent: :destroy
  has_many :roles, through: :assignments
  has_many :peer_reviews
  has_many :messages, dependent: :destroy
  has_many :channel_users, dependent: :destroy
  has_many :channels, through: :channel_users
  has_many :events
  has_many :internal_invitations, dependent: :destroy, foreign_key: "invitee_id"

  accepts_nested_attributes_for :assignments, allow_destroy: true

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :password, presence: true
  validates :password_confirmation, presence: true

  enum title: ["Mr.", "Mrs.", "Ms.", "Dr.", "Prof."]

  after_invitation_accepted :accept_internal_invitation

  def accept_internal_invitation
    invitation = InternalInvitation.where(invitee: self).last
    invitation.accept!
  end

  def role?(role)
    roles.any? { |r| r.name.gsub(/ /, '').underscore.to_sym == role }
  end

  def journals
    assignments = Assignment.where(user_id: id,
                                   assignable_type: "Journal")
    Journal.where id: assignments.pluck(:assignable_id)
  end

  def manuscripts
    assignments = Assignment.where(user_id: id,
                                   assignable_type: "Manuscript")
    Manuscript.where id: assignments.pluck(:assignable_id)
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.linkedin_data"] && session["devise.linkedin_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
  
  def self.from_omniauth provider:, uid:, email:, first_name:, last_name:, avatar_url:
    devise_friendly_token = Devise.friendly_token[0, 20]
    where(provider: provider, uid: uid).first_or_create do |user|
      user.email = email
      user.first_name = first_name
      user.last_name = last_name
      user.avatar_url = avatar_url
      user.password = devise_friendly_token
      user.password_confirmation = devise_friendly_token 
    end
  end

  def confirmation_required?
    provider.nil?
  end
end
