# frozen_string_literal: true

class Manuscript < ApplicationRecord
  include AASM
  has_paper_trail

  scope :published, -> { where.not(published_at: nil) }

  has_many_attached :additional_files
  has_one_attached :docx
  has_and_belongs_to_many :subjects
  has_many :plagiarism_scans, dependent: :destroy
  has_many :co_authors
  has_many :rejections, dependent: :destroy
  has_many :revision_requests, dependent: :destroy
  has_many :peer_reviews, dependent: :destroy
  has_many :reviewers, through: :peer_reviews
  has_many :technical_edits, dependent: :destroy
  has_many :technical_editors, through: :technical_edits
  has_many :language_edits, dependent: :destroy
  has_many :language_editors, through: :language_edits
  has_many :proof_reads, dependent: :destroy
  has_many :proof_readers, through: :proof_reads
  has_many :channels, dependent: :destroy
  belongs_to :corresponding_author, class_name: 'User'
  belongs_to :issue, optional: true
  belongs_to :journal

  serialize :languages

  accepts_nested_attributes_for :corresponding_author
  accepts_nested_attributes_for :co_authors, allow_destroy: true

  has_rich_text :abstract
  has_rich_text :body
  has_rich_text :references

  validates :title, presence: true
  validates :abstract, presence: true
  validates_presence_of :docx

  acts_as_taggable_on :keywords

  aasm column: 'state' do
    state :submitted, initial: true
    state :accepted, :rejected, :in_revision

    event :accept do
      transitions from: %i[submitted rejected in_revision], to: :accepted
    end

    event :reject do
      transitions from: %i[submitted accepted in_revision], to: :rejected
    end

    event :request_revision do
      transitions from: %i[submitted accepted rejected], to: :in_revision
    end

    event :resubmit do
      transitions from: :in_revision, to: :submitted
    end
  end

  def transform_to_pdf
    ConvertDocxToPdfJob.perform_later id
  end

  def transform_to_html
    TransformToHtmlJob.perform_later id
  end

  def published?
    published_at.present?
  end

  def editors
    assignments = Assignment.where(assignable_id: id,
                                   role_id: $editor.id,
                                   assignable_type: "Manuscript")
    User.where id: assignments.pluck(:user_id)
  end

  def editor
    admin_assignments = Assignment.where(assignable_id: journal.id,
                                         assignable_type: "Journal",
                                         role_id: $admin.id)
                                   .pluck(:user_id)
    ### TODO Refactor
    User.where(id: admin_assignments)[0]
  end

  def all_editors
    admin_assignments = Assignment.where(assignable_id: journal.id,
                                         assignable_type: "Journal",
                                         role_id: $admin.id)
                                   .pluck(:user_id)
    editor_in_chief_assignments = Assignment.where(assignable_id: journal.id,
                                                   assignable_type: "Journal",
                                                   role_id: $editor_in_chief.id)
                                             .pluck(:user_id)
    managing_editor_assignments = Assignment.where(assignable_id: journal.id,
                                                   assignable_type: "Journal",
                                                   role_id: $managing_editor.id)
    editor_assignments = Assignment.where(assignable_id: id,
                                          role_id: $editor.id,
                                          assignable_type: "Manuscript")
                                   .pluck(:user_id)
    User.where id: (admin_assignments +
                    editor_in_chief_assignments +
                    managing_editor_assignments +
                    editor_assignments)
  end

  def management_editors
    admin_assignments = Assignment.where(assignable_id: journal.id,
                                         assignable_type: "Journal",
                                         role_id: $admin.id)
                                   .pluck(:user_id)
    editor_in_chief_assignments = Assignment.where(assignable_id: journal.id,
                                                   assignable_type: "Journal",
                                                   role_id: $editor_in_chief.id)
                                             .pluck(:user_id)
    managing_editor_assignments = Assignment.where(assignable_id: journal.id,
                                                   assignable_type: "Journal",
                                                   role_id: $managing_editor.id)
    User.where id: (admin_assignments +
                    editor_in_chief_assignments +
                    managing_editor_assignments)
  end

  def generate_doi
    self.update(doi: if journal.publisher.present?
                       "#{journal.publisher.doi}-#{id}"
                     else
                       "MISSING-PUBLISHER-#{id}"
                     end)
  end

  def first_event
    Event.where(eventable: self).order(:created_at).first
  end

  def first_decision
    Event.where(eventable: self, action: "accepted").or(Event.where(eventable: self, action: "rejected")).first
  end

  def last_event
    Event.where(eventable: self).order(:created_at).last
  end

  def publisher
    journal.publisher
  end

  def outstanding_reviewer_invitations
    InternalInvitation.includes([:invitee]).where(invitable: self,
                                                  invited_as_id: $reviewer.id).where.not(state: "accepted")
  end
end
