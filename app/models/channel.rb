# frozen_string_literal: true

class Channel < ApplicationRecord
  has_many :channel_users, dependent: :destroy
  has_many :users, through: :channel_users, validate: false
  has_many :messages, dependent: :destroy
  belongs_to :manuscript, optional: true

  validates :name, presence: true
end
