# frozen_string_literal: true

class Question < ApplicationRecord
  belongs_to :questionnaire_template
  after_save :update_field_attr

  private

  def update_field_attr
    update_column(:field_attr, text.gsub(/[^0-9A-Za-z]/, '').underscore)
  end

end
