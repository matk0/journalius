class PlagiarismScan < ApplicationRecord
  include AASM

  belongs_to :manuscript

  has_one_attached :report

  aasm column: 'state' do
    state :submitted, initial: true
  end

  def api_id
    case id.to_s.length
    when 1
      "00#{id}"
    when 2
      "0#{id}"
    else
      id
    end
  end
end
