# frozen_string_literal: true

class Assignment < ApplicationRecord
  belongs_to :user
  belongs_to :role
  belongs_to :assignable, polymorphic: true

  after_create :create_related_record

  private

    def create_related_record
      # case role_id
      # when $reviewer.id
      #   PeerReview.create(user_id: user_id,
      #                     manuscript_id: assignable_id)
      # end
    end
end
