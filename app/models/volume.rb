# frozen_string_literal: true

class Volume < ApplicationRecord
  belongs_to :journal
  has_many :issues
end
