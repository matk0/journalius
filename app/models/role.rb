# frozen_string_literal: true

class Role < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  has_many :assignments
  has_many :users, through: :assignments

  scope :editor_accounts, -> { where(name: ['Editor', 'Managing Editor']) }
end
