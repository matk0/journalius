# frozen_string_literal: true

class AbstractAndIndexingDatabase < ApplicationRecord
  has_and_belongs_to_many :journals
end
