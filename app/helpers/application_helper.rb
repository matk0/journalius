# frozen_string_literal: true

module ApplicationHelper
  def blank_option
    tag.option "", disabled: true, selected: true
  end

  def link_to_add_fields(name, f, association, _class_name = '')
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + '_fields', f: builder)
    end
    link_to(name, '#', class: 'add_fields btn btn-lg btn-primary mt-3', data: { id: id, fields: fields.gsub("\n", '') })
  end

  def language_options
    ISO_639::ISO_639_1.map do |language|
      { 'label' => language[3], 'value' => language[3] }
    end.to_json
  end

  def file_preview file
    tag.div(class: "col-3 attachment-#{file.id}", id: file.id) do
      safe_join([tag.div(class: 'row') do
        safe_join([tag.div(class: 'col-10') do
          case file.content_type
          when 'application/pdf'
            tag.i class: 'far fa-file-pdf fa-2x'
          when 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword'
            tag.i class: 'far fa-file-word fa-2x'
          when 'image/jpeg'
            image_tag file.blob.representation(resize_to_limit: [150, 150])
          else
            tag.i class: 'far fa-file fa-2x'
          end
        end,
                   delete_icon(file)])
      end,
                 tag.div(class: 'row') do
                   tag.div(class: 'col-12') do
                     tag.p(class: 'do-not-break-out') do
                       file.filename.to_s
                     end
                   end
                 end])
    end
  end

  def file_icon file
    icon_class = case file.content_type
                 when 'application/pdf'
                   'far fa-file-pdf fa-2x'
                 when 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword'
                   'far fa-file-word fa-2x'
                 when 'image/jpeg', 'image/png'
                   'far fa-image fa-2x'
                 else
                   'far fa-file fa-2x'
                 end
    tag.i(class: icon_class)
  end

  def hidden_file_input(file)
    hidden_field_tag 'manuscript[files][]', file.signed_id, class: "attachment-#{file.id}"
  end

  def delete_icon(file)
    action_name != 'show' ?
      tag.div(class: 'col-2') do
        link_to "/attachments/#{file.id}", method: :delete, confirm: 'Are you sure you want to delete this attachment?', remote: true do
          tag.i(class: 'fas fa-trash fa-2x align-top') {}
        end
      end :
      tag.div(class: 'col-2') {}
  end

  def is_number?(string)
    true if Float(string)
  rescue StandardError
    false
  end

  def input_class
    if resource.errors.any?
      "form-control is-invalid"
    else
      "form-control"
    end
  end

  def feedback_for_invalid_field field
    if resource.errors.messages.key? field
      case field
      when :password_confirmation
        if resource.errors.messages[field][0].nil?
          tag.div "#{field.to_s.humanize} can't be blank.", class: "invalid-feedback"
        else
          tag.div "#{field.to_s.humanize} #{resource.errors.messages[field][0]}.", class: "invalid-feedback"
        end
      else
        unless resource.errors.messages[field][0].nil?
          tag.div "#{field.to_s.humanize} #{resource.errors.messages[field][0]}.", class: "invalid-feedback"
        end
      end
    end
  end


  def flash_messages
    tag.div style: "position: absolute; bottom: 0; right: 0; min-height: 200px; min-width: 500px;" do
      tag.div class: "flash-wrapper mb-4 mr-4", style: "position: absolute; bottom: 0; right: 0" do
        if flash.to_a.any?
          flash.to_a.map do |alert|
            toast alert
          end.join("").html_safe
        end
      end
    end
  end

  def toast alert
    tag.div id: "toast", class: "toast", role: "alert", aria: { live: "assertive", atomic: "true" } do
      safe_join([toast_header, toast_body(alert[1])])
    end
  end

  def toast_header caption=""
    tag.div class: "toast-header" do
      safe_join([toast_header_caption(caption), toast_header_close_button])
    end
  end

  def toast_header_caption caption=nil
    tag.strong class: "mr-auto" do
      caption || "Title Text"
    end
  end

  def toast_header_close_button
    tag.button type: "button", class: "ml-2 mb-1 close", data: { dismiss: "toast" }, aria: { label: "Close" } do
      tag.span aria: { hidden: "true" } do
        "x"
      end
    end
  end

  def toast_body text
    tag.div class: "toast-body" do
      text
    end
  end

  def bootstrap_class_for msg_type
    case msg_type.to_sym
    when :notice
  "alert-info"
    when :success
  "alert-success"
    when :error
  "alert-danger"
    when :alert
  "alert-danger"
    end
  end

  def avatar user
    tag.div(class: "avatar avatar-sm") do
      tag.span(class: "avatar-title rounded") do
        user.decorate.initials
      end
    end
  end

  def full_name user
    tag.p(class: "mb-1") do
      user.decorate.full_name
    end
  end

  def info_row content
    tag.div class: "row mt-3" do
      tag.div class: "col-12" do
        content
      end
    end
  end

  def validation_error resource, error_text
    tag.div class: "invalid-feedback #{resource}-error", style: "display: block;" do
      "#{resource.capitalize} #{error_text}"
    end
  end

  def state_badge state, size="h5"
    tag.send(size) do
      tag.span(class: "badge #{badge_class state}") { state.humanize }
    end
  end

  def badge_class state
    case state
    when "in_progress", "submitted"
      "badge-soft-primary"
    when "Request revision"
      "badge-soft-warning"
    when "accept"
      "badge-soft-success"
    else
      "badge-soft-danger"
    end
  end
end
