# frozen_string_literal: true

module PeerReviewsHelper
  def recommendation
    case @peer_review.recommendation
    when "recommend_to_accept"
      state_badge "accept", "h2"
    when "request_revision"
      state_badge "Request revision", "h2"
    else
      state_badge "Reject", "h2"
    end
  end
end
