# frozen_string_literal: true

module ManuscriptsHelper
  def link_to_manuscripts_issue
    link_to "Volume #{@manuscript.issue.volume.number}: Issue #{@manuscript.issue.number}", journal_volume_issue_path(@manuscript.journal, @manuscript.issue.volume, @manuscript.issue)
  end

  def manuscripts_keywords
    @manuscript.keyword_list.any? ?
    @manuscript.keyword_list.map do |k|
      { 'label' => is_number?(k) ? ActsAsTaggableOn::Tag.find(k).name : k, 'value' => k }
    end.to_json :
    nil
  end

  def manuscripts_subjects
    @manuscript.subjects.present? ?
    @manuscript.subjects.map { |s| { 'label' => s.name, 'value' => s.id } }.to_json :
    nil
  end

  def manuscripts_languages
    @manuscript.languages.present? && (@manuscript.languages != ['']) ?
    @manuscript.languages.map { |l| { 'label' => l, 'value' => l } }.to_json :
    nil
  end

  def manuscripts_language_options
    if @journal.present? && @journal.languages.present?
      @journal.languages.map { |l| { 'label' => l, 'value' => l } }.to_json
    else
      language_options
    end
  end

  def state(manuscript)
    case manuscript[:role]
    when 'Corresponding Author', 'Co-Author', 'Managing Editor', 'Editor'
      manuscript['state']
    when 'Reviewer'
      manuscript[:peer_review].state
    when 'Technical Editor'
      manuscript[:technical_edit].state
    when 'Language Editor'
      manuscript[:language_edit].state
    when 'Proof Reader'
      manuscript[:proof_read].state
    else
      'Missing state determination for current role! Contact Matej.'
    end
  end

  def plagiarism_scan_action
    if @manuscript.plagiarism_scans.any?
      case @manuscript.plagiarism_scans.last.state
      when "submitted"
        tag.div(class: "btn btn-primary mb-2") do
          safe_join([tag.span(class: "spinner-border spinner-border-sm",
                              id: "plagiarism-scan-in-progress",
                              role: "status",
                              "aria-hidden": "true"),
                     " Plagiarism scan in progress"])
        end
      else
      end
    else
      link_to "Scan for plagiarism", plagiarism_scans_path(manuscript_id: @manuscript.id), method: :post, remote: true, class: "btn btn-primary mb-2", id: "request-plagiarism-scan"
    end
  end

  def co_authors
    @manuscript.co_authors.pluck(:full_name).join(", ")
  end

  def keywords
    @manuscript.keywords.map do |keyword|
      keyword.name
    end.join(", ") if @manuscript.keywords.any?
  end

  def manuscript_text
    "Manuscript Text"
    # safe_join[doc.paragraphs.map do |p|
    #   p.to_html
    # end]
  end

  def pin_unpin_manuscript_to_from_dashboard
    @pin = Pin.find_by(pinnable: @manuscript, user: current_user)
    if @pin.present?
      unpin_manuscript_from_dashboard
    else
      @pin = Pin.new(pinnable_id: @manuscript.id, pinnable_type: "Manuscript")
      pin_manuscript_to_dashboard
    end
  end

  def pin_manuscript_to_dashboard
    link_to pins_path(pin: {pinnable_id: @manuscript, pinnable_type: "Manuscript"}), method: :post, remote: true, class: "pin-to-dashboard false" do
      tag.i class:"fas fa-thumbtack", data: {"original-title": "Pin this manuscript to your Dashboard.", placement: "top", toggle: "tooltip"}
    end
  end

  def unpin_manuscript_from_dashboard
    link_to pin_path(@pin), method: :delete, remote: true, class: "pin-to-dashboard true" do
      tag.i class:"fas fa-thumbtack", data: {"original-title": "Unpin this manuscript from your Dashboard.", placement: "top", toggle: "tooltip"}
    end
  end

  def assigned_editors
    @manuscript.editors.collect do |editor|
      editor_row editor
    end.join("").html_safe
  end

  def editor_row editor
    tag.div class: "row align-items-center mt-2", id: "assignment-#{Assignment.find_by(user: editor, role_id: $editor.id).id}" do
      [avatar_column(editor), name_column(editor), remove_column(editor)].join("").html_safe
    end
  end

  def avatar_column user
    link_to user_path(user), class: "col-auto" do
      avatar user
    end
  end

  def name_column user
    link_to user_path(user), class: "col ml-n2" do
      full_name user
    end
  end

  def remove_column editor
    if policy(@manuscript).update?
      assignment_id = Assignment.find_by(user: editor, role_id: $editor.id, assignable: @manuscript).id
      link_to assignment_path(assignment_id), remote: true, method: :delete, class: "col-auto cursor-pointer" do
        tag.i id: "remove-editorial-assignment-#{assignment_id}", class: "fe fe-trash"
      end
    end
  end

  def state_column state
    tag.span class: "badge badge-soft-primary" do
      state.humanize
    end
  end

  def peer_reviews
    @manuscript.peer_reviews.includes([:reviewer]).collect do |peer_review|
      tag.div class: "list-group-item" do
        peer_review_row peer_review
      end
    end.join("").html_safe
  end

  def peer_review_row peer_review
    tag.div class: "row align-items-center mt-2", id: "peer-review-#{peer_review.id}" do
      [peer_review_avatar_column(peer_review), peer_review_column(peer_review), state_column(peer_review.state)].join("").html_safe
    end
  end

  def peer_review_avatar_column peer_review
    link_to peer_review_overview_path(peer_review), class: "col-auto" do
      avatar peer_review.reviewer
    end
  end

  def peer_review_column peer_review
    link_to peer_review_overview_path(peer_review), class: "col ml-n2" do
      full_name peer_review.reviewer
    end
  end

  def doi
    tag.div class: "row", id: "doi" do
      tag.div class: "col-12" do
        if @manuscript.doi.present?
          [tag.h6("DOI", class: "text-uppercase text-muted mb-2"),
           tag.span(@manuscript.doi, class: "h2")].join("").html_safe
        elsif policy(@manuscript).generate_doi?
          link_to "Generate DOI number", dois_path(manuscript: { id: @manuscript.id}), method: :post, remote: true, class: "btn btn-primary"
        end
      end
    end
  end

  def languages
    info_row [tag.h6("Languages", class: "text-uppercase text-muted mb-2"),
              languages_spans].flatten.join("").html_safe
  end

  def languages_spans
    if @manuscript.languages.present?
      @manuscript.languages.map{|l| language_span l }
    else
      "No languages yet"
    end
  end

  def language_span language
    tag.p language, class: "mb-1"
  end

  def subjects
    info_row [tag.h6("Areas of science", class: "text-uppercase text-muted mb-2"),
              subjects_spans].flatten.join("").html_safe
  end

  def subjects_spans
    if @manuscript.subjects.present?
      @manuscript.subjects.map{|s| subject_span s }
    else
      "No areas of science yet"
    end
  end

  def subject_span subject
    tag.p subject.name, class: "mb-1"
  end

  def number_of_words
    info_row [tag.h6("Number of words", class: "text-uppercase text-muted mb-2"),
              "TBD"].join("").html_safe
  end

  def number_of_pages
    info_row [tag.h6("Number of pages", class: "text-uppercase text-muted mb-2"),
             "TBD"].join("").html_safe
  end

  def number_of_figures
    info_row [tag.h6("Number of figures", class: "text-uppercase text-muted mb-2"),
             "TBD"].join("").html_safe

  end

  def time_from_submission_to_first_decision
    info_row [tag.h6("Time from submission to first decision", class: "text-uppercase text-muted mb-2"),
              @manuscript.time_from_submission_to_first_decision].join("").html_safe
  end

  def time_from_submission_to_final_decision
    info_row [tag.h6("Time from submission to final decision", class: "text-uppercase text-muted mb-2"),
              "What is the formula to calculate this?"].join("").html_safe
  end

  def time_from_submission_to_publication
    info_row [tag.h6("Time from submission to publication", class: "text-uppercase text-muted mb-2"),
              @manuscript.time_from_submission_to_publication].join("").html_safe
  end

  def time_from_last_action
    info_row [tag.h6("Time from last action", class: "text-uppercase text-muted mb-2"),
              @manuscript.time_from_last_action].join("").html_safe
  end

  def my_role manuscript
    assignment = Assignment.find_by(assignable: manuscript, user: current_user)
    if assignment.present?
      assignment.role.name
    elsif current_user == manuscript.corresponding_author
      'Corresponding Author'
    else
      'Journal Admin'
    end
  end

  def edit_button
    if @manuscript.in_revision? and current_user == @manuscript.corresponding_author
      link_to "Edit", edit_manuscript_path(@manuscript.id), class: "btn btn-primary mb-2", data: {"original-title": "Edit this manuscript", placement: "top", toggle: "tooltip"}
    elsif @manuscript.state != "in_revision"
      link_to "Edit", edit_manuscript_path(@manuscript.id), class: "btn btn-primary mb-2", data: {"original-title": "Edit this manuscript", placement: "top", toggle: "tooltip"}
    end
  end

  def no_actions_message
    tag.p do
      "You cannot take any actions at this moment."
    end
  end

  def changes_history
    @manuscript.versions[1..-1].map do |version|
      diff_table(version) if version.changeset.present?
    end.join("").html_safe
  end

  def diff_table version
    tag.table class: "table table-sm" do
      [diff_table_caption(version),
       diff_table_header,
       diff_table_body(version)].join("").html_safe
    end
  end

  def diff_table_caption version
    version.changeset["updated_at"][1].strftime "Changes made %d.%m.%Y"
  end

  def diff_table_header
    tag.thead do
      tag.tr do
        [tag.th{"Attribute"},
         tag.th{"Changes"}].join("").html_safe
      end
    end
  end

  def diff_table_body version
    tag.tbody do
      version.changeset.select{|k,v| k != "updated_at"}.map do |key, value|
        tag.tr do
          [tag.td{key.humanize},
           tag.td{Diffy::Diff.new(value[0], value[1]).to_s(:html_simple).html_safe}].join("").html_safe
        end
      end.join("").html_safe
    end
  end

  def peer_reviewer_options
    already_invited = User.find InternalInvitation.where(invitable: @manuscript, invited_as_id: $reviewer.id).pluck(:invitee_id)
    (User.all - [current_user] - already_invited).flatten.collect do |user|
      [user.decorate.full_name, user.id]
    end
  end

  def available_editors
    UserDecorator.decorate_collection((@manuscript.all_editors - [current_user])).map do |user|
      [user.full_name, user.id]
    end
  end
end
