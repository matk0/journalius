module UsersHelper
  def title_options
    safe_join([blank_option] + User.titles.map do |key, value|
      tag.option key.humanize
    end)
  end
end
