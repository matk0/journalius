# frozen_string_literal: true

module CarouselHelper
  def carousel_for(contents)
    Carousel.new(self, contents).html
  end

  class Carousel
    def initialize(view, contents)
      @view = view
      @contents = contents
    end

    def html
      content = view.safe_join([indicators, slides, controls])
      view.tag.div(content, id: 'carousel', class: 'carousel slide', data: { ride: 'carousel' })
    end

    private

    attr_accessor :view, :contents
    delegate :link_to, :tag, :image_tag, :safe_join, to: :view

    def indicators
      items = contents.count.times.map { |index| indicator_tag(index) }
      tag.ol safe_join(items), class: 'carousel-indicators'
    end

    def indicator_tag(index)
      options = {
        class: (index.zero? ? 'active' : ''),
        data: {
          target: '#carousel',
          'slide-to': index
        }
      }
      tag.li '', options
    end

    def slides
      items = contents.map.with_index { |content, index| slide_tag(content, index.zero?) }
      tag.div safe_join(items), class: 'carousel-inner'
    end

    def slide_tag(content, is_active)
      options = {
        class: (is_active ? 'carousel-item active' : 'carousel-item')
      }

      image = image_tag content[:image], class: 'd-block w-100'
      h = tag.h1 content[:caption]
      p = tag.p content[:text]
      caption = tag.div class: 'carousel-caption d-block' do
        safe_join [h, p]
      end
      tag.div options do
        safe_join [image, caption]
      end
    end

    def controls
      safe_join [control_tag('left'), control_tag('right')]
    end

    def control_tag(direction)
      prev_or_next = direction == 'left' ? 'prev' : 'next'
      options = {
        class: "carousel-control-#{prev_or_next}",
        role: 'button',
        data: { slide: prev_or_next }
      }

      link_to '#carousel', options do
        tag.span '', class: "carousel-control-#{prev_or_next}-icon", "aria-hidden": true
      end
    end
  end
end
