# frozen_string_literal: true

module NavigationHelper

  def link_to_dashboard
    tag.li class: "nav-item" do
      link_to dashboard_path, class: "nav-link #{"active" if request.path.include?("dashboard")}" do
        safe_join [(tag.i class: "fe fe-home"), "Dashboard"]
      end
    end
  end

  def link_to_journals
    tag.li class: "nav-item" do
      link_to journals_path, class: "nav-link #{"active" if request.path.include?("journal")}" do
        safe_join [(tag.i class: "fe fe-book-open"), "Journals"]
      end
    end
  end

  def link_to_manuscripts
    tag.li class: "nav-item" do
      link_to manuscripts_path, class: "nav-link #{"active" if request.path.include?("manuscript")}" do
        safe_join [(tag.i class: "fe fe-file"), "Manuscripts"]
      end
    end
  end

  def link_to_my_peer_reviews
    tag.li class: "nav-item" do
      link_to peer_reviews_path, class: "nav-link #{"active" if request.path.include?("peer_review") and !request.path.include?("manuscript")}" do
        safe_join [(tag.i class: "fe fe-file-text"), "My Peer Reviews"]
      end
    end
  end
end
