# frozen_string_literal: true

module JournalsHelper
  def users_for_editor_account_assigments
    User.all.map { |u| [u.id, u.full_name] }
  end

  def roles_for_editor_account_assignments
    Role.editor_accounts.map { |r| [r.id, r.name] }
  end

  def publishing_models
    [['oa_no_apcs', 'Open Access with no APCs'],
     ['oa_apcs', 'Open Access with APCs'],
     ['paid_access', 'Paid Access']]
  end

  def abstracting_and_indexing_databases
    AbstractAndIndexingDatabase.all.map do |db|
      [db.id, db.name]
    end
  end

  def peer_review_types
    [['single_blind', 'Single blind'],
     ['double_blind', 'Double blind'],
     ['triple_blind', 'Triple blind']]
  end

  def journals_subjects
    @journal.subjects.present? ?
    @journal.subjects.map { |s| { 'label' => s.name, 'value' => s.id } }.to_json :
    nil
  end

  def journals_languages
    @journal.languages.present? && (@journal.languages != ['']) ?
    @journal.languages.map { |l| { 'label' => l, 'value' => l } }.to_json :
    nil
  end

  def journals_publisher
    @journal.publisher.present? ?
    { 'label' => @journal.publisher.name, 'value' => @journal.publisher.id }.to_json :
    nil
  end

  def journals_payer
    @journal.payer.present? ?
    { 'label' => @journal.payer.name, 'value' => @journal.payer.id }.to_json :
    nil
  end

  def journals_owner
    @journal.owner.present? ?
    { 'label' => @journal.owner.name, 'value' => @journal.owner.id }.to_json :
    nil
  end
  
  def journals_table
    tag.table class: "table table-striped table-bordered table-hover" do
      [journals_table_head,
       journals_table_body].join("").html_safe
    end
  end

  def journals_table_head
    tag.thead do
      tag.tr do
        [tag.th("Title"),
         tag.th("Publisher"),
         tag.th("My Role"),
         tag.th("Subject"),
         tag.th("Web of Science"),
         tag.th("IF"),
         tag.th("SJR"),
         tag.th("ERIH"),
         tag.th("Manuscripts in review"),
         tag.th("Average time to publication"),
         tag.th("Charge APCs"),
         tag.th("Predatory score"),
        ].join("").html_safe
      end
    end
  end

  def journals_table_body
    tag.tbody id: "journals" do
      @journals.collect{|j| journals_table_row(j)}.join("").html_safe
    end
  end

  def journals_table_row journal
    tag.tr do
      [journals_title_table_cell(journal),
       journals_publisher_table_cell(journal),
       journals_my_role_table_cell(journal),
       journals_subjects_table_cell(journal),
       journals_web_of_science_table_cell(journal),
       journals_if_table_cell(journal),
       journals_sjr_table_cell(journal),
       journals_erih_table_cell(journal),
       journals_no_of_manuscripts_in_review_table_cell(journal),
       journals_average_time_to_publication_table_cell(journal),
       journals_charge_apcs_table_cell(journal),
       journals_predatory_score_table_cell(journal)].join("").html_safe
    end
  end

  def journals_title_table_cell journal
    tag.td do
      link_to journal.title, journal_overview_path(journal)
    end
  end

  def journals_publisher_table_cell journal
    journal.publisher.present? ?
      tag.td do
        journal.publisher.name
      end :
      tag.td do
        ""
      end
  end

  def journals_my_role_table_cell journal
    tag.td do
      Assignment.find_by(assignable: journal, user: current_user).try(:role).try(:name)
    end
  end

  def journals_subjects_table_cell journal
    tag.td do
      journal.subjects.map{|s| s.name}.join(", ")
    end
  end

  def journals_web_of_science_table_cell journal
    tag.td class: "text-center" do
      tag.div class: "custom-control custom-checkbox table-checkbox" do
        [tag.input(class: "list-checkbox custom-control-input", id: "web-of-science", name: "web-of-science", type: "checkbox", checked: true),
         tag.label(class: "custom-control-label align-middle", for: "web-of-science")].join("").html_safe
      end
    end
  end

  def journals_if_table_cell journal
    tag.td do
      "12345"
    end
  end

  def journals_sjr_table_cell journal
    tag.td do
      "12345"
    end
  end

  def journals_erih_table_cell journal
    tag.td class: "text-center" do
      tag.div class: "custom-control custom-checkbox table-checkbox" do
        [tag.input(class: "list-checkbox custom-control-input", id: "erih", name: "erih", type: "checkbox", checked: true),
         tag.label(class: "custom-control-label align-middle", for: "erih")].join("").html_safe
      end
    end
  end

  def journals_no_of_manuscripts_in_review_table_cell journal
    tag.td do
      journal.no_of_manuscripts_in_review.to_s
    end
  end

  def journals_average_time_to_publication_table_cell journal
    tag.td do
      journal.average_time_to_publication
    end
  end

  def journals_charge_apcs_table_cell journal
    tag.td class: "text-center" do
      tag.div class: "custom-control custom-checkbox table-checkbox" do
        [tag.input(class: "list-checkbox custom-control-input", id: "charge-apcs", name: "charge-apcs", type: "checkbox", checked: true),
         tag.label(class: "custom-control-label align-middle", for: "charge-apcs")].join("").html_safe
      end
    end
  end

  def journals_predatory_score_table_cell journal
    tag.td do
      "12345"
    end
  end

  def pin_unpin_journal_to_from_dashboard
    @pin = Pin.find_by(pinnable: @journal, user: current_user)
    if @pin.present?
      unpin_journal_from_dashboard
    else
      @pin = Pin.new(pinnable_id: @journal.id, pinnable_type: "Journal")
      pin_journal_to_dashboard
    end
  end

  def pin_journal_to_dashboard
    link_to pins_path(pin: {pinnable_id: @journal, pinnable_type: "Journal"}), method: :post, remote: true, class: "pin-to-dashboard false float-right" do
      tag.i class:"fas fa-thumbtack", data: {"original-title": "Pin this journal to your Dashboard.", placement: "top", toggle: "tooltip"}
    end
  end

  def unpin_journal_from_dashboard
    link_to pin_path(@pin), method: :delete, remote: true, class: "pin-to-dashboard true float-right" do
      tag.i class:"fas fa-thumbtack", data: {"original-title": "Unpin this journal from your Dashboard.", placement: "top", toggle: "tooltip"}
    end
  end

  def journal_cover
    tag.div class: "card bg-transparent journal-cover" do
      tag.div class: "card-body text-center upload-journal-cover", id: @journal.id do
        if @journal.cover.attached?
          [image_tag(@journal.cover, class: "img-fluid"),
           remove_journal_cover_icon].join("").html_safe
        else
          tag.p class: "text-muted" do
            ["This Journal has no cover yet.",
             tag.div(class: "btn btn-primary", data: {behavior: "uppy-trigger"}) { "Upload cover" },].join("").html_safe
          end
              
        end
      end
    end
  end

  def remove_journal_cover_icon
    link_to attachment_path(@journal.cover.id), remote: true, method: :delete do
      tag.i class: "fe fe-trash float-right align-bottom"
    end
  end

  def publisher_name
    tag.div id: "publisher-name" do
      @journal.publisher.present? ?
        link_to(@journal.publisher.name, institution_path(@journal.publisher)) :
        "N/A"
    end
  end

  def payer_name
    tag.div id: "payer-name" do
      @journal.payer.present? ?
        link_to(@journal.payer.name, institution_path(@journal.payer)) :
        "N/A"
    end
  end

  def owner_name
    tag.div id: "owner-name" do
      @journal.owner.present? ?
        link_to(@journal.owner.name, institution_path(@journal.owner)) :
        "N/A"
    end
  end

  def owner_id
    if @journal.owner.present?
      @journal.owner.id
    else
      nil
    end
  end

  def owner_name_string
    if @journal.owner.present?
      @journal.owner.name
    else
      nil
    end
  end

  def publisher_id
    if @journal.publisher.present?
      @journal.publisher.id
    else
      nil
    end
  end

  def publisher_name_string
    if @journal.publisher.present?
      @journal.publisher.name
    else
      nil
    end
  end

  def show_new_journal_payer_form?
    if @journal.payer != nil
      @journal.payer == @journal.owner or @journal.payer == @journal.publisher
    else
      false
    end
  end

  def show_new_journal_publisher_form?
    if @journal.owner != nil
      @journal.publisher == @journal.owner
    else
      false
    end
  end

  def editors_in_chief
    @journal.editors_in_chief.collect do |eic|
      journal_editor_list_item eic, "Editor in Chief"
    end.join("").html_safe
  end

  def managing_editors
    @journal.managing_editors.collect do |me|
      journal_editor_list_item me, "Managing Editor"
    end.join("").html_safe
  end

  def editors
    @journal.editors.collect do |me|
      journal_editor_list_item me, "Editor"
    end.join("").html_safe
  end

  def journal_editor_list_item editor, role_name
    role = Role.find_by_name role_name
    tag.div class: "list-group-item", id: "assignment-#{Assignment.find_by(user: editor, role_id: role.id, assignable: @journal).id}" do
      tag.div class: "row align-items-center"  do
      [avatar_column(editor), name_column(editor), invitation_state_column(editor), remove_editor_column(editor, role)].join("").html_safe
      end
    end
  end

  def invitation_state_column state
    tag.span(class: "badge badge-light") { state }
  end

  def remove_editor_column editor, role
    assignment = Assignment.find_by(user: editor, role_id: role.id, assignable: @journal)
    if policy(assignment).destroy?
      link_to assignment_path(assignment.id), remote: true, method: :delete, class: "col-auto cursor-pointer" do
        tag.i id: "remove-assignment-#{assignment.id}", class: "fe fe-trash"
      end
    end
  end

  def editor_in_chief_options
    already_invited = User.find InternalInvitation.where(invitable: @journal, invited_as_id: $editor_in_chief.id, state: "pending").pluck(:invitee_id)
    (User.all - [current_user] - @journal.editors_in_chief - already_invited).flatten.collect do |user|
      [user.decorate.full_name, user.id]
    end
  end

  def managing_editor_options
    already_invited = User.find InternalInvitation.where(invitable: @journal, invited_as_id: $managing_editor.id, state: "pending").pluck(:invitee_id)
    (User.all - [current_user] - @journal.managing_editors - already_invited).flatten.collect do |user|
      [user.decorate.full_name, user.id]
    end
  end

  def editor_options
    already_invited = User.find InternalInvitation.where(invitable: @journal, invited_as_id: $editor.id, state: "pending").pluck(:invitee_id)
    (User.all - [current_user] - @journal.editors - already_invited).flatten.collect do |user|
      [user.decorate.full_name, user.id]
    end
  end

  def editor_in_chief_invitations
    @journal.outstanding_editor_in_chief_invitations.collect do |eic|
      invitation_list_item eic
    end.join("").html_safe
  end

  def managing_editor_invitations
    @journal.outstanding_managing_editor_invitations.collect do |mei|
      invitation_list_item mei
    end.join("").html_safe
  end

  def editor_invitations
    @journal.outstanding_editor_invitations.collect do |eic|
      invitation_list_item eic
    end.join("").html_safe
  end

  def invitation_list_item invitation
    tag.div class: "list-group-item", id: "invitation-#{invitation.id}" do
      tag.div class: "row align-items-center" do
        [avatar_column(invitation.invitee), name_column(invitation.invitee), invitation_state_column(invitation.state), remove_invitation_column(invitation)].join("").html_safe
      end
    end
  end

  def remove_invitation_column invitation
    if policy(invitation).destroy?
      link_to internal_invitation_path(invitation.id), remote: true, method: :delete, class: "col-auto cursor-pointer", data: { confirm: "Are you sure you want to withdraw this invitation?" } do
        tag.i id: "remove-invitation-#{invitation.id}", class: "fe fe-trash"
      end
    end
  end
end
