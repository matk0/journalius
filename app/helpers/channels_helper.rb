# frozen_string_literal: true

module ChannelsHelper
  def subject_of
    if @channel.manuscript.present?
      @channel.manuscript.title
    else
      (@channel.users - current_user).join(', ')
    end
  end

  def available_conversation_partners
    UserDecorator.decorate_collection((@manuscript.all_editors + [@manuscript.corresponding_author] - [current_user])).map do |user|
      [user.full_name, user.id]
    end
  end
end
