# frozen_string_literal: true

require 'sidekiq/web'
require 'sidekiq-scheduler/web'

Rails.application.routes.draw do
  mount Split::Dashboard, at: "split"
  resources :channels

  if Rails.env.development?
  end

  authenticate :admin_user do
    mount Sidekiq::Web => '/sidekiq'
  end

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :users, controllers: { sessions: 'users/sessions',
                                    confirmations: 'users/confirmations',
                                    passwords: 'users/passwords',
                                    invitations: 'users/invitations',
                                    sessions: 'users/sessions',
                                    omniauth_callbacks: 'users/omniauth_callbacks' }
  resources :users, only: [:show]
  resources :internal_invitations, only: [:create, :destroy] do
    get :accept
    get :decline
  end


  root 'homes#show'
  get :about_us, controller: :homes
  get :privacy_policy, controller: :homes
  resource :dashboard, only: [:show]
  get :withdrawn, controller: :internal_invitations

  resources :channels, only: %i[new show index create] do
    resources :messages, only: %i[create]
    resource :channel_user
  end

  resources :institutions, only: %i[show create]
  resources :institution_searches, only: %i[new]
  resources :publishers, only: %i[create]
  resources :payers, only: %i[create]
  resources :owners, only: %i[create]
  resources :questionnaire_templates

  namespace :published do
    resources :journals, only: %i[index show]
    ### TODO: add published volumes, issues, manuscripts, etc.
  end

  resources :keyword_searches, only: [:new]
  resources :subject_searches, only: [:new]
  resources :global_searches, only: [:new]
  resources :attachments, only: [:create, :destroy]
  resources :blobs, only: [:destroy]

  resources :manuscripts do
    get :overview, controller: "manuscripts/overview", action: :show
    get :peer_reviews, controller: "manuscripts/peer_reviews", action: :show
  end

  resources :peer_reviews, only: [:index, :update] do
    get :overview, controller: "peer_reviews/overview", action: :show
    get :questionnaire, controller: "peer_reviews/questionnaire", action: :show
    get :submit
  end

  resources :rejections, only: :create
  resources :acceptances, only: :create
  resources :revision_requests, only: :create
  resources :resubmissions, only: :create

  resources :journals do
    get :overview, controller: "journals/overview", action: :show
    get :editorial, controller: "journals/editorial", action: :show
    get :manuscripts, controller: "journals/manuscripts", action: :show
    get :volumes, controller: "journals/volumes", action: :show
    get :metrics, controller: "journals/metrics", action: :show
    get :marketing, controller: "journals/marketing", action: :show
    get :guidelines, controller: "journals/guidelines", action: :show
    get :statements_and_policies, controller: "journals/statements_and_policies", action: :show
    get :files, controller: "journals/files", action: :show
    get :other_settings, controller: "journals/other_settings", action: :show

    resources :manuscripts do
      member do
        get :publish
      end
      resources :technical_edits, only: %i[index show edit update] do
        member do
          get :accept
          get :reject
        end
      end
      resources :language_edits, only: %i[index show edit update] do
        member do
          get :accept
          get :reject
        end
      end
      resources :proof_reads, only: %i[index show edit update] do
        member do
          get :accept
          get :reject
        end
      end
    end
    resources :volumes do
      resources :issues do
        resources :manuscripts
      end
    end
    resources :issues
  end

  resources :plagiarism_scans, only: [:create]

  post '/copyleaks_webhooks/completed/:id' => 'copyleaks_webhooks#completed'
  post '/copyleaks_webhooks/error/:id' => 'copyleaks_webhooks#error'
  post '/copyleaks_webhooks/creditsChecked/:id' => 'copyleaks_webhooks#creditsChecked'
  post '/copyleaks_webhooks/indexed/:id' => 'copyleaks_webhooks#indexed'
  post '/copyleaks_webhooks/exports/:id/completed' => 'copyleaks_webhooks#export_completed'
  	# "completionWebhook": "https://yourserver.com/export/export-id/completed",

  resources :pins, only: [:create, :destroy]

  resources :co_authors, only: [:destroy]
  resources :assignments, only: [:create, :destroy]
  resources :dois, only: [:create]

  get '/journals/:id', to: redirect("/journals/%{id}/overview")
  get '/manuscripts/:id', to: redirect("/manuscripts/%{id}/overview")
end
