# frozen_string_literal: true

ConvertApi.configure do |config|
  config.api_secret = Rails.application.credentials.dig(:convert_api, :access_key_id)
end
