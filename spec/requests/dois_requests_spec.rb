require 'rails_helper'

RSpec.describe "Dois", type: :request do
  let!(:admin_assignment) { create(:admin_assignment) }
  let(:admin) { admin_assignment.user }
  let!(:manuscript) { create(:manuscript,
                             doi: nil,
                             journal: admin_assignment.assignable) }
  before :each do
    sign_in admin
  end

  describe "POST create" do
    it "generates a DOI for submitted manuscript" do
      expect do
        post "/dois", params: { manuscript: { id: manuscript.id }}, xhr: true
        manuscript.reload
      end.to change{manuscript.doi}.from(nil).to("#{manuscript.journal.publisher.doi}-#{manuscript.id}")
    end
  end
end
