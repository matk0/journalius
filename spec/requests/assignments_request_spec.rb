require 'rails_helper'

RSpec.describe "Assignments", type: :request do
  let!(:admin_assignment) { create(:admin_assignment) }
  let!(:editor_in_chief_role) { Role.create(name: "Editor in Chief"); $editor_in_chief = Role.find_by_name("Editor in Chief") }
  let!(:managing_editor_role) { Role.create(name: "Managing Editor"); $managing_editor = Role.find_by_name("Managing Editor") }
  let(:admin) { admin_assignment.user }
  let!(:manuscript) { create(:manuscript, journal: admin_assignment.assignable) }

  before :each do
    sign_in admin
  end


  describe "POST create" do
    let!(:editor_role) { Role.create(name: "Editor"); $editor = Role.find_by_name("Editor") }
    let!(:editor) { create(:editor) }

    it "creates an manuscript editor assignment." do
      expect do
        post "/assignments", params: { assignment: { assignable_type: "Manuscript",
                                                     assignable_id: manuscript.id,
                                                     role_id: editor_role.id,
                                                     user_id: editor.id }}, xhr: true
      end.to change{Assignment.count}.by(1)
    end
  end

  describe "DELETE destroy" do
    let!(:editorial_assignment) { create(:editorial_assignment, assignable: Manuscript.first ) }

    it "destroy's an existing editorial assignment." do
      expect do
        delete "/assignments/#{editorial_assignment.id}", xhr: true
      end.to change{Assignment.count}.by(-1)
    end
  end
end
