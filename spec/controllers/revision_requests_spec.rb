# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RevisionRequestsController, type: :controller do
  let!(:editorial_assignment) { create(:editorial_assignment) }
  let(:manuscript) { editorial_assignment.assignable }
  let(:editor) { editorial_assignment.user }

  before :each do
    sign_in editor
  end

  describe 'POST create' do
    it "transitions manuscript's state to 'in revision'." do
      expect do
        post :create, params: { revision_request: { manuscript_id: manuscript.id, text: 'Please revise this shit!' } }
        manuscript.reload
      end.to change { manuscript.state }.from('submitted').to('in_revision')
    end

    it 'creates a new revision request.' do
      expect do
        post :create, params: { revision_request: { manuscript_id: manuscript.id, text: 'Please revise this shit!' } }
      end.to change { RevisionRequest.count }.by(1)
      expect(RevisionRequest.last.manuscript_id).to eq manuscript.id
      expect(RevisionRequest.last.text.to_plain_text).to eq 'Please revise this shit!'
    end
  end
end
