# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ManuscriptsController, type: :controller do
  let!(:admin_assignment) { create(:admin_assignment) }
  let(:admin) { admin_assignment.user }
  let(:journal) { admin_assignment.assignable }

  describe "DELETE destroy" do

    before :each do
      sign_in admin
    end

    let!(:manuscript) { create(:manuscript, journal: journal) }

    it "should destroy a manuscript." do
      expect { post :destroy, params: { id: manuscript.id, method: :delete } }.to change{ Manuscript.count }.by(-1)
    end
  end
end
