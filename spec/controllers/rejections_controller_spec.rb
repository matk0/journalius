# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RejectionsController, type: :controller do
  let!(:editorial_assignment) { create(:editorial_assignment) }
  let(:manuscript) { editorial_assignment.assignable }
  let(:editor) { editorial_assignment.user }

  before :each do
    sign_in editor
  end

  describe 'POST create' do
    it "transitions manuscript's state to rejected." do
      expect do
        post :create, params: { rejection: { manuscript_id: manuscript.id, text: 'Get the fuck outta here!' } }
        manuscript.reload
      end.to change { manuscript.state }.from('submitted').to('rejected')
    end

    it 'creates a new rejection.' do
      expect do
        post :create, params: { rejection: { manuscript_id: manuscript.id, text: 'Get the fuck outta here!' } }
      end.to change { Rejection.count }.by(1)
      expect(Rejection.last.manuscript_id).to eq manuscript.id
    end
  end
end
