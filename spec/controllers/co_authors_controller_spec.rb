# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CoAuthorsController, type: :controller do
  let!(:co_author) { create(:manuscript).co_authors[0] }

  describe "DELETE destroy" do
    it "should destroy a co author." do
      expect { post :destroy, params: { id: co_author.id, method: :delete }, format: :js }.to change{ CoAuthor.count }.by(-1)
    end
  end
end
