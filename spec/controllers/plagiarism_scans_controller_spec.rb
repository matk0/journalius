# frozen_string_literal: true

require 'rails_helper'
ActiveJob::Base.queue_adapter = :test

RSpec.describe PlagiarismScansController, type: :controller do
  let!(:editorial_assignment) { create(:editorial_assignment) }
  let(:manuscript) { editorial_assignment.assignable }
  let(:editor) { editorial_assignment.user }

  before :each do
    sign_in editor
  end


  describe 'POST create' do
    it "creates a scan." do
      expect do
        post :create, format: :js, params: { manuscript_id: manuscript.id }
      end.to have_enqueued_job(PlagiarismScanJob)
    end
  end
end
