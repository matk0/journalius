# frozen_string_literal: true

require 'rails_helper'

RSpec.describe VolumesController, type: :controller do
  let!(:admin_assignment) { create(:admin_assignment) }
  let(:journal) { admin_assignment.assignable }
  let(:admin) { admin_assignment.user }

  before :each do
    sign_in admin
  end

  describe 'POST create' do
    it "creates a new volume." do
      expect { post :create, params: { journal_id: journal.id } }.to change{Volume.count}.by(1)
    end
  end
end
