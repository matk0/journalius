# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AcceptancesController, type: :controller do
  let!(:admin_assignment) { create(:admin_assignment) }
  let!(:editor_in_chief_assignment) { create(:editor_in_chief_assignment) }
  let!(:managing_editor_assignment) { create(:managing_editor_assignment) }
  let!(:editorial_assignment) { create(:editorial_assignment) }
  let(:manuscript) { editorial_assignment.assignable }
  let(:editor) { editorial_assignment.user }

  before :each do
    sign_in editor
  end

  describe 'POST create' do
    it "transitions manuscript's state to accepted." do
      expect do
        post :create, params: { id: manuscript.id }
        manuscript.reload
      end.to change { manuscript.state }.from('submitted').to('accepted')
    end
  end
end
