# frozen_string_literal: true

FactoryBot.define do
  factory :journal do
    sequence :title do |n|
      Faker::Books::Dune.title + n.to_s
    end
    volumes_per_year { 1 }
    issues_per_volume { 10 }
    publishing_model { :oa_no_apcs }
    published { true }
    subjects { [create(:subject)] }
    publisher { create(:publisher) }
  end
end
