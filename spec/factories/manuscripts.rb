# frozen_string_literal: true

FactoryBot.define do
  factory :manuscript do
    sequence :title do |n|
      Faker::Book.title + n.to_s
    end
    abstract { 'I found something amazing!' }
    association :corresponding_author
    association :journal
    docx { Rack::Test::UploadedFile.new('spec/fixtures/Manuscript.docx', 'docx') }
    rejections { [create(:rejection)] }
    co_authors { [build(:co_author)] }
  end
end
