# frozen_string_literal: true

FactoryBot.define do
  factory :rejection do
    text { "What a piece of crap!" }
  end
end
