# frozen_string_literal: true

FactoryBot.define do
  factory :reviewer_invitation, class: InternalInvitation do
    invitee { association :user }
    invited_as { $reviewer }
    inviter { Assignment.where(role_id: $admin.id).first.user }
    invitable { association :manuscript }
  end
end
