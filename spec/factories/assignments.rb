# frozen_string_literal: true

FactoryBot.define do
  factory :assignment do
    factory :admin_assignment do
      assignable { association :journal, manuscripts: [FactoryBot.build(:manuscript, journal: nil)] }
      user { association :admin }
      role do
        Role.create(name: "Admin") unless Role.find_by_name("Admin").present?
        $admin = Role.find_by_name("Admin")
      end
    end

    factory :editorial_assignment do
      assignable { association :manuscript }
      user { association :editor }
      role do
        Role.create(name: "Editor") unless Role.find_by_name("Editor").present?
        $editor = Role.find_by_name("Editor")
      end
    end

    factory :managing_editor_assignment do
      assignable { association :manuscript }
      user { association :managing_editor }
      role do
        Role.create(name: "Managing Editor") unless Role.find_by_name("Managing Editor").present?
        $managing_editor = Role.find_by_name("Managing Editor")
      end
    end

    factory :editor_in_chief_assignment do
      assignable { association :manuscript }
      user { association :editor_in_chief }
      role do
        Role.create(name: "Editor in Chief") unless Role.find_by_name("Editor in Chief").present?
        $editor_in_chief = Role.find_by_name("Editor in Chief")
      end
    end

    factory :reviewer_assignment do
      assignable { association :manuscript }
      user { association :user }
      role do
        Role.create(name: "Reviewer") unless Role.find_by_name("Reviewer").present?
        $reviewer = Role.find_by_name("Reviewer")
      end
    end
  end
end
