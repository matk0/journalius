# frozen_string_literal: true

FactoryBot.define do
  factory :institution do
    name { 'University of Texas' }
    kind { 'university' }
    iban { '123456789' }
  end

  factory :publisher, class: Institution do
    name { Faker::Company.name }
    kind { 'publisher' }
    sequence :doi do |n|
      "123-#{n}"
    end
  end
end
