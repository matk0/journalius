# frozen_string_literal: true

FactoryBot.define do
  factory :co_author do
    full_name { "Mgr. Attila Vegh" }
  end
end
