# frozen_string_literal: true

FactoryBot.define do
  factory :revision_request do
    submission { nil }
  end
end
