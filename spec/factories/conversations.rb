# frozen_string_literal: true

FactoryBot.define do
  factory :conversation do
    author { nil }
    receiver { nil }
    manuscript { nil }
  end
end
