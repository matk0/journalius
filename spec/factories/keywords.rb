# frozen_string_literal: true

FactoryBot.define do
  factory :keyword, class: ActsAsTaggableOn::Tag do
    name { 'hello' }
  end
end
