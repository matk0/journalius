# frozen_string_literal: true

FactoryBot.define do
  factory :scopus, class: AbstractAndIndexingDatabase do
    name { 'Scopus' }
  end
end
