# frozen_string_literal: true

FactoryBot.define do
  factory :subject do
    sequence :name do |n|
      Faker::Books::Dune.character + n.to_s
    end
  end
end
