# frozen_string_literal: true

FactoryBot.define do
  factory :address do
    street { 'Račianska 78' }
    zipcode { '83120' }
    city { 'Bratislava' }
    country { 'Slovakia' }
    association :institution
  end
end
