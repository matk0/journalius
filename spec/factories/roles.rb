# frozen_string_literal: true

FactoryBot.define do
  factory :editor_role, class: Role do
    name { 'Editor' }
  end

  factory :managing_editor_role, class: Role do
    name { 'Managing Editor' }
  end

  factory :admin_role, class: Role do
    name { 'Admin' }
  end
end
