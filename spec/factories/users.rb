# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    title { 'Mr.' }
    first_name { 'Matej' }
    last_name { 'Lukášik' }
    password { 'asdfasdf' }
    password_confirmation { 'asdfasdf' }
    sequence :email do |n|
      "user#{n}@example.com"
    end
  end

  factory :admin, class: User do
    title { 'Dr.' }
    first_name { 'Admin' }
    last_name { 'Adminovie' }
    password { 'asdfasdf' }
    password_confirmation { 'asdfasdf' }
    email { "admin@journalius.com" }
  end

  factory :editor, class: User do
    title { 'Prof.' }
    first_name { 'Alex' }
    last_name { 'Ionut' }
    password { 'asdfasdf' }
    password_confirmation { 'asdfasdf' }
    sequence :email do |n|
      "alex#{n}@example.com"
    end
  end

  factory :corresponding_author, class: User do
    title { 'Prof.' }
    first_name { 'Bilbo' }
    last_name { 'Baggins' }
    password { 'tovictory' }
    password_confirmation { 'tovictory' }
    sequence :email do |n|
      "corresponding_author#{n}@example.com"
    end
  end

  factory :freelance_editor, class: User do
    title { 'Prof.' }
    first_name { 'Samwise' }
    last_name { 'Gamgee' }
    password { 'tovictory' }
    password_confirmation { 'tovictory' }
    sequence :email do |n|
      "freelance_editor#{n}@example.com"
    end
  end

  factory :managing_editor, class: User do
    title { 'Dr.' }
    first_name { 'Martin' }
    last_name { 'Velický' }
    password { 'asdfasdf' }
    password_confirmation { 'asdfasdf' }
    email { 'martin@journalius.com' }
  end

  factory :editor_in_chief, class: User do
    title { 'Mr.' }
    first_name { 'Editor' }
    last_name { 'InChief' }
    password { 'asdfasdf' }
    password_confirmation { 'asdfasdf' }
    email { 'editor.in.chief@journalius.com' }
  end
end
