# # frozen_string_literal: true

# require 'rails_helper'

# RSpec.feature 'JournalCreations', type: :feature, js: true do
#   scenario 'User can create a new Journal' do
#     create(:editor_role)
#     scopus = create(:scopus)
#     managing_editor_assignment = create(:managing_editor_assignment)
#     managing_editor = managing_editor_assignment.user
#     managing_editor.update(institution: managing_editor_assignment.institution)
#     sign_in managing_editor
#     visit journals_path

#     click_on 'New'

#     fill_in 'journal_title', with: 'Journal of Rocket Science'
#     fill_in 'Subtitle', with: 'Putting people on moon since 1999!'
#     fill_in 'journal_webpage', with: 'www.zerohedge.com'
#     fill_in 'p-ISSN', with: '2049-3630'
#     fill_in 'e-ISSN', with: '2049-3630'
#     fill_in 'journal_volumes_per_year', with: 20
#     fill_in 'journal_issues_per_volume', with: 40
#     select 'Periodical Publishing', from: 'journal_publishing_frequency'
#     check 'March'
#     check 'June'
#     check 'September'
#     check 'December'
#     fill_in_trix_editor('journal_editorial_information_trix_input_journal', with: 'Very important editorial information!')
#     select 'Martin Velický', from: 'journal_assignments_attributes_0_user_id'
#     select 'Editor', from: 'journal_assignments_attributes_0_role_id'
#     select 'Open Access with no APCs', from: 'journal_publishing_model'
#     select 'Scopus', from: 'journal_abstract_and_indexing_database_ids'
#     select 'Double blind', from: 'journal_peer_review_type'

#     click_button 'Create Journal'
#     expect(page).to have_text('Journal of Rocket Science has been created.')

#     expect(Journal.last.title).to eq 'Journal of Rocket Science'
#     expect(Journal.last.subtitle).to eq 'Putting people on moon since 1999!'
#     expect(Journal.last.webpage).to eq 'www.zerohedge.com'
#     expect(Journal.last.print_issn).to eq '2049-3630'
#     expect(Journal.last.electronic_issn).to eq '2049-3630'
#     expect(Journal.last.volumes_per_year).to eq 20
#     expect(Journal.last.issues_per_volume).to eq 40
#     expect(Journal.last.publishing_frequency).to eq 'periodical_publishing'
#     expect(Journal.last.expected_months_of_issue).to eq ['march', 'june', 'september', 'december']
#     expect(Journal.last.editorial_information.to_plain_text).to eq 'Very important editorial information!'
#     expect(Journal.last.assignments[0].user.full_name).to eq 'Martin Velický'
#     expect(Journal.last.assignments[0].role.name).to eq 'Editor'
#     expect(Journal.last.abstract_and_indexing_databases[0]).to eq scopus
#     expect(Journal.last.peer_review_type).to eq 'double_blind'
#   end
# end
