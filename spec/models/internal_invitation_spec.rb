# frozen_string_literal: true

require 'rails_helper'

RSpec.describe InternalInvitation, type: :model do
  it { should belong_to(:invitee) }
  it { should belong_to(:inviter) }
  it { should belong_to(:invited_as) }
  it { should belong_to(:invitable) }
end
