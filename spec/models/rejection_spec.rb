# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Rejection, type: :model do
  it { should belong_to(:manuscript).optional }
end
