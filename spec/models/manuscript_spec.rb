# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Manuscript, type: :model do
  it { should have_many(:co_authors) }
  it { should have_many(:peer_reviews) }
  it { should have_many(:technical_edits) }
  it { should have_many(:channels) }
  it { should have_many(:rejections) }
  it { should have_many(:revision_requests) }
  it { should have_and_belong_to_many(:subjects) }
  it { should belong_to(:corresponding_author) }
  it { should belong_to(:journal) }
end
