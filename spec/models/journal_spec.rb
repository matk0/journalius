# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Journal, type: :model do
  it { should have_and_belong_to_many(:subjects) }
  it { should have_and_belong_to_many(:abstract_and_indexing_databases) }
  it { should validate_presence_of(:title) }
  it { should validate_uniqueness_of(:title) }
  it { should belong_to(:publisher).optional }
  it { should belong_to(:payer).optional }
  it { should belong_to(:owner).optional }
end
