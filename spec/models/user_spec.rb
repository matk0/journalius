# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should validate_presence_of(:password_confirmation) }
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should have_many(:assignments) }
  it { should have_many(:messages) }
  it { should have_many(:events) }
  it { should have_many(:channel_users) }
  it { should define_enum_for(:title) }
end
