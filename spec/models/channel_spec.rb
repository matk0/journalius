# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Channel, type: :model do
  it { should have_many(:messages) }
  it { should have_many(:channel_users) }
  it { should belong_to(:manuscript).optional }
end
