# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Subject, type: :model do
  it { should validate_uniqueness_of(:name) }
  it { should have_and_belong_to_many(:journals) }
  it { should have_and_belong_to_many(:manuscripts) }
end
