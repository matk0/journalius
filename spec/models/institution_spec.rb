# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Institution, type: :model do
  it { should validate_presence_of :name }
  it { should validate_presence_of :kind }
  it { should have_one :address }
  it { should have_one :billing_address }
  it { should have_many :journals }
end
