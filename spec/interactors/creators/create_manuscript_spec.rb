# frozen_string_literal: true
#
require 'rails_helper'

RSpec.describe Creators::CreateManuscript do
  let!(:admin_assignment) { create(:admin_assignment ) }
  let(:manuscript) { build(:manuscript, journal: admin_assignment.assignable ) }

  subject(:context) { Creators::CreateManuscript.call(manuscript: manuscript) }

  describe ".call" do
    it "succeeds." do
      expect(context).to be_a_success
    end

    it "creates an event for the user." do
      subject
      expect(Event.last.eventable).to eq manuscript
      expect(Event.last.user).to eq manuscript.corresponding_author
      expect(Event.last.action).to eq "submitted"
    end
  end
end
