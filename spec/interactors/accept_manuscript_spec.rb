# frozen_string_literal: true
#
require 'rails_helper'

RSpec.describe AcceptManuscript do
  let!(:editorial_assignment) { create(:editorial_assignment) }
  let(:manuscript) { editorial_assignment.assignable }
  subject(:context) { AcceptManuscript.call(manuscript: manuscript, executor: editorial_assignment.user ) }

  describe ".call" do
    it "succeeds." do
      expect(context).to be_a_success
    end

    it "changes the state of the manuscript to accepted." do
      expect{subject}.to change{manuscript.state}.from("submitted").to("accepted")
    end

    it "creates an event." do
      subject
      expect(Event.last.eventable).to eq manuscript
      expect(Event.last.user).to eq manuscript.editors[0]
      expect(Event.last.action).to eq "accepted"
    end
  end
end
