# frozen_string_literal: true
#
require 'rails_helper'

RSpec.describe Payers::Create do
  let!(:admin_assignment) { create(:admin_assignment) }

  subject(:context) { Payers::Create.call(payer: Institution.new(name: "Hakuna Matata", kind: "university"), user: admin_assignment.user, journal: admin_assignment.assignable) }

  describe ".call" do
    it "succeeds." do
      expect(context).to be_a_success
    end
  end
end
