Feature: Author can resubmit a manuscript
	@javascript
	Scenario:
		Given there is an editorial assignment
		Given there is an admin assignment
		Given I am signed in as manuscript's author
		Given the manuscript is in revision
		When I visit my manuscript's page
		And I click on "Edit"
		And I edit it's title
		And I click on "Save changes"
		Then I have saved the new title
		When I click on Re-Submit modal button
		And I fill out resubmission message
		And I click on Re-Submit form button
		Then I have resubmitted the manuscript
