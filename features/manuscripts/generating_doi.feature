Feature: Managing Editor can generate a DOI
	@javascript
	Scenario: Managing Editor can generate a DOI
		Given there is an admin assignment
		Given assignment's journal has an editorial assignment
		Given I am signed in as journal's admin
		When I visit assigned manuscript's page 
		Then I can generate a DOI for the manuscript
