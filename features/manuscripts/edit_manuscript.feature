Feature: I can update a manuscript
	As an editor of a manuscript
	I can update it

	@javascript
	Scenario: Editor can remove co authors
		Given there is an admin assignment
		Given there is an editor role
		Given I am signed in as journal's admin
		When I visit the manuscript's page
		And I click on 'Edit'
		And I remove the first co author
		Then I should have removed the co author from the manuscript

	@javascript
	Scenario: Editor can add co authors
		Given there is an admin assignment
		Given there is an editor role
		Given I am signed in as journal's admin
		When I visit the manuscript's page
		And I click on 'Edit'
		And I add a co author
		Then I should have added a co author to the manuscript

	@javascript
	Scenario: Editor of a manuscript can edit it
		Given there is an admin assignment
		Given there is an editor role
		Given I am signed in as journal's admin
		When I visit the manuscript's page
		And I click on 'Edit'
		Then I should see 'EDITING'
		When I change the manuscript's attributes
		And I click on 'Save changes'
		Then I should have changed the manuscript

	@javascript
	Scenario: Editor of a manuscript can add a file
		Given there is an admin assignment
		Given there is an editor role
		Given I am signed in as journal's admin
		When I visit the manuscript's page
		When I click on 'Edit'
		Then I can see the existing file attached
		Then I can delete the file
