Feature: Peer reviews management
	@javascript
	Scenario: Journal admin can invite an internal user to review a manuscript
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		When I visit manuscript's peer reviews section
		And I click on new peer review
		And I invite an existing user to peer review the manuscript at hand
		Then I have created an internal invitation for Prof. Alex Ionut to become peer reviewer of the manuscript
		Then the system should have notified the invitee about the reviewer invitation
	Scenario: Internal user can accept the invitation
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given there is a reviewer invitation
		Given I am signed in as the invitee
		When I accept the invitation
		Then the invitation's state should be 'accepted'
		Then the invitation should have been transformed to an assignment
		Then the system should notify the inviter about the acceptance
