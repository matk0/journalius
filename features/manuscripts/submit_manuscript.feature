Feature: Visitor can submit a manuscript
	@javascript
	Scenario:
		Given there is a subject
		Given there is a keyword
		Given there is an admin assignment
		When I visit the published journal's page
		And I click on "Submit a manuscript"
		And I fill out the manuscript submission form
		And I click on "Submit"
		Then I should have created a new manuscript
