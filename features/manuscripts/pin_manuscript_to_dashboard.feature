Feature: As a managing editor I can add a manuscript to dashboard

	@javascript
	Scenario: Journal owner can pin it to his/her dashboard
		Given there is an admin assignment
		Given the admin is assigned a manuscript
		Given I am signed in as journal's admin
		When I visit the manuscript's page
		And I click on the pin to dashboard icon
		Then I changed the manuscript's pinned to true
		Then I can see it on my dashboard
