Feature: Managing Editor can add or remove other editors from a manuscript
	@javascript
	Scenario: Managing Editor can remove an editor from a manuscript
		Given there is an admin assignment
		Given assignment's journal has an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		When I visit assigned manuscript's page 
		Then I can remove an existing editor
