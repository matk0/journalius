Feature: Journal Admin can update journal's editors in chief
	## EXTERNAL INVITATIONS
	@javascript
	Scenario: Journal Admin can invite an outside person to be journal's editor in chief
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		When I visit journal's editorial information page
		And I click on add editor in chief
		And I fill out and submit the invitation form
		Then I have invited Matej Lukášik to become a editor in chief
		Then the system should have notified the invitee about the invitation
	@javascript
	Scenario: Journal Admin can delete an outstanding invitation to an outside person
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		Given there is an internal invitation
		When I visit journal's editorial information page
		And I click on remove editor in chief invitation
		Then I should not see the invitation anymore
		Then the system should have notified the invitee about the invitation being withdrawn
	@javascript
	Scenario: The outside invitee accepts the invitation
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		When I visit journal's editorial information page
		And I click on add editor in chief
		And I fill out and submit the invitation form
		When the current user signs out
		And the invitee accepts the invitation
		Then the invitation's state should be 'accepted'
		Then I should receive a notification about the acceptance
		Then the invitation should have been transformed to an assignment
	@javascript
	Scenario: The outside invitee declines the invitation
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		When I visit journal's editorial information page
		And I click on add editor in chief
		And I fill out and submit the invitation form
		When the current user signs out
		And the invitee rejects the invitation
		Then I should receive a notification about the declination
	## INTERNAL INVITATIONS
	@javascript
	Scenario: Journal Admin can invite a user to be journal's editor in chief
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		When I visit journal's editorial information page
		And I click on add editor in chief
		And I invite an existing user
		Then I have invited Prof. Alex Ionut to become a editor in chief
		Then the system should have notified Alex about the invitation
	@javascript
	Scenario: The internal invitee declines the invitation
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		When I visit journal's editorial information page
		And I click on add editor in chief
		And I invite an existing user
		And the current user signs out
		Given I'm signed in as the internal invitee
		And the internal invitee rejects the invitation
		Then the invitation's state should be 'declined'
		Then the system should notify the inviter about the declination
	@javascript
	Scenario: The internal invitee accepts the invitation
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		When I visit journal's editorial information page
		And I click on add editor in chief
		And I invite an existing user
		And the current user signs out
		Given I'm signed in as the accepting internal invitee
		And the internal invitee accepts the invitation
		Then the invitation's state should be 'accepted'
		Then I should receive a notification about the acceptance
		Then the invitation should have been transformed to an assignment
