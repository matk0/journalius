Feature: User can create a journal
	@javascript
	Scenario: User can create a Journal
		Given there is an admin assignment
		Given there is a user
		Given there is a subject
		Given I am signed in as a user
		When I visit Journals page
		And I click on new journal button
		And I fill out the new journal form
		And I submit the new journal form
		Then I created a journal
