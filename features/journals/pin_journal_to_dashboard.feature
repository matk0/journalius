Feature: As journal admin, I can add manuscript to my dashboard

	@javascript
	Scenario: Journal admin can pin manuscript to his/her dashboard
		Given there is an admin assignment
		Given I am signed in as journal's admin
		When I visit journal page
		And I click on the pin to dashboard icon
		Then I changed the journals pinned attribute to true
		Then I can see the journal on my dashboard
