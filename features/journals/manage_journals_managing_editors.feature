Feature: Journal Admin can update journal's managing editors
	## EXTERNAL INVITATIONS
	@javascript
	Scenario: Journal Admin can invite an outside person to be journal's managing editor
		Given there is a admin assignment
		Given there is an editorial assignment
		Given there is a managing editor assignment
		Given there is a editor in chief assignment
		Given there is a reviewer assignment
		Given I am signed in as journal's admin
		When I visit journal's editorial information page
		And I click on add managing editor
		And I fill out and submit the managing editor invitation form
		Then I have invited Matej Lukášik to become a managing editor
		Then the system should have notified the invitee about the managing editor invitation
