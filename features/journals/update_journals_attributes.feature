Feature: Journal Admin can update journal's attributes
	@javascript
	Scenario: Journal Admin can update journal's publishing model
		Given there is a admin assignment
		Given there is an editorial assignment
		Given I am signed in as journal's admin
		When I visit journal page
		When I click on edit publishing model
		When I edit journal's publishing model
		Then I updated it's publishing model
	@javascript
	Scenario: Journal Admin can update journal's publishing model
		Given there is a admin assignment
		Given there is an editorial assignment
		Given I am signed in as journal's admin
		When I visit journal page
		When I click on edit print ISSN
		When I edit journal's print ISSN
		Then I updated it's print ISSN
	@javascript
	Scenario: Journal Admin can update journal's publishing model
		Given there is a admin assignment
		Given there is an editorial assignment
		Given I am signed in as journal's admin
		When I visit journal page
		When I click on edit digital ISSN
		When I edit journal's digital ISSN
		Then I updated it's digital ISSN
