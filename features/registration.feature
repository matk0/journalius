Feature: Visit landing page
  As a visitor
  I can visit the landing page
	And register into Journalius

	Scenario: Visitor can see the 'Sign up' link
		When I visit the landing page
		Then I should be able to go to registration
	Scenario: Visitor can register
		Given I visit the landing page
		And I click on 'Sign up'
		And fill out the sign up form
		When I click on Sign up
		Then I registered into Journalius
		Then I am redirected to my Journals
