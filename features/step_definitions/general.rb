Then('I should see a {string} button') do |string|
  expect(page).to have_content(string)
end

Then('I should see {string}') do |string|
  expect(page).to have_content(string)
end

When('I click on {string}') do |string|
  click_on string
end

When('I click on Sign up') do
  find("input.btn.btn-lg.btn-block.btn-primary.mb-3").click
end

When('I click on Add Editor') do
  find('div[data-target="#add-editor-modal"]').click
end

Given("I am signed in as journal's admin") do
  visit new_user_session_path
  fill_in "Email", with: "admin@journalius.com"
  fill_in "Password", with: "asdfasdf"
  click_button "Sign in"
end

Given("I am signed in as a user") do
  visit new_user_session_path
  fill_in "Email", with: User.last.email
  fill_in "Password", with: "asdfasdf"
  click_button "Sign in"
end

Given("I'm signed in as the internal invitee") do
  visit new_user_session_path
  fill_in "Email", with: "alex6@example.com"
  fill_in "Password", with: "asdfasdf"
  click_button "Sign in"
end

Given("I'm signed in as the accepting internal invitee") do
  visit new_user_session_path
  fill_in "Email", with: "alex7@example.com"
  fill_in "Password", with: "asdfasdf"
  click_button "Sign in"
end

When("I visit a manuscript's page") do
  manuscript = FactoryBot.create(:manuscript, journal: Journal.first)
  visit manuscript_overview_path(manuscript)
end


When("I visit assigned manuscript's page") do
  role = Role.find_by_name("Editor")
  manuscript = Assignment.find_by_role_id(role.id).assignable
  visit manuscript_overview_path(manuscript)
end

Given("assignment's journal has an editorial assignment") do
  manuscript = FactoryBot.create :manuscript, journal: Journal.first
  FactoryBot.create(:editorial_assignment, assignable: manuscript)
end

Given("I am signed in as manuscript's author") do
  visit new_user_session_path
  fill_in "Email", with: Manuscript.last.corresponding_author.email
  fill_in "Password", with: "tovictory"
  click_button "Sign in"
end

When("I visit my manuscript's page") do
  visit manuscript_overview_path(Manuscript.last)
end

When('the current user signs out') do
  visit root_path
  page.windows[0].maximize
  find(".avatar.avatar-sm.dropdown-toggle").click
  click_on "Log out"
end
