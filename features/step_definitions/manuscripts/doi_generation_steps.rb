Then('I can generate a DOI for the manuscript') do
  click_on "Generate DOI number"
  sleep 0.1
  expect(page).to have_content("#{Manuscript.second.journal.publisher.doi}-#{Manuscript.second.id}")
end
