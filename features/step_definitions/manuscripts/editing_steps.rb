When('I click on the pin to dashboard icon') do
  sleep 0.1
  find(".pin-to-dashboard").click
end

When("I visit the manuscript's page") do
  visit manuscript_overview_path(Manuscript.first)
end

Then("I changed the manuscript's pinned to true") do
  sleep 0.1
  expect(Pin.last.pinnable).to eq Manuscript.last
end

Then("I can see it on my dashboard") do
  visit dashboard_path
  expect(page).to have_content(Manuscript.last.title)
end

When('I remove the first co author') do
  find(".fe.fe-trash.float-left").click
  sleep 0.1
end

Then('I should have removed the co author from the manuscript') do
  expect(Manuscript.last.co_authors).to eq []
  expect(page).to_not have_content("Mgr. Attila Vegh")
end

When('I add a co author') do
  click_on "Add Co-Author"
  find(".manuscript_co_authors_full_name").set "Gandalf the Grey" 
  click_on "Add Co-Author"
  all(".manuscript_co_authors_full_name")[1].set "Bilbo Baggins"
  click_on "Save changes"
end

Then('I should have added a co author to the manuscript') do
  expect(Manuscript.last.co_authors.count).to eq 3
end

When("I change the manuscript's attributes") do
    fill_in 'Title', with: 'Revolutionary Discovery'
    subject = Subject.create(name: "mad skills")
    page.execute_script("document.getElementsByName('manuscript[subject_ids][]')[0].value = #{subject.id}")
    keyword = ActsAsTaggableOn::Tag.create(name: "self-improvement")
    page.execute_script("document.getElementsByName('manuscript[keyword_list][]')[0].value = '#{keyword.name}'")
    find('#react-languages-select').click
    within('#react-languages-select') do
      find('#react-select-2-option-0').click
    end
    fill_in_trix_editor('manuscript_abstract_trix_input_manuscript_1', with: 'Abstract bitches!')
    fill_in_trix_editor('manuscript_references_trix_input_manuscript_1', with: 'References')
    # attach_file "files[]", Rails.root + "spec/fixtures/Manuscript.docx", make_visible: true, match: :first
    # sleep 0.5
end

Then('I should have changed the manuscript') do
  expect(Manuscript.last.title).to eq 'Revolutionary Discovery'
  expect(Manuscript.last.subjects[0].name).to eq 'mad skills'
  expect(Manuscript.last.keyword_list).to eq ["self-improvement"]
  expect(Manuscript.last.languages).to eq ["Afar"]
  expect(Manuscript.last.abstract.to_s).to eq "<div class=\"trix-content\">\n  <div>Abstract bitches!</div>\n</div>\n"
  expect(Manuscript.last.references.to_s).to eq "<div class=\"trix-content\">\n  <div>References</div>\n</div>\n"
end

Then('I can see the existing file attached') do
  expect(page).to have_content "Manuscript.docx"
end

Then('I can delete the file') do
  find("#docx-remove-icon").click
  expect(page).to have_content("No docx attached yet.")
end
