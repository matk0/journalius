Given('I am signed in as the invitee') do
  visit new_user_session_path
  fill_in "Email", with: InternalInvitation.last.invitee.email
  fill_in "Password", with: "asdfasdf"
  click_button "Sign in"
end

When("I visit manuscript's peer reviews section") do
  visit manuscript_peer_reviews_path(Manuscript.first)
end

When('I click on new peer review') do
  find("i[data-target='#new-peer-review-invitation-modal']").click
end

When('I invite an existing user to peer review the manuscript at hand') do
  select "Prof. Alex Ionut", from: "internal_invitation[invitee_id]"
  click_on "Invite"
  sleep 0.5
end

Then('I have created an internal invitation for Prof. Alex Ionut to become peer reviewer of the manuscript') do
  find("#reviewer-invitations > #invitation-1")
  expect(InternalInvitation.last.invitee.email).to eq "alex14@example.com"
  expect(InternalInvitation.last.inviter.email).to eq "admin@journalius.com"
  expect(InternalInvitation.last.invited_as.id).to eq $reviewer.id
  expect(InternalInvitation.last.invitable_type).to eq "Manuscript"
end

Then('the system should have notified the invitee about the reviewer invitation') do
  expect(ActionMailer::Base.deliveries.last.subject).to eq "You have been invited by Dr. Admin Adminovie to review #{InternalInvitation.last.invitable.title}."
  expect(ActionMailer::Base.deliveries.last.to).to eq ["alex14@example.com"]
end

When('I accept the invitation') do
  visit internal_invitation_accept_path(InternalInvitation.last.id)
end

Then('the system should notify the inviter about the acceptance') do
  expect(ActionMailer::Base.deliveries.last.subject).to eq "Your invitation of #{InternalInvitation.last.invitee.decorate.full_name} to review #{InternalInvitation.last.invitable.title} was accepted."
  expect(ActionMailer::Base.deliveries.last.to).to eq ["admin@journalius.com"]
end
