Given("I visit the journal's page") do
  visit journal_path(Journal.first)
end

Given("I visit the published journal's page") do
  visit published_journal_path(Journal.first)
end

When('I fill out the manuscript submission form') do
    select "Mr.", from: "manuscript[corresponding_author_attributes][title]"
    fill_in 'manuscript[corresponding_author_attributes][first_name]', with: 'Matej'
    fill_in 'manuscript[corresponding_author_attributes][last_name]', with: 'Lukášik'
    fill_in 'manuscript[corresponding_author_attributes][email]', with: 'matej@university.com'
    fill_in 'manuscript[corresponding_author_attributes][password]', with: 'asdfasdf'
    fill_in 'manuscript[corresponding_author_attributes][password_confirmation]', with: 'asdfasdf'
    click_on 'Add Co-Author'
    all('.form-control.manuscript_co_authors_full_name').last.set('Mgr. Martin Velický')
    click_on 'Add Co-Author'
    all('.form-control.manuscript_co_authors_full_name').last.set('Alexandru Barbu')
    fill_in 'Title', with: 'Revolutionary Discovery'
    page.execute_script("document.getElementsByName('manuscript[subject_ids][]')[0].value = #{Subject.last.id}")
    page.execute_script("document.getElementsByName('manuscript[keyword_list][]')[0].value = '#{ActsAsTaggableOn::Tag.last.name}'")
    find('#react-languages-select').click
    within('#react-languages-select') do
      find('#react-select-2-option-0').click
    end
    fill_in_trix_editor('manuscript_abstract_trix_input_manuscript', with: 'Abstract bitches!')
    fill_in_trix_editor('manuscript_references_trix_input_manuscript', with: 'References')
    attach_file "files[]", Rails.root + "spec/fixtures/Manuscript.docx", make_visible: true, match: :first
    sleep 0.5
end

Then('I should have created a new manuscript') do
  expect(Manuscript.last.title).to eq 'Revolutionary Discovery'
  expect(Manuscript.last.keyword_list).to eq ['hello']
  expect(Manuscript.last.corresponding_author.title).to eq 'Mr.'
  expect(Manuscript.last.corresponding_author.first_name).to eq 'Matej'
  expect(Manuscript.last.corresponding_author.last_name).to eq 'Lukášik'
  expect(Manuscript.last.corresponding_author.email).to eq 'matej@university.com'
  expect(Manuscript.last.docx.blob.filename.to_s).to eq 'Manuscript.docx'
end
