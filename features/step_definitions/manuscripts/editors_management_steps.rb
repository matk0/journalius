Then('I created an assignment') do
  expect(Assignment.last.role.name).to eq "Editor"
  find("#assignment-#{Assignment.last.id}")
end
