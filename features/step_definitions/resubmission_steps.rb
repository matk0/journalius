Given('the manuscript is in revision') do
  revision_request = RevisionRequest.new manuscript_id: Manuscript.last.id, text: "Revise this shit!"
  Creators::CreateRevisionRequest.call revision_request: revision_request, executor: User.first
end

When("I edit it's title") do
  fill_in "Title", with: "A discovery like never before!!!"
end

Then('I have saved the new title') do
  expect(Manuscript.last.title).to eq "A discovery like never before!!!"
end

When('I click on Re-Submit modal button') do
  find('.btn[data-target="#resubmit-modal"]').click
end

When('I fill out resubmission message') do
  fill_in_trix_editor('resubmission_text_trix_input_resubmission', with: 'Here is my revision you slowtard!')
end

When('I click on Re-Submit form button') do
  click_on "Re-Submit"
end

Then('I have resubmitted the manuscript') do
  sleep 0.3
  expect(Resubmission.last.manuscript).to eq Manuscript.last
  expect(Manuscript.last.state).to eq "submitted"
end


