When('I click on edit publishing model') do
  find('i[data-target="#edit-publishing-model-modal"]').click
end

When('I click on edit languages') do
  find('i[data-target="#edit-languages-modal"]').click
end

When("I edit journal's publishing model") do
  select "Open Access with APCs", from: "journal[publishing_model]"
  click_on "Change"
  sleep 0.2
end

Then("I updated it's publishing model") do
  expect(Journal.first.publishing_model).to eq "oa_apcs"
end

When('I click on edit print ISSN') do
  find('i[data-target="#edit-print-issn-modal"]').click
end

When("I edit journal's print ISSN") do
  expect(Journal.first.print_issn).to_not eq "0953-4563"
  fill_in "journal_print_issn", with: "0953-4563"
  click_on "Change"
  sleep 0.2
end

Then("I updated it's print ISSN") do
  expect(Journal.first.print_issn).to eq "0953-4563"
end

When('I click on edit digital ISSN') do
  find('i[data-target="#edit-electronic-issn-modal"]').click
end

When("I edit journal's digital ISSN") do
  expect(Journal.first.print_issn).to_not eq "0953-4563"
  fill_in "journal_electronic_issn", with: "0953-4563"
  click_on "Change"
  sleep 0.2
end

Then("I updated it's digital ISSN") do
  expect(Journal.first.electronic_issn).to eq "0953-4563"
end
