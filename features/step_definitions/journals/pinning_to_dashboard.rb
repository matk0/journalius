Then('I changed the journals pinned attribute to true') do
  sleep 0.1
  expect(Pin.last.pinnable).to eq Journal.last
end

Then('I can see the journal on my dashboard') do
  visit dashboard_path
  expect(page).to have_content(Journal.last.title)
end
