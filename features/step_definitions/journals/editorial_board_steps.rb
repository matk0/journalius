Given('there is an internal invitation') do
  InternalInvitation.create! invitee: FactoryBot.create(:user),
                             invited_as: $editor_in_chief,
                             inviter: Role.find_by_name("Admin").users.first,
                             invitable: Role.find_by_name("Admin").assignments.first.assignable
                            
end

When('I click on add editor in chief') do
  find('i[data-target="#edit-editors-in-chief-modal"]').click
end

When('I click on add managing editor') do
  find('i[data-target="#edit-managing-editors-modal"]').click
end

When('I fill out and submit the invitation form') do
  select "Mr.", from: "user[title]"
  fill_in "First name", with: "Matej"
  fill_in "Last name", with: "Lukášik"
  fill_in "Email", with: "matej.lukasik@occupationbliss.com"
  click_on "Invite new editor"
end

Then('I have invited Matej Lukášik to become a editor in chief') do
  sleep 0.3
  find("#editor-in-chief-invitations > #invitation-1")
  expect(InternalInvitation.last.invitee.email).to eq "matej.lukasik@occupationbliss.com"
  expect(InternalInvitation.last.inviter.email).to eq "admin@journalius.com"
  expect(InternalInvitation.last.invited_as.id).to eq $editor_in_chief.id
  expect(InternalInvitation.last.invitable_type).to eq "Journal"
end

When('I fill out and submit the managing editor invitation form') do
  select "Mr.", from: "user[title]"
  fill_in "First name", with: "Matej"
  fill_in "Last name", with: "Lukášik"
  fill_in "Email", with: "matej.lukasik@occupationbliss.com"
  click_on "Invite new Managing Editor"
end

Then('I have invited Matej Lukášik to become a managing editor') do
  sleep 0.3
  find("#managing-editor-invitations > #invitation-1")
  expect(InternalInvitation.last.invitee.email).to eq "matej.lukasik@occupationbliss.com"
  expect(InternalInvitation.last.inviter.email).to eq "admin@journalius.com"
  expect(InternalInvitation.last.invited_as.id).to eq $managing_editor.id
  expect(InternalInvitation.last.invitable_type).to eq "Journal"
end

Then('the system should have notified the invitee about the invitation') do
  expect(ActionMailer::Base.deliveries.last.subject).to eq "You have been invited by Dr. Admin Adminovie to become Editor in Chief at #{Journal.first.title}."
  expect(ActionMailer::Base.deliveries.last.to).to eq ["matej.lukasik@occupationbliss.com"]
end

Then('the system should have notified the invitee about the managing editor invitation') do
  expect(ActionMailer::Base.deliveries.last.subject).to eq "You have been invited by Dr. Admin Adminovie to become Managing Editor at #{Journal.first.title}."
  expect(ActionMailer::Base.deliveries.last.to).to eq ["matej.lukasik@occupationbliss.com"]
end

Then('I can remove an existing editor') do
  expect(page).to have_content "Prof. Alex Ionut"
  find("#remove-editorial-assignment-#{Assignment.find_by_role_id(Role.find_by_name("Editor")).id}").click
  sleep 0.1
  expect(page).to_not have_content "Prof. Alex Ionut"
end

When('I click on remove editor in chief invitation') do
  accept_confirm do
    find("#remove-invitation-#{InternalInvitation.last.id}").click
  end
end

Then('I should not see the invitation anymore') do
  expect(page).not_to have_selector("#invitation-#{InternalInvitation.last.id}")
end

Then('the system should have notified the invitee about the invitation being withdrawn') do
  expect(ActionMailer::Base.deliveries.last.subject).to eq "Your invitation to become Editor in Chief at #{Journal.first.title} was withdrawn."
  expect(ActionMailer::Base.deliveries.last.to).to eq ["user4@example.com"]
end

When('the invitee accepts the invitation') do
  visit accept_user_invitation_path(invitation_token: InternalInvitation.last.raw_invitation_token)
  fill_in "user[password]", with: "matk0jepan"
  fill_in "user[password_confirmation]", with: "matk0jepan"
  click_on "Set my password"
end

Then("the invitation's state should be {string}") do |string|
  expect(InternalInvitation.last.state).to eq string
end

Then('I should receive a notification about the acceptance') do
  expect(ActionMailer::Base.deliveries.last.subject).to eq "Your invitation of #{InternalInvitation.last.invitee.decorate.full_name} to become Editor in Chief at #{Journal.first.title} was accepted."
  expect(ActionMailer::Base.deliveries.last.to).to eq ["admin@journalius.com"]
end

Then('the invitation should have been transformed to an assignment') do
  assignment = Assignment.last
  invitation = InternalInvitation.last
  expect(assignment.role) == invitation.invited_as
  expect(assignment.user) == invitation.invitee
  expect(assignment.assignable) == invitation.invitable
end

When('the invitee rejects the invitation') do
  node = Capybara.string(ActionMailer::Base.deliveries.last.body.to_s).find("#decline-invitation")
  current_driver = Capybara.current_driver
  Capybara.current_driver = :rack_test
  visit node[:href]
  Capybara.current_driver = current_driver
end

Then('I should receive a notification about the declination') do
  expect(ActionMailer::Base.deliveries.last.subject).to eq "Your invitation of Mr. Matej Lukášik to become Editor in Chief at #{Journal.first.title} was declined."
  expect(ActionMailer::Base.deliveries.last.to).to eq ["admin@journalius.com"]
end

When('I invite an existing user') do
  select "Prof. Alex Ionut", from: "internal_invitation[invitee_id]"
  click_on "Invite"
end

Then('I have invited Prof. Alex Ionut to become a editor in chief') do
  find("#editor-in-chief-invitations > #invitation-1")
  expect(InternalInvitation.last.invitee.email).to eq "alex5@example.com"
  expect(InternalInvitation.last.inviter.email).to eq "admin@journalius.com"
  expect(InternalInvitation.last.invited_as.id).to eq $editor_in_chief.id
  expect(InternalInvitation.last.invitable_type).to eq "Journal"
end

Then('the system should have notified Alex about the invitation') do
  expect(ActionMailer::Base.deliveries.last.subject).to eq "You have been invited by Dr. Admin Adminovie to become Editor in Chief at #{Journal.first.title}."
  expect(ActionMailer::Base.deliveries.last.to).to eq ["alex5@example.com"]
end

When('the internal invitee rejects the invitation') do
  visit internal_invitation_decline_path(InternalInvitation.last.id)
end

Then('the system should notify the inviter about the declination') do
  expect(ActionMailer::Base.deliveries.last.subject).to eq "Your invitation of Prof. Alex Ionut to become Editor in Chief at #{Journal.first.title} was declined."
  expect(ActionMailer::Base.deliveries.last.to).to eq ["admin@journalius.com"]
end

When('the internal invitee accepts the invitation') do
  visit internal_invitation_accept_path(InternalInvitation.last.id)
end
