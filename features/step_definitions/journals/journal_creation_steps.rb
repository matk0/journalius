When('I visit Journals page') do
  visit journals_path
end

When('I click on new journal button') do
  find('div[data-target="#new-journal-modal"]').click
end

When('I fill out the new journal form') do
  fill_in "journal[title]", with: "Journal of Rocket Science"
  page.execute_script("document.getElementsByName('journal[subject_ids][]')[0].value = #{Subject.last.id}")
end

When('I submit the new journal form') do
  click_on "Create Journal"
  sleep 0.5
end

Then('I created a journal') do
  expect(Journal.last.title).to eq "Journal of Rocket Science"
end
