When('I visit journal page') do
  visit journal_overview_path(Journal.first)
end

When("I visit journal's editorial information page") do
  visit journal_editorial_path(Role.find_by_name("Admin").assignments.first.assignable)
end

Given('the admin is assigned a manuscript') do
  Assignment.create(assignable: Manuscript.first, user: User.last, role: FactoryBot.create(:editor_role))
end
