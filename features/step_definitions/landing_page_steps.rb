When('I visit the landing page') do
  visit root_path
end

Then('I should be able to go to registration') do
  expect(page).to have_content "Sign up"
end

When('I click on the "Journals" link') do
  click_on "Journals"
end

When('I click on the "Journal of Rocket Science" link') do
  click_on "Journal of Rocket Science"
end
