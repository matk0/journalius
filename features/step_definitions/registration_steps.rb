When('fill out the sign up form') do
  select "Mr.", from: "user[title]"
  fill_in "First name", with: "Frodo"
  fill_in "Last name", with: "Baggins"
  fill_in "Email", with: "frodo@shire.com"
  fill_in "Password", with: "shireIsGreat!"
  fill_in "Password confirmation", with: "shireIsGreat!"
end

When("I click on the 'Sign up' button") do
  find('input.btn.btn-lg.btn-block.btn-primary.mb-3').click
end

Then('I registered into Journalius') do
  expect(User.last.title).to eq "Mr."
  expect(User.last.first_name).to eq "Frodo"
  expect(User.last.last_name).to eq "Baggins"
  expect(User.last.email).to eq "frodo@shire.com"
end

Then('I am redirected to my Journals') do
  expect(page).to have_selector "h1", text: "Journals"
end
