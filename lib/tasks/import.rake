# frozen_string_literal: true

require 'roo'

namespace :import do
  task subject_tree: :environment do
    xlsx = Roo::Spreadsheet.open('./subject_tree.xlsx')
    sheet = xlsx.sheet('Sheet1')
    sheet.each do |row|
      subjects = row[0].split(':')

      if subjects.size == 1
        Subject.create(name: subjects[0][0..-2])
      elsif subjects.size == 2
        root_subject = Subject.find_by(name: subjects[0].strip)
        if root_subject.present?
          root_subject.children.create(name: subjects[1].strip)
        else
          root_subject = Subject.create(name: subjects[0].strip)
          root_subject.children.create(name: subjects[1].strip)
        end
      elsif subjects.size == 3
        root_subject = Subject.find_by(name: subjects[0].strip)
        if root_subject.present?
          sub_root_subject = Subject.find_by(name: subjects[1].strip)
          if sub_root_subject.present?
            sub_root_subject.children.create(name: subjects[2].strip)
          else
            sub_root_subject = root_subject.children.create(name: subjects[1].strip)
            sub_root_subject.children.create(name: subjects[2].strip)
          end
        else
          root_subject = Subject.create(name: subjects[0].strip)
          sub_root_subject = root_subject.children.create(name: subjects[1].strip)
          sub_root_subject.children.create(name: subjects[2].strip)
        end
      end
    end
  end
end
