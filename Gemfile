# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

gem 'rails', '~> 6.0.3'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 4.1'
gem 'sass-rails', '>= 6'
gem 'webpacker', '~> 5.2', '>= 5.2.1'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.7'
gem 'convert_api', '~> 1.2'
gem 'aasm', '~> 5.0', '>= 5.0.8'
gem 'activeadmin', '~> 2.7'
gem 'acts-as-taggable-on', '~> 6.5'
gem 'aws-sdk-s3'
gem 'bootsnap', '>= 1.4.2'
gem 'devise', '~> 4.7', '>= 4.7.1'
gem 'devise_invitable', '~> 2.0', '>= 2.0.2'
gem 'haml-rails', '~> 2.0', '>= 2.0.1'
gem 'image_processing', '~> 1.2'
gem 'interactor', '~> 3.1', '>= 3.1.2'
gem 'omniauth', '~> 1.9', '>= 1.9.1'
gem 'omniauth-google-oauth2', '~> 0.8.0'
gem 'omniauth-linkedin-oauth2', '~> 1.0'
gem 'omniauth-orcid', '~> 2.1', '>= 2.1.1'
gem 'pundit', '~> 2.1'
gem 'sendgrid-ruby', '~> 6.2', '>= 6.2.1'
gem 'sidekiq', '~> 6.0', '>= 6.0.7'
gem 'sidekiq-scheduler', '~> 3.0', '>= 3.0.1'
gem 'simple_form', '~> 5.0', '>= 5.0.2'
gem 'will_paginate', '~> 3.3'
gem 'faker', '~> 2.11'
gem 'roo', '~> 2.8', '>= 2.8.3'
gem 'wicked', '~> 1.3', '>= 1.3.4'
gem 'country_select', '~> 4.0'
gem 'iso-639', '~> 0.3.5'
gem 'rollbar', '~> 2.26'
gem 'breadcrumbs_on_rails', '~> 4.0'
gem 'ancestry', '~> 3.0', '>= 3.0.7'
gem 'draper', '~> 4.0', '>= 4.0.1'
gem 'library_stdnums', '~> 1.6'
gem 'skylight'
gem 'aws-sdk', '~> 3.0', '>= 3.0.1'
gem 'rubocop', require: false
gem 'rubocop-rails', '~> 2.6', require: false
gem 'rubycritic', require: false
gem 'rack-cors', '~> 1.1', '>= 1.1.1'
gem 'docx', '~> 0.6.0'
gem 'split', '~> 3.4', '>= 3.4.1', require: 'split/dashboard'
gem 'activerecord-session_store', '~> 1.1', '>= 1.1.3'
gem 'paper_trail', '~> 11.0'
gem 'diffy'
gem 'recaptcha', '~> 5.6'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '~> 4.0', '>= 4.0.1'
  gem 'factory_bot_rails', '~> 6.0'
  gem 'bullet', '~> 6.1'
  gem 'cucumber-rails', require: false
end

group :development do
  gem 'listen', '~> 3.2'
  gem 'web-console', '>= 3.3.0'
  gem 'guard-livereload', '~> 2.5', '>= 2.5.2'
  gem 'guard-rails', '~> 0.8.1'
  gem 'guard-webpacker', '~> 0.2.1'
  gem 'letter_opener', '~> 1.7'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'solargraph', '~> 0.39.8'
  gem 'traceroute', '~> 0.8.1'
  gem 'brakeman', '~> 4.10'
end

group :test do
  gem 'capybara', '~> 3.33'
  gem 'selenium-webdriver', '~> 3.142', '>= 3.142.7'
  gem 'webdrivers', '~> 4.4', '>= 4.4.1'
  gem 'shoulda-matchers', '~> 4.3'
  gem 'database_cleaner'
  gem 'rails-controller-testing'
  gem 'cucumber_factory', '~> 2.3', '>= 2.3.1'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
