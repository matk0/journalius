# frozen_string_literal: true

require 'rails_helper'

RSpec.describe QuestionnaireTemplate, type: :model do
  it { should belong_to :institution }
end
